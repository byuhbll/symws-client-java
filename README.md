[![Build Status](https://semaphoreci.com/api/v1/byuhbll/symws-client-java/branches/master/shields_badge.svg)](https://semaphoreci.com/byuhbll/symws-client-java)

# Symphony Web Services (SYMWS) Client for Java

This library provides JAX-RS clients and models for the legacy Symphony Web Services (SYMWS) provided by SirsiDynix.
The goal of this library is to make it trivially easy to use SYMWS.  Authentication and session management is handled
automatically for the most part, and the provided client classes provide a guided experience to expose the various
options available to users of the underlying services.


## Usage

### Creating a SymwsClientConfig

The heart of the SYMWS client library is the **SymwsClientConfig** class, a thread-safe container of of connection
information and credentials for a specific installation of SYMWS.  A **SymwsClientConfig** can be constructed in two
ways, demonstrated below:

#### Using the builder to to programmatically create a new instance:

```java
SymwsClientConfig symwsClientConfig = new SymwsClientConfig.Builder("myClientId")
        .rootUri("https://ils.example.com/symws")  // The base path to the SYMWS installation.
        .authentication("myUserId", "myPassword")  // Login credentials, if you plan to use a service which requires
                                                   // authentication.  These can also be provided later; see the API
                                                   // docs for more information.
        .build();
```

#### Using Jackson to create a new instance from a JSON template:

```java
try(Reader reader = Files.newBufferedReader(Paths.get("config.json")) {
    ObjectMapper mapper = new ObjectMapper();
    SymwsClientConfig symwsClientConfig = mapper.readValue(reader, SymwsClientConfig.class);
}
```

```json
{
  "rootUri" : "https://ils.example.com/symws",
  "clientId" : "myClientId",
  "userId" : "myUserId",
  "password" : "myPassword"
}
```

### Using the client classes

Once you have constructed a **SymwsClientConfig** object, you are ready to begin making web service calls using the
built-in client classes.  Each client classes uses a standard constructor that expects a **SymwsClientConfig** instance.
This allows the client classes to automatically resolve the service path and provide any required headers (such as the
`x-sirs-clientID` and `x-sirs-sessionToken` headers.  Additionally, most clients will also take advantage of built-in
"keep-alive" session logic which will automatically renew a user session if the client detects that it has timed out.

As stated earlier, the chief goal of this library is to provide a guided, easy experience for users of SYMWS.
Accordingly, required parameters which **must** be included with each request are baked into the method definitions of
each client class.  Some classes may additionally expect an "OptionalParams" object specific to the underlying service.
These classes behave much like builders, and allow users great flexibility to tweak the behavior of each request made
using that client.  While most optional parameters simply require a boolean trigger to enable their functionality, some
instead expect an enumerated option or value to be included, and in these cases, the user is prompted to specify
their desired option through the use of a Java enum.

The following code sample demonstrates the process of retrieving MARC record data for a list of a few catalog records:

```java
// Create a SymwsClientConfig object and the appropriate client.  Both of these object need only be created once, since
// they are thread-safe.
SymwsClientConfig symwsClientConfig = new SymwsClientConfig.Builder("myClientId")
        .rootUri("https://ils.example.com/symws")
        .build();

LookupTitleInfoClient client = new LookupTitleInfoClient(symwsClientConfig, new LookupTitleInfoClient.OptionalParams()
        .marcEntryFilter(MarcEntryFilter.ALL));

// Iterate through the desired title IDs and download the bibliographic data for each.
for (String catalogKey : Arrays.asList("1", "2", "3", "4", "5")) {
    TitleInfo titleInfo = client.lookupWithTitleId(catalogKey);
}
```

The "lookupTitleInfo" service does not normally require authentication, but some services do.  In some cases, you may
even need to quickly switch between multiple user sessions.  The library makes it easy to make copies of a base
**SymwsClientConfig** object with different sessions.  The following code sample demonstrates how to place a hold on an
item for several different users at once:

```java
// Create a base SymwsClientConfig object.
SymwsClientConfig baseClientConfig = new SymwsClientConfig.Builder("myClientId")
        .rootUri("https://ils.example.com/symws")
        .build();

Map<String, String> users = new HashMap<>();
users.add("user1", "pin1");
users.add("user2", "pin2");
users.add("user3", "pin3");

for (Map.Entry entry : users.entrySet()) {
        // Since each user will require different credentials, we must create a new SymwsClientConfig and client for
        // each iteration.  We do this safely by using "withSession" to create a sessioned copy of the base
        // SymwsClientConfig created earlier.
        CreateMyHoldClient client = new CreateMyHoldClient(
                baseClientConfig.withSession(entry.getKey(), entry.getValue()),
                new CreateMyHoldClient.OptionalParams()
        );
        long holdKey = client.createTitleHold("123456");
}
```

### Using the `request` method in `SymwsUtil`

Should you wish to use a service which is not yet implemented by an existing client class, you may choose to call
`SymwsUtil.request(SymwsClientConfig, String, Map<String, List<Object>>)` to directly make a call to the needed
web service.

This method, and its companion method `requestWithoutRetry` (which will not attempt to restore an expired session), are
used by all the client classes, and will return a raw JAX-RS response with minimal validation.  It is generally safe to
use, but should you find yourself using it regularly, please consider making a feature request or submitting a pull
request to this project for a new client to be created to support your use case.

## Error Handling

This library offers fairly robust error handling.  Failures to connect to your SYMWS installation and failure responses
returned by SYMWS are both thrown as checked **SymwsClientException**s.  This exception provides incredibly useful
methods for developers needing to log or troubleshoot failed web service requests.

Basic input validation is also provided with this library to help protect developers from common mistakes such as
accidentally providing null values, blank strings, or negative numbers to the myriad of methods offered throughout
the library.  Again, reiterating what has been stated earlier, the goal of this library is to make it easy to use SYMWS.