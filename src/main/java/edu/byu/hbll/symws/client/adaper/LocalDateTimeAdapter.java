package edu.byu.hbll.symws.client.adaper;

import java.time.LocalDateTime;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/** {@link XmlAdapter} for marshalling and unmarshalling {@link LocalDateTime} objects. */
public class LocalDateTimeAdapter extends XmlAdapter<String, LocalDateTime> {

  @Override
  public LocalDateTime unmarshal(String v) {
    return LocalDateTime.parse(v);
  }

  @Override
  public String marshal(LocalDateTime v) {
    return v.toString();
  }
}
