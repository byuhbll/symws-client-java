package edu.byu.hbll.symws.client.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing a group member.
 *
 * <p>From the SYMWS documentation: "The GroupMemberInfo type displays information for each User
 * Group member."
 */
public class GroupMemberInfo {

  @XmlElement(name = "userID")
  private String userId;

  @XmlElement private String userName;

  @XmlElement(name = "userGroupResponsibilityPolicyID")
  private String userGroupResponsibilityPolicyId; // Optional

  @XmlElement private String userGroupResponsibilityPolicyDescription; // Optional
  @XmlElement private Boolean noticeMaster;

  @XmlElement(name = "userStatusID")
  private String userStatusId; // Optional

  @XmlElement private String userStatusDescription; // Optional
  @XmlElement private String userStatusMessage; // Optional
  @XmlElement private Integer userNumberOfBills; // Optional
  @XmlElement private String userEstimatedFines; // Optional
  @XmlElement private Integer userNumberOfOverdues; // Optional

  /**
   * Returns the userId.
   *
   * @return the userId
   */
  public String getUserId() {
    return userId;
  }

  /**
   * Returns the userName.
   *
   * @return the userName
   */
  public String getUserName() {
    return userName;
  }

  /**
   * Returns the userGroupResponsibilityPolicyId.
   *
   * @return the userGroupResponsibilityPolicyId
   */
  public String getUserGroupResponsibilityPolicyId() {
    return userGroupResponsibilityPolicyId;
  }

  /**
   * Returns the userGroupResponsibilityPolicyDescription.
   *
   * @return the userGroupResponsibilityPolicyDescription
   */
  public String getUserGroupResponsibilityPolicyDescription() {
    return userGroupResponsibilityPolicyDescription;
  }

  /**
   * Returns the noticeMaster.
   *
   * @return the noticeMaster
   */
  public Boolean getNoticeMaster() {
    return noticeMaster;
  }

  /**
   * Returns the userStatusId.
   *
   * @return the userStatusId
   */
  public String getUserStatusId() {
    return userStatusId;
  }

  /**
   * Returns the userStatusDescription.
   *
   * @return the userStatusDescription
   */
  public String getUserStatusDescription() {
    return userStatusDescription;
  }

  /**
   * Returns the userStatusMessage.
   *
   * @return the userStatusMessage
   */
  public String getUserStatusMessage() {
    return userStatusMessage;
  }

  /**
   * Returns the userNumberOfBills.
   *
   * @return the userNumberOfBills
   */
  public Integer getUserNumberOfBills() {
    return userNumberOfBills;
  }

  /**
   * Returns the userEstimatedFines.
   *
   * @return the userEstimatedFines
   */
  public String getUserEstimatedFines() {
    return userEstimatedFines;
  }

  /**
   * Returns the userNumberOfOverdues.
   *
   * @return the userNumberOfOverdues
   */
  public Integer getUserNumberOfOverdues() {
    return userNumberOfOverdues;
  }
}
