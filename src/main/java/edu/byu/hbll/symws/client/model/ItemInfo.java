package edu.byu.hbll.symws.client.model;

import java.time.LocalDateTime;
import javax.xml.bind.annotation.XmlElement;

/** JAXB bean representing item-level bibliographic data. */
public class ItemInfo {

  @XmlElement(name = "itemID")
  private String itemId;

  @XmlElement(name = "itemTypeID")
  private String itemTypeId;

  @XmlElement(name = "currentLocationID")
  private String currentLocationId;

  @XmlElement(name = "homeLocationID")
  private String homeLocationId;

  @XmlElement private LocalDateTime dueDate; // Optional
  @XmlElement private LocalDateTime recallDueDate; // Optional

  @XmlElement(name = "reshelvingLocationID")
  private String reshelvingLocationId; // Optional

  @XmlElement(name = "transitSourceLibraryID")
  private String transitSourceLibraryId; // Optional

  @XmlElement(name = "transitDestinationLibraryID")
  private String transitDestinationLibraryId; // Optional

  @XmlElement private String transitReason; // Optional
  @XmlElement private LocalDateTime transitDate; // Optional
  @XmlElement private Boolean chargeable;
  @XmlElement private Integer numberOfHolds; // Optional

  @XmlElement(name = "reserveCollectionID")
  private String reserveCollectionId; // Optional

  @XmlElement private String reserveCirculationRule; // Optional

  @XmlElement(name = "mediaDeskID")
  private String mediaDeskId; // Optional

  @XmlElement private Boolean fixedTimebooking; // Optional
  @XmlElement private String publicNote; // Optional
  @XmlElement private String staffNote; // Optional

  /**
   * Returns the itemId.
   *
   * @return the itemId
   */
  public String getItemId() {
    return itemId;
  }

  /**
   * Returns the itemTypeId.
   *
   * @return the itemTypeId
   */
  public String getItemTypeId() {
    return itemTypeId;
  }

  /**
   * Returns the currentLocationId.
   *
   * @return the currentLocationId
   */
  public String getCurrentLocationId() {
    return currentLocationId;
  }

  /**
   * Returns the homeLocationId.
   *
   * @return the homeLocationId
   */
  public String getHomeLocationId() {
    return homeLocationId;
  }

  /**
   * Returns the dueDate.
   *
   * @return the dueDate
   */
  public LocalDateTime getDueDate() {
    return dueDate;
  }

  /**
   * Returns the recallDueDate.
   *
   * @return the recallDueDate
   */
  public LocalDateTime getRecallDueDate() {
    return recallDueDate;
  }

  /**
   * Returns the reshelvingLocationId.
   *
   * @return the reshelvingLocationId
   */
  public String getReshelvingLocationId() {
    return reshelvingLocationId;
  }

  /**
   * Returns the transitSourceLibraryId.
   *
   * @return the transitSourceLibraryId
   */
  public String getTransitSourceLibraryId() {
    return transitSourceLibraryId;
  }

  /**
   * Returns the transitDestinationLibraryId.
   *
   * @return the transitDestinationLibraryId
   */
  public String getTransitDestinationLibraryId() {
    return transitDestinationLibraryId;
  }

  /**
   * Returns the transitReason.
   *
   * @return the transitReason
   */
  public String getTransitReason() {
    return transitReason;
  }

  /**
   * Returns the transitDate.
   *
   * @return the transitDate
   */
  public LocalDateTime getTransitDate() {
    return transitDate;
  }

  /**
   * Returns the chargeable.
   *
   * @return the chargeable
   */
  public Boolean getChargeable() {
    return chargeable;
  }

  /**
   * Returns the numberOfHolds.
   *
   * @return the numberOfHolds
   */
  public Integer getNumberOfHolds() {
    return numberOfHolds;
  }

  /**
   * Returns the reserveCollectionId.
   *
   * @return the reserveCollectionId
   */
  public String getReserveCollectionId() {
    return reserveCollectionId;
  }

  /**
   * Returns the reserveCirculationRule.
   *
   * @return the reserveCirculationRule
   */
  public String getReserveCirculationRule() {
    return reserveCirculationRule;
  }

  /**
   * Returns the mediaDeskId.
   *
   * @return the mediaDeskId
   */
  public String getMediaDeskId() {
    return mediaDeskId;
  }

  /**
   * Returns the fixedTimebooking.
   *
   * @return the fixedTimebooking
   */
  public Boolean getFixedTimebooking() {
    return fixedTimebooking;
  }

  /**
   * Returns the publicNote.
   *
   * @return the publicNote
   */
  public String getPublicNote() {
    return publicNote;
  }

  /**
   * Returns the staffNote.
   *
   * @return the staffNote
   */
  public String getStaffNote() {
    return staffNote;
  }
}
