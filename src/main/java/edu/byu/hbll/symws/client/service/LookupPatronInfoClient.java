package edu.byu.hbll.symws.client.service;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.SymwsClientException;
import edu.byu.hbll.symws.client.SymwsUtil;
import edu.byu.hbll.symws.client.model.CheckoutInfoFilter;
import edu.byu.hbll.symws.client.model.FeeInfo;
import edu.byu.hbll.symws.client.model.FeeInfoFilter;
import edu.byu.hbll.symws.client.model.GroupInfo;
import edu.byu.hbll.symws.client.model.HoldInfoFilter;
import edu.byu.hbll.symws.client.model.LookupPatronInfoResponse;
import edu.byu.hbll.symws.client.model.PatronAddressInfo;
import edu.byu.hbll.symws.client.model.PatronCheckoutHistoryInfo;
import edu.byu.hbll.symws.client.model.PatronCheckoutInfo;
import edu.byu.hbll.symws.client.model.PatronCirculationInfo;
import edu.byu.hbll.symws.client.model.PatronHoldInfo;
import edu.byu.hbll.symws.client.model.PatronInfo;
import edu.byu.hbll.symws.client.model.PatronStatusInfo;
import edu.byu.hbll.symws.client.model.SMSInfo;
import edu.byu.hbll.symws.client.model.UserSuspensionInfo;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * Client for looking up account information about a SYMWS user.
 *
 * <p>This class is thread-safe, conditional upon the safety of the underlying service itself.
 */
public class LookupPatronInfoClient {

  /**
   * The path of this service, relative to the root URI provided in {@link
   * SymwsClientConfig#getRootUri()}.
   */
  public static final String SERVICE_PATH = "/rest/patron/lookupPatronInfo";

  /** The name/key of the "userId" query parameter. */
  public static final String QUERYPARAM_USER_ID = "userID";

  /** The name/key of the "alternateId" query parameter. */
  public static final String QUERYPARAM_ALTERNATE_ID = "alternateID";

  /** The name/key of the "webAuthId" query parameter. */
  public static final String QUERYPARAM_WEB_AUTH_ID = "webAuthID";

  private final SymwsClientConfig symwsClientConfig;
  private final MultivaluedMap<String, Object> optionalParams;

  /**
   * Constructs a new instance.
   *
   * @param symwsClientConfig the client configuration to use for all requests made using this
   *     client
   * @param optionalParams the optional parameters to send with all requests made using this client
   */
  public LookupPatronInfoClient(
      SymwsClientConfig symwsClientConfig, OptionalParams optionalParams) {
    this.symwsClientConfig = symwsClientConfig;
    this.optionalParams = (optionalParams == null ? new OptionalParams() : optionalParams).asMap();
  }

  /**
   * Lookup the account information for the user identified by the provided userId.
   *
   * @param userId the userId of the user being looked up
   * @return the account information
   * @throws SymwsClientException if the request is not successful
   */
  public LookupPatronInfoResponse lookupWithUserId(String userId) throws SymwsClientException {
    SymwsUtil.requireNonBlank(userId, "userId cannot be null or blank");
    return lookupPatronInfo(QUERYPARAM_USER_ID, userId);
  }

  /**
   * Lookup the account information for the user identified by the provided alternateId.
   *
   * @param alternateId the alternateId of the user being looked up
   * @return the account information
   * @throws SymwsClientException if the request is not successful
   */
  public LookupPatronInfoResponse lookupWithAlternateId(String alternateId)
      throws SymwsClientException {
    SymwsUtil.requireNonBlank(alternateId, "alternateId cannot be null or blank");
    return lookupPatronInfo(QUERYPARAM_ALTERNATE_ID, alternateId);
  }

  /**
   * Lookup the account information for the user identified by the provided webAuthId.
   *
   * @param webAuthId the webAuthId of the user being looked up
   * @return the account information
   * @throws SymwsClientException if the request is not successful
   */
  public LookupPatronInfoResponse lookupWithWebAuthId(String webAuthId)
      throws SymwsClientException {
    SymwsUtil.requireNonBlank(webAuthId, "webAuthId cannot be null or blank");
    return lookupPatronInfo(QUERYPARAM_WEB_AUTH_ID, webAuthId);
  }

  /**
   * Backend method used in common by other methods to actually call the lookupPatronInfo service.
   *
   * @param idType the type of identifier being used to lookup the user; should be {@link
   *     #QUERYPARAM_USER_ID}, {@link #QUERYPARAM_ALTERNATE_ID}, or {@link #QUERYPARAM_WEB_AUTH_ID}
   * @param idValue the identifier value corresponding with the provided identifierKey
   * @return the account information
   * @throws SymwsClientException if the request is not successful
   */
  private LookupPatronInfoResponse lookupPatronInfo(String idType, String idValue)
      throws SymwsClientException {
    // Generics on right side must be explicitly declared to prevent constructor ambiguity.
    MultivaluedMap<String, Object> queryParams =
        new MultivaluedHashMap<String, Object>(optionalParams);
    queryParams.add(idType, idValue);

    Response response = SymwsUtil.request(symwsClientConfig, SERVICE_PATH, queryParams);
    return SymwsUtil.parseResponse(response, LookupPatronInfoResponse.class);
  }

  /**
   * Optional query parameters which can be supplied with a lookupPatronInfo or lookupMyAccountInfo
   * request.
   */
  public static class OptionalParams {

    /** The name/key of the "includePatronInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_PATRON_INFO = "includePatronInfo";

    /** The name/key of the "includePatronCirculationInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_PATRON_CIRCULATION_INFO =
        "includePatronCirculationInfo";

    /** The name/key of the "includePatronAddressInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_PATRON_ADDRESS_INFO = "includePatronAddressInfo";

    /** The name/key of the "includePatronCheckoutInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_PATRON_CHECKOUT_INFO =
        "includePatronCheckoutInfo";

    /** The name/key of the "includePatronHistoryInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_PATRON_CHECKOUT_HISTORY_INFO =
        "includePatronHistoryInfo";

    /** The name/key of the "includePatronHoldInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_PATRON_HOLD_INFO = "includePatronHoldInfo";

    /** The name/key of the "includeFeeInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_FEE_INFO = "includeFeeInfo";

    /** The name/key of the "includePatronStatusInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_PATRON_STATUS_INFO = "includePatronStatusInfo";

    /** The name/key of the "includeUserGroupInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_USER_GROUP_INFO = "includeUserGroupInfo";

    /** The name/key of the "includeUserSuspensionInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_USER_SUSPENSION_INFO =
        "includeUserSuspensionInfo";

    /** The name/key of the "stationLibraryId" query parameter. */
    public static final String QUERYPARAM_STATION_LIBRARY_ID = "stationLibraryId";

    /** The name/key of the "includeSMSInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_SMS_INFO = "includeSMSInfo";

    /** The name/key of the "includePhoto" query parameter. */
    public static final String QUERYPARAM_INCLUDE_PHOTO = "includePhoto";

    private boolean patronInfo = false;
    private boolean patronCirculationInfo = false;
    private boolean patronAddressInfo = false;
    private CheckoutInfoFilter patronCheckoutInfo = null;
    private boolean patronCheckoutHistoryInfo = false;
    private HoldInfoFilter patronHoldInfo = null;
    private FeeInfoFilter feeInfo = null;
    private boolean patronStatusInfo = false;
    private boolean userGroupInfo = false;
    private boolean userSuspensionInfo = false;
    private boolean smsInfo = false;
    private boolean photo = false;

    /**
     * Specifies that the response should include a {@link PatronInfo} object.
     *
     * @return this object
     */
    public OptionalParams includePatronInfo() {
      patronInfo = true;
      return this;
    }

    /**
     * Specifies that the response should include a {@link PatronCirculationInfo} object.
     *
     * @return this object
     */
    public OptionalParams includePatronCirculationInfo() {
      patronCirculationInfo = true;
      return this;
    }

    /**
     * Specifies that the response should include a {@link PatronAddressInfo} object.
     *
     * @return this object
     */
    public OptionalParams includePatronAddressInfo() {
      patronAddressInfo = true;
      return this;
    }

    /**
     * Specifies that the response should include {@link PatronCheckoutInfo} objects and defines
     * which checkouts should be included.
     *
     * @param filter which checkouts/charges should be included
     * @return this object
     */
    public OptionalParams includePatronCheckoutInfo(CheckoutInfoFilter filter) {
      patronCheckoutInfo = filter;
      return this;
    }

    /**
     * Specifies that the response should include a {@link PatronCheckoutHistoryInfo} object.
     *
     * @return this object
     */
    public OptionalParams includePatronCheckoutHistoryInfo() {
      patronCheckoutHistoryInfo = true;
      return this;
    }

    /**
     * Specifies that the response should include {@link PatronHoldInfo} objects and defines which
     * holds should be included.
     *
     * @param filter which holds should be included
     * @return this object
     */
    public OptionalParams includePatronHoldInfo(HoldInfoFilter filter) {
      patronHoldInfo = filter;
      return this;
    }

    /**
     * Specifies that the response should include {@link FeeInfo} objects and defines which fees
     * should be included.
     *
     * @param filter which fees should be included
     * @return this object
     */
    public OptionalParams includeFeeInfo(FeeInfoFilter filter) {
      feeInfo = filter;
      return this;
    }

    /**
     * Specifies that the response should include a {@link PatronStatusInfo} object.
     *
     * @return this object
     */
    public OptionalParams includePatronStatusInfo() {
      patronStatusInfo = true;
      return this;
    }

    /**
     * Specifies that the response should include a {@link GroupInfo} object.
     *
     * @return this object
     */
    public OptionalParams includeUserGroupInfo() {
      userGroupInfo = true;
      return this;
    }

    /**
     * Specifies that the response should include a {@link UserSuspensionInfo} object.
     *
     * @return this object
     */
    public OptionalParams includeUserSuspensionInfo() {
      userSuspensionInfo = true;
      return this;
    }

    /**
     * Specifies that the response should include a {@link SMSInfo} object.
     *
     * @return this object
     */
    public OptionalParams includeSmsInfo() {
      smsInfo = true;
      return this;
    }

    /**
     * Specifies that the response should include photo data.
     *
     * @return this object
     */
    public OptionalParams includePhoto() {
      photo = true;
      return this;
    }

    /**
     * Returns the non-null members of this object as an multivalued map.
     *
     * @return the map containing query parameters
     */
    public MultivaluedMap<String, Object> asMap() {
      MultivaluedMap<String, Object> map = new MultivaluedHashMap<>();

      map.add(QUERYPARAM_INCLUDE_PATRON_INFO, patronInfo);
      map.add(QUERYPARAM_INCLUDE_PATRON_CIRCULATION_INFO, patronCirculationInfo);
      map.add(QUERYPARAM_INCLUDE_PATRON_ADDRESS_INFO, patronAddressInfo);
      map.add(QUERYPARAM_INCLUDE_PATRON_CHECKOUT_HISTORY_INFO, patronCheckoutHistoryInfo);
      map.add(QUERYPARAM_INCLUDE_PATRON_STATUS_INFO, patronStatusInfo);
      map.add(QUERYPARAM_INCLUDE_USER_GROUP_INFO, userGroupInfo);
      map.add(QUERYPARAM_INCLUDE_USER_SUSPENSION_INFO, userSuspensionInfo);
      map.add(QUERYPARAM_INCLUDE_SMS_INFO, smsInfo);
      map.add(QUERYPARAM_INCLUDE_PHOTO, photo);

      if (patronCheckoutInfo != null) {
        map.add(QUERYPARAM_INCLUDE_PATRON_CHECKOUT_INFO, patronCheckoutInfo);
      }
      if (patronHoldInfo != null) {
        map.add(QUERYPARAM_INCLUDE_PATRON_HOLD_INFO, patronHoldInfo);
      }
      if (feeInfo != null) {
        map.add(QUERYPARAM_INCLUDE_FEE_INFO, feeInfo);
      }

      return map;
    }
  }
}
