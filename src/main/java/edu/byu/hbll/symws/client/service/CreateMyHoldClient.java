package edu.byu.hbll.symws.client.service;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.SymwsClientException;
import edu.byu.hbll.symws.client.SymwsUtil;
import edu.byu.hbll.symws.client.service.PlaceMyHoldClient.OptionalParams;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * Client for creating a hold belonging to the current user session in SYMWS.
 *
 * <p>This class is thread-safe, conditional upon the safety of the underlying service itself.
 */
public class CreateMyHoldClient {

  /**
   * The path of this service, relative to the root URI provided in {@link
   * SymwsClientConfig#getRootUri()}.
   */
  public static final String SERVICE_PATH = "/rest/patron/createMyHold";

  /** The name/key of the "titleKey" query parameter. */
  public static final String QUERYPARAM_TITLE_KEY = "titleKey";

  /** The name/key of the "itemID" query parameter. */
  public static final String QUERYPARAM_ITEM_ID = "itemID";

  /** The name/key of the "pickupLibraryID" query parameter. */
  public static final String QUERYPARAM_PICKUP_LIBRARY_ID = "pickupLibraryID";

  /** The name/key of the "mailServiceID" query parameter. */
  public static final String QUERYPARAM_MAIL_SERVICE_ID = "mailServiceID";

  private final SymwsClientConfig symwsClientConfig;
  private final MultivaluedMap<String, Object> optionalParams;

  /**
   * Constructs a new instance.
   *
   * @param symwsClientConfig the client configuration to use for all requests made using this
   *     client
   * @param optionalParams the optional parameters to send with all requests made using this client
   */
  public CreateMyHoldClient(SymwsClientConfig symwsClientConfig, OptionalParams optionalParams) {
    this.symwsClientConfig = symwsClientConfig;
    this.optionalParams = (optionalParams == null ? new OptionalParams() : optionalParams).asMap();
  }

  /**
   * Creates a new title-level Symphony hold to be mailed upon fulfillment.
   *
   * @param titleKey the title or catalog key of the material being requested
   * @return the hold key of the newly created hold
   * @throws SymwsClientException if the web service returns an error
   */
  public long createTitleHold(long titleKey) throws SymwsClientException {
    if (titleKey < 0) {
      throw new IllegalArgumentException("titleKey must be a positive whole number");
    }
    return createHold(QUERYPARAM_TITLE_KEY, Long.toString(titleKey));
  }

  /**
   * Creates a new item/copy-level Symphony hold to be mailed upon fulfillment.
   *
   * @param itemId the item ID or barcode of the material being requested
   * @return the hold key of the newly created hold
   * @throws SymwsClientException if the web service returns an error
   */
  public long createItemHold(String itemId) throws SymwsClientException {
    SymwsUtil.requireNonBlank(itemId, "itemId cannot be null or blank");
    return createHold(QUERYPARAM_ITEM_ID, itemId);
  }

  /**
   * Backend method used in common by other methods to actually call the createMyHold service.
   *
   * @param idType the type of hold being placed; should be {@link #QUERYPARAM_TITLE_KEY} or {@link
   *     #QUERYPARAM_ITEM_ID}
   * @param idValue the titleKey or itemID corresponding with the appropriate identifierKey
   * @return the hold key of the newly created hold
   * @throws SymwsClientException if the web service returns an error
   */
  private long createHold(String idType, String idValue) throws SymwsClientException {
    // Generics on right side must be explicitly declared to prevent constructor ambiguity.
    MultivaluedMap<String, Object> queryParams =
        new MultivaluedHashMap<String, Object>(optionalParams);
    queryParams.add(idType, idValue);

    Response response = SymwsUtil.request(symwsClientConfig, SERVICE_PATH, queryParams);
    return response.readEntity(Long.class);
  }
}
