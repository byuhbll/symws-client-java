package edu.byu.hbll.symws.client;

import edu.byu.hbll.symws.client.model.SymwsFault;
import java.net.URI;
import javax.ws.rs.core.Response;

/**
 * Thrown when a SYMWS request fails.
 *
 * <p>Such failures could include:
 *
 * <ul>
 *   <li>404 Not Found error
 *   <li>Calling a privileged service without a valid session
 *   <li>Attempting to retrieve data that does not exist
 * </ul>
 *
 * <p>In the case of a general HTTP error (such as a 404) there may be no additional explanation.
 * However, in most cases, a {@link SymwsFault} will be attached to the exception instance,
 * providing additional detail as to the failure and its cause.
 */
public class SymwsClientException extends Exception {

  private static final long serialVersionUID = 1L;

  private final URI uri;
  private final int responseStatus;
  private final String responseBody;
  private final SymwsFault fault;

  /** Constructs a new instance with no information. */
  public SymwsClientException() {
    super();
    uri = null;
    responseStatus = 0;
    responseBody = null;
    fault = null;
  }

  /**
   * Constructs a new instance with only a message string to provide information.
   *
   * @param message the message to attach to this exception
   */
  public SymwsClientException(String message) {
    super(message);
    uri = null;
    responseStatus = 0;
    responseBody = null;
    fault = null;
  }

  /**
   * Constructs a new instance with basic information about the HTTP request and response.
   *
   * @param uri the URI of the service endpoint that was called
   * @param cause the cause of this exception
   */
  public SymwsClientException(URI uri, Throwable cause) {
    super(cause);
    this.uri = uri;
    this.responseStatus = 0;
    this.responseBody = null;
    this.fault = null;
  }

  /**
   * Constructs a new instance with basic information about the HTTP request and response.
   *
   * @param uri the URI of the service endpoint that was called
   * @param response the JAX-RS response returned by the service; this {@link Response} will be
   *     buffered during construction
   */
  public SymwsClientException(URI uri, Response response) {
    super();
    this.uri = uri;
    this.responseStatus = response.getStatus();
    response.bufferEntity();
    this.responseBody = response.readEntity(String.class);
    this.fault = parseFault(response);
  }

  /**
   * Constructs a new instance with basic information about the HTTP request and response.
   *
   * @param uri the URI of the service endpoint that was called
   * @param response the JAX-RS response returned by the service; this {@link Response} will be
   *     buffered during construction
   * @param cause the cause of this exception
   */
  public SymwsClientException(URI uri, Response response, Throwable cause) {
    super(cause);
    this.uri = uri;
    this.responseStatus = response.getStatus();
    response.bufferEntity();
    this.responseBody = response.readEntity(String.class);
    this.fault = parseFault(response);
  }

  /**
   * Constructs a new instance with basic information about the HTTP request and response.
   *
   * @param uri the URI of the service endpoint that was called
   * @param response the JAX-RS response returned by the service; this {@link Response} will be
   *     buffered during construction
   * @param message the message to attach to this exception
   */
  public SymwsClientException(URI uri, Response response, String message) {
    super(message);
    this.uri = uri;
    this.responseStatus = response.getStatus();
    response.bufferEntity();
    this.responseBody = response.readEntity(String.class);
    this.fault = parseFault(response);
  }

  /**
   * Indicates whether this instance has an attached SymwsFault.
   *
   * @return true if this instance has a SymwsFault, false otherwise
   */
  public boolean hasFault() {
    return fault != null;
  }

  /**
   * Returns the uri.
   *
   * @return the uri
   */
  public URI getUri() {
    return uri;
  }

  /**
   * Returns the responseStatus.
   *
   * @return the responseStatus
   */
  public int getResponseStatus() {
    return responseStatus;
  }

  /**
   * Returns the responseBody.
   *
   * @return the responseBody
   */
  public String getResponseBody() {
    return responseBody;
  }

  /**
   * Returns the fault.
   *
   * @return the fault
   */
  public SymwsFault getFault() {
    return fault;
  }

  /**
   * Attempts to parse and return a SYMWS fault from the given response.
   *
   * @param response the JAX-RS response to parse
   * @return the fault if one is found, or null otherwise
   */
  private SymwsFault parseFault(Response response) {
    try {
      return SymwsUtil.parseResponse(response, SymwsFault.class);
    } catch (Exception e) {
      // The response didn't contain the error messages we were hoping to parse.
      return null;
    }
  }
}
