/** JAXB namespaces and adapters needed for this package to parse properly. */
@XmlJavaTypeAdapters({
  @XmlJavaTypeAdapter(value = LocalDateAdapter.class, type = LocalDate.class),
  @XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class, type = LocalDateTime.class),
  @XmlJavaTypeAdapter(value = CurrencyAdapter.class, type = Currency.class)
})
@XmlAccessorType(XmlAccessType.FIELD)
package edu.byu.hbll.symws.client.model;

import edu.byu.hbll.symws.client.adaper.CurrencyAdapter;
import edu.byu.hbll.symws.client.adaper.LocalDateAdapter;
import edu.byu.hbll.symws.client.adaper.LocalDateTimeAdapter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Currency;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
