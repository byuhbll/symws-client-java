package edu.byu.hbll.symws.client.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/** JAXB bean representing a MARC holdings entry. */
public class MarcHoldingsInfo {

  @XmlElement(name = "holdingLibraryID")
  private String holdingLibraryId;

  @XmlElement(name = "MarcEntryInfo")
  private List<MarcEntryInfo> marcEntryInfo = new ArrayList<>();

  /**
   * Returns the holdingLibraryId.
   *
   * @return the holdingLibraryId
   */
  public String getHoldingLibraryId() {
    return holdingLibraryId;
  }

  /**
   * Returns the marcEntryInfo.
   *
   * @return the marcEntryInfo
   */
  public List<MarcEntryInfo> getMarcEntryInfo() {
    return marcEntryInfo;
  }
}
