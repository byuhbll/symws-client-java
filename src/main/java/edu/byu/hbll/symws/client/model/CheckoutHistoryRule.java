package edu.byu.hbll.symws.client.model;

/**
 * Enumerated list of possible checkout history rules.
 *
 * <p>From the SYMWS documentation: "The CheckoutHistoryRule type displays information about how
 * charge history is kept for a specific user."
 */
public enum CheckoutHistoryRule {
  NOHISTORY,
  CIRCRULE,
  ALLCHARGES;
}
