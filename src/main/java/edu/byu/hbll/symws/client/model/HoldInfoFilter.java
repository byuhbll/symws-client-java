package edu.byu.hbll.symws.client.model;

/**
 * Enumeration of possible hold types.
 *
 * <p>From the SYMWS documentation: "The HoldInfoFilter type contains enumeration values that you
 * can use in the lookupMyAccountInfo operation request to limit your results when you retrieve hold
 * information."
 */
public enum HoldInfoFilter {
  ALL,
  AVAILABLE,
  COUNT_ONLY,
  ACTIVE,
  INACTIVE,
  RECALLED,
  RESERVE,
  UNAVAILABLE;
}
