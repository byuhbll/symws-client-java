package edu.byu.hbll.symws.client.model;

/** Enumeration of possible hold ranges/scopes. */
public enum HoldRange {
  LIBRARY,
  GROUP,
  SYSTEM;
}
