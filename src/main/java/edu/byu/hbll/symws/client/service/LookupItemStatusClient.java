package edu.byu.hbll.symws.client.service;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.SymwsClientException;
import edu.byu.hbll.symws.client.SymwsUtil;
import edu.byu.hbll.symws.client.model.ItemStatusInfo;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * Client for lookup up item status in SYMWS.
 *
 * <p>This class is thread-safe, conditional upon the safety of the underlying service itself.
 */
public class LookupItemStatusClient {

  /**
   * The path of this service, relative to the root URI provided in {@link
   * SymwsClientConfig#getRootUri()}.
   */
  public static final String SERVICE_PATH = "/rest/circulation/lookupItemStatus";

  /** The name/key of the "itemID" query parameter. */
  public static final String QUERYPARAM_ITEM_ID = "itemID";

  private final SymwsClientConfig symwsClientConfig;

  /**
   * Constructs a new instance.
   *
   * @param symwsClientConfig the client configuration to use for all requests made using this
   *     client; this client configuration MUST have an active session.
   */
  public LookupItemStatusClient(SymwsClientConfig symwsClientConfig) {
    this.symwsClientConfig = symwsClientConfig;
  }

  /**
   * Looks up the current status of the given item in Symphony.
   *
   * @param itemId the item ID or barcode of the item to lookup
   * @return the item details returned by the service
   * @throws SymwsClientException if the request is not successful
   */
  public ItemStatusInfo lookup(String itemId) throws SymwsClientException {
    MultivaluedMap<String, Object> queryParams = new MultivaluedHashMap<>(1);
    queryParams.add(
        QUERYPARAM_ITEM_ID, SymwsUtil.requireNonBlank(itemId, "itemId cannot be null or blank"));
    Response response = SymwsUtil.request(symwsClientConfig, SERVICE_PATH, queryParams);
    return SymwsUtil.parseResponse(response, ItemStatusInfo.class);
  }
}
