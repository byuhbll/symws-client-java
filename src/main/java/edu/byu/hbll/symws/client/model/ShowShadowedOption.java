package edu.byu.hbll.symws.client.model;

/**
 * Enumeration of the possible options for displaying "shadowed" catalog data.
 *
 * <p>From the SYMWS documentation: "The ShowShadowedOption type displays information about shadowed
 * items."
 */
public enum ShowShadowedOption {

  /** Displays only unshadowed data. */
  NONE,

  /** Displays only shadowed data. */
  ONLY,

  /** Displays both shadowed and unshadowed data. */
  BOTH;
}
