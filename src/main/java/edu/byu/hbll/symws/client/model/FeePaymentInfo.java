package edu.byu.hbll.symws.client.model;

import java.time.LocalDate;
import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing the payment of a patron fee.
 *
 * <p>From the SYMWS documentation: "The FeePaymentInfo type displays information about payments
 * that a patron has paid on fees and fines."
 */
public class FeePaymentInfo {

  @XmlElement private String transactionId;

  @XmlElement(name = "paymentLibraryID")
  private String paymentLibraryId; // Optional

  @XmlElement private String paymentLibraryDescription; // Optional
  @XmlElement private LocalDate paymentDate; // Optional
  @XmlElement private String paymentAmount;

  @XmlElement(name = "paymentTypeID")
  private String paymentTypeId;

  @XmlElement private String paymentTypeDescription; // Optional

  /**
   * Returns the transactionId.
   *
   * @return the transactionId
   */
  public String getTransactionId() {
    return transactionId;
  }

  /**
   * Returns the paymentLibraryId.
   *
   * @return the paymentLibraryId
   */
  public String getPaymentLibraryId() {
    return paymentLibraryId;
  }

  /**
   * Returns the paymentLibraryDescription.
   *
   * @return the paymentLibraryDescription
   */
  public String getPaymentLibraryDescription() {
    return paymentLibraryDescription;
  }

  /**
   * Returns the paymentDate.
   *
   * @return the paymentDate
   */
  public LocalDate getPaymentDate() {
    return paymentDate;
  }

  /**
   * Returns the paymentAmount.
   *
   * @return the paymentAmount
   */
  public String getPaymentAmount() {
    return paymentAmount;
  }

  /**
   * Returns the paymentTypeId.
   *
   * @return the paymentTypeId
   */
  public String getPaymentTypeId() {
    return paymentTypeId;
  }

  /**
   * Returns the paymentTypeDescription.
   *
   * @return the paymentTypeDescription
   */
  public String getPaymentTypeDescription() {
    return paymentTypeDescription;
  }
}
