package edu.byu.hbll.symws.client.model;

import java.time.LocalDate;
import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing patron status information.
 *
 * <p>From the SYMWS documentation: "The PatronStatusInfo type displays information specific to the
 * patron's status."
 */
public class PatronStatusInfo {

  @XmlElement private LocalDate datePrivilegeExpires; // Optional

  @XmlElement(name = "statusID")
  private String statusId; // Optional

  @XmlElement private String statusDescription; // Optional
  @XmlElement private StatusType statusType; // Optional
  @XmlElement private String statusMessage; // Optional
  @XmlElement private Boolean routingAllowed;
  @XmlElement private Boolean userHasAlerts;

  /**
   * Returns the datePrivilegeExpires.
   *
   * @return the datePrivilegeExpires
   */
  public LocalDate getDatePrivilegeExpires() {
    return datePrivilegeExpires;
  }

  /**
   * Returns the statusId.
   *
   * @return the statusId
   */
  public String getStatusId() {
    return statusId;
  }

  /**
   * Returns the statusDescription.
   *
   * @return the statusDescription
   */
  public String getStatusDescription() {
    return statusDescription;
  }

  /**
   * Returns the statusType.
   *
   * @return the statusType
   */
  public StatusType getStatusType() {
    return statusType;
  }

  /**
   * Returns the statusMessage.
   *
   * @return the statusMessage
   */
  public String getStatusMessage() {
    return statusMessage;
  }

  /**
   * Returns the routingAllowed.
   *
   * @return the routingAllowed
   */
  public Boolean getRoutingAllowed() {
    return routingAllowed;
  }

  /**
   * Returns the userHasAlerts.
   *
   * @return the userHasAlerts
   */
  public Boolean getUserHasAlerts() {
    return userHasAlerts;
  }
}
