package edu.byu.hbll.symws.client.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing a user group.
 *
 * <p>From the SYMWS documentation: "The GroupInfo type displays information about a particular user
 * group."
 */
public class GroupInfo {

  @XmlElement private Long groupName;
  @XmlElement private Integer numberOfGroupMembers;
  @XmlElement private LocalDate dateGroupCreated;

  @XmlElement(name = "groupStatusID")
  private String groupStatusId; // Optional

  @XmlElement private String groupStatusDescription; // Optional
  @XmlElement private String groupStatusMessage; // Optional

  @XmlElement(name = "userGroupResponsibilityPolicyID")
  private String userGroupResponsibilityPolicyId;

  @XmlElement private String userGroupResponsibilityPolicyDescription;

  @XmlElement(name = "userGroupResponsibilityPolicyLevelID")
  private String userGroupResponsibilityLevelId;

  @XmlElement(name = "userGroupResponsibilityPolicyTypeID")
  private String userGroupResponsibilityTypeId;

  @XmlElement private Boolean noticeMaster;
  @XmlElement private Boolean allowedDisplayGroupCheckouts;
  @XmlElement private Boolean allowedDisplayGroupHolds;
  @XmlElement private Boolean allowedCheckoutGroupHolds;
  @XmlElement private Boolean allowedDisplayGroupFees;
  @XmlElement private Boolean allowedPayGroupFees;
  @XmlElement private Boolean allowedGroupChoice;
  @XmlElement private List<GroupMemberCheckoutInfo> groupMemberCheckoutInfo = new ArrayList<>();
  @XmlElement private List<GroupMemberHoldInfo> groupMemberHoldInfo = new ArrayList<>();
  @XmlElement private List<GroupMemberFeeInfo> groupMemberFeeInfo = new ArrayList<>();
  @XmlElement private List<GroupMemberInfo> groupMemberInfo = new ArrayList<>();

  /**
   * Returns the groupName.
   *
   * @return the groupName
   */
  public Long getGroupName() {
    return groupName;
  }

  /**
   * Returns the numberOfGroupMembers.
   *
   * @return the numberOfGroupMembers
   */
  public Integer getNumberOfGroupMembers() {
    return numberOfGroupMembers;
  }

  /**
   * Returns the dateGroupCreated.
   *
   * @return the dateGroupCreated
   */
  public LocalDate getDateGroupCreated() {
    return dateGroupCreated;
  }

  /**
   * Returns the groupStatusId.
   *
   * @return the groupStatusId
   */
  public String getGroupStatusId() {
    return groupStatusId;
  }

  /**
   * Returns the groupStatusDescription.
   *
   * @return the groupStatusDescription
   */
  public String getGroupStatusDescription() {
    return groupStatusDescription;
  }

  /**
   * Returns the groupStatusMessage.
   *
   * @return the groupStatusMessage
   */
  public String getGroupStatusMessage() {
    return groupStatusMessage;
  }

  /**
   * Returns the userGroupResponsibilityPolicyId.
   *
   * @return the userGroupResponsibilityPolicyId
   */
  public String getUserGroupResponsibilityPolicyId() {
    return userGroupResponsibilityPolicyId;
  }

  /**
   * Returns the userGroupResponsibilityPolicyDescription.
   *
   * @return the userGroupResponsibilityPolicyDescription
   */
  public String getUserGroupResponsibilityPolicyDescription() {
    return userGroupResponsibilityPolicyDescription;
  }

  /**
   * Returns the userGroupResponsibilityLevelId.
   *
   * @return the userGroupResponsibilityLevelId
   */
  public String getUserGroupResponsibilityLevelId() {
    return userGroupResponsibilityLevelId;
  }

  /**
   * Returns the userGroupResponsibilityTypeId.
   *
   * @return the userGroupResponsibilityTypeId
   */
  public String getUserGroupResponsibilityTypeId() {
    return userGroupResponsibilityTypeId;
  }

  /**
   * Returns the noticeMaster.
   *
   * @return the noticeMaster
   */
  public Boolean getNoticeMaster() {
    return noticeMaster;
  }

  /**
   * Returns the allowedDisplayGroupCheckouts.
   *
   * @return the allowedDisplayGroupCheckouts
   */
  public Boolean getAllowedDisplayGroupCheckouts() {
    return allowedDisplayGroupCheckouts;
  }

  /**
   * Returns the allowedDisplayGroupHolds.
   *
   * @return the allowedDisplayGroupHolds
   */
  public Boolean getAllowedDisplayGroupHolds() {
    return allowedDisplayGroupHolds;
  }

  /**
   * Returns the allowedCheckoutGroupHolds.
   *
   * @return the allowedCheckoutGroupHolds
   */
  public Boolean getAllowedCheckoutGroupHolds() {
    return allowedCheckoutGroupHolds;
  }

  /**
   * Returns the allowedDisplayGroupFees.
   *
   * @return the allowedDisplayGroupFees
   */
  public Boolean getAllowedDisplayGroupFees() {
    return allowedDisplayGroupFees;
  }

  /**
   * Returns the allowedPayGroupFees.
   *
   * @return the allowedPayGroupFees
   */
  public Boolean getAllowedPayGroupFees() {
    return allowedPayGroupFees;
  }

  /**
   * Returns the allowedGroupChoice.
   *
   * @return the allowedGroupChoice
   */
  public Boolean getAllowedGroupChoice() {
    return allowedGroupChoice;
  }

  /**
   * Returns the groupMemberCheckoutInfo.
   *
   * @return the groupMemberCheckoutInfo
   */
  public List<GroupMemberCheckoutInfo> getGroupMemberCheckoutInfo() {
    return groupMemberCheckoutInfo;
  }

  /**
   * Returns the groupMemberHoldInfo.
   *
   * @return the groupMemberHoldInfo
   */
  public List<GroupMemberHoldInfo> getGroupMemberHoldInfo() {
    return groupMemberHoldInfo;
  }

  /**
   * Returns the groupMemberFeeInfo.
   *
   * @return the groupMemberFeeInfo
   */
  public List<GroupMemberFeeInfo> getGroupMemberFeeInfo() {
    return groupMemberFeeInfo;
  }

  /**
   * Returns the groupMemberInfo.
   *
   * @return the groupMemberInfo
   */
  public List<GroupMemberInfo> getGroupMemberInfo() {
    return groupMemberInfo;
  }
}
