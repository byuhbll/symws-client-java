package edu.byu.hbll.symws.client;

/** Utility class enumerating the custom headers used by SYMWS. */
public final class SymwsHeaders {

  /**
   * Specifies the client ID authorizing the request (the value associated with this header must be
   * included in the enumeration of authorized clients in the license file attached to the SYMWS
   * installation).
   */
  public static final String X_SIRS_CLIENT_ID = "x-sirs-clientID";

  /** Specifies the session token authorizing the request. */
  public static final String X_SIRS_SESSION_TOKEN = "x-sirs-sessionToken";

  /**
   * Constructs a new instance; since this class is a utility class, this constructor will never be
   * used.
   */
  private SymwsHeaders() {
    // Do nothing.
  }
}
