package edu.byu.hbll.symws.client.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/** JAXB bean representing item bibliographic information. */
public class BibliographicInfo {

  @XmlElement(name = "MarcEntryInfo")
  private List<MarcEntryInfo> marcEntryInfo = new ArrayList<>();

  /**
   * Returns the marcEntryInfo.
   *
   * @return the marcEntryInfo
   */
  public List<MarcEntryInfo> getMarcEntryInfo() {
    return marcEntryInfo;
  }

  /**
   * Sets the marcEntryInfo.
   *
   * @param marcEntryInfo the marcEntryInfo to set
   */
  public void setMarcEntryInfo(List<MarcEntryInfo> marcEntryInfo) {
    this.marcEntryInfo = marcEntryInfo;
  }
}
