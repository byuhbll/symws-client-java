package edu.byu.hbll.symws.client.model;

/**
 * Enumeration of possible patron statuses.
 *
 * <p>From the SYMWS documentation: "The StatusType type displays the user’s status."
 */
public enum StatusType {
  BARRED,
  BLOCKED,
  DELINQUENT,
  OK;
}
