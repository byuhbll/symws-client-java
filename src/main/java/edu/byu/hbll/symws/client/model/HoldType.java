package edu.byu.hbll.symws.client.model;

/** Enumeration of possible hold types. */
public enum HoldType {
  COPY,
  TITLE,
  CALL,
  SYSTEM;
}
