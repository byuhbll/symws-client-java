package edu.byu.hbll.symws.client.adaper;

import java.util.Currency;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/** JAXB adapter to marshal and unmarshal ISO-4217 currency codes. */
public class CurrencyAdapter extends XmlAdapter<String, Currency> {

  @Override
  public Currency unmarshal(String v) {
    return Currency.getInstance(v);
  }

  @Override
  public String marshal(Currency v) {
    return v.toString();
  }
}
