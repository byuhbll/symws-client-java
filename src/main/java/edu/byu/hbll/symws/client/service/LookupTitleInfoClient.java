package edu.byu.hbll.symws.client.service;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.SymwsClientException;
import edu.byu.hbll.symws.client.SymwsUtil;
import edu.byu.hbll.symws.client.model.BoundwithLinkInfo;
import edu.byu.hbll.symws.client.model.CallInfo;
import edu.byu.hbll.symws.client.model.CallSummary;
import edu.byu.hbll.symws.client.model.ItemInfo;
import edu.byu.hbll.symws.client.model.LookupTitleInfoResponse;
import edu.byu.hbll.symws.client.model.MarcEntryFilter;
import edu.byu.hbll.symws.client.model.MarcHoldingsInfo;
import edu.byu.hbll.symws.client.model.ShowShadowedOption;
import edu.byu.hbll.symws.client.model.TitleAvailabilityInfo;
import edu.byu.hbll.symws.client.model.TitleInfo;
import edu.byu.hbll.symws.client.model.TitleOrderInfo;
import java.util.Objects;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * Client for looking up a catalog record in SYMWS.
 *
 * <p>This class is thread-safe, conditional upon the safety of the underlying service itself.
 */
public class LookupTitleInfoClient {

  /**
   * The path of this service, relative to the root URI provided in {@link
   * SymwsClientConfig#getRootUri()}.
   */
  public static final String SERVICE_PATH = "/rest/standard/lookupTitleInfo";

  /** The name/key of the "titleID" query parameter. */
  public static final String QUERYPARAM_TITLE_ID = "titleID";

  /** The name/key of the "itemID" query parameter. */
  public static final String QUERYPARAM_ITEM_ID = "itemID";

  private SymwsClientConfig symwsClientConfig;
  private MultivaluedMap<String, Object> optionalParams;

  /**
   * Constructs a new instance.
   *
   * @param symwsClientConfig the client configuration to use for all requests made using this
   *     client
   * @param optionalParams the optional parameters to send with all requests made using this client
   */
  public LookupTitleInfoClient(SymwsClientConfig symwsClientConfig, OptionalParams optionalParams) {
    this.symwsClientConfig = Objects.requireNonNull(symwsClientConfig);
    this.optionalParams = (optionalParams == null ? new OptionalParams() : optionalParams).asMap();
  }

  /**
   * Lookup the catalog record identified by the provided title ID (catalog key).
   *
   * @param titleId the title ID or catalog key of the record being looked up
   * @return the catalog record
   * @throws SymwsClientException if the request is not successful
   */
  public TitleInfo lookupWithTitleId(Long titleId) throws SymwsClientException {
    SymwsUtil.requireGreaterThanOrEqualTo(titleId, 1L, "titleId must be a positive integer");
    return lookupTitleInfo(QUERYPARAM_TITLE_ID, Long.toString(titleId));
  }

  /**
   * Lookup the catalog record identified by the provided title ID (catalog key).
   *
   * <p>This is a convenience method to allow titleIds to be stored and submitted as Strings.
   *
   * @param titleId the title ID or catalog key of the record being looked up
   * @return the catalog record
   * @throws SymwsClientException if the request is not successful
   */
  public TitleInfo lookupWithTitleId(String titleId) throws SymwsClientException {
    SymwsUtil.requireGreaterThanOrEqualTo(
        Long.parseLong(titleId), 1L, "titleId must be a positive integer");
    return lookupTitleInfo(QUERYPARAM_TITLE_ID, titleId);
  }

  /**
   * Lookup the catalog record containing the given item identified by the provided item ID or
   * barcode.
   *
   * @param itemId the item ID or barcode of the item belonging to the record being looked up
   * @return the catalog record
   * @throws SymwsClientException if the request is not successful
   */
  public TitleInfo lookupWithItemId(String itemId) throws SymwsClientException {
    SymwsUtil.requireNonBlank(itemId, "itemId cannot be null or blank");
    return lookupTitleInfo(QUERYPARAM_ITEM_ID, itemId);
  }

  /**
   * Backend method used in common by other methods to actually call the lookupTitleInfo service.
   *
   * @param idKey the type of identifier being used to lookup the catalog record; should be {@link
   *     #QUERYPARAM_TITLE_ID} or {@link #QUERYPARAM_ITEM_ID}
   * @param idValue the identifier value corresponding with the provided identifierKey
   * @return the catalog record
   * @throws SymwsClientException if the request is not successful
   */
  private TitleInfo lookupTitleInfo(String idKey, String idValue) throws SymwsClientException {
    // Generics on right side must be explicitly declared to prevent constructor ambiguity.
    MultivaluedMap<String, Object> queryParams =
        new MultivaluedHashMap<String, Object>(optionalParams);
    queryParams.add(idKey, idValue);

    Response response = SymwsUtil.request(symwsClientConfig, SERVICE_PATH, queryParams);
    return SymwsUtil.parseResponse(response, LookupTitleInfoResponse.class).getTitleInfo();
  }

  /** Optional query parameters which can be supplied with a lookupTitleInfo request. */
  public static class OptionalParams {

    /** The name/key of the "libraryFilter" query parameter. */
    public static final String QUERYPARAM_LIBRARY_FILTER = "libraryFilter";

    /** The name/key of the "lncludeAvailabilityInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_AVAILABILITY_INFO = "includeAvailabilityInfo";

    /** The name/key of the "includeItemInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_ITEM_INFO = "includeItemInfo";

    /** The name/key of the "includeCatalogingInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_CATALOGING_INFO = "includeCatalogingInfo";

    /** The name/key of the "includeOrderInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_ORDER_INFO = "includeOrderInfo";

    /** The name/key of the "includeOPACInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_OPAC_INFO = "includeOPACInfo";

    /** The name/key of the "includeBoundTogether" query parameter. */
    public static final String QUERYPARAM_INCLUDE_BOUND_TOGETHER = "includeBoundTogether";

    /** The name/key of the "includeMarcHoldings" query parameter. */
    public static final String QUERYPARAM_INCLUDE_MARC_HOLDINGS = "includeMarcHoldings";

    /** The name/key of the "marcEntryFilter" query parameter. */
    public static final String QUERYPARAM_MARC_ENTRY_FILTER = "marcEntryFilter";

    /** The name/key of the "marcEntryId" query parameter. */
    public static final String QUERYPARAM_MARC_ENTRY_ID = "marcEntryID";

    /** The name/key of the "includeRelatedSearchInfo" query parameter. */
    public static final String QUERYPARAM_INCLUDE_RELATED_SEARCH_INFO = "includeRelatedSearchInfo";

    /** The name/key of the "includeCallNumberSummary" query parameter. */
    public static final String QUERYPARAM_INCLUDE_CALL_NUMBER_SUMMARY = "includeCallNumberSummary";

    /** The name/key of the "includeItemCategory" query parameter. */
    public static final String QUERYPARAM_INCLUDE_ITEM_CATEGORY = "includeItemCategory";

    /** The name/key of the "includeShadowed" query parameter. */
    public static final String QUERYPARAM_INCLUDE_SHADOWED = "includeShadowed";

    private String libraryFilter = null;
    private boolean includeAvailabilityInfo = false;
    private boolean includeItemInfo = false;
    private boolean includeCatalogingInfo = false;
    private boolean includeOrderInfo = false;
    private boolean includeOPACInfo = false;
    private boolean includeBoundTogether = false;
    private boolean includeMarcHoldings = false;
    private MarcEntryFilter marcEntryFilter = null;
    private String marcEntryId = null;
    private boolean includeRelatedSearchInfo = false;
    private boolean includeCallNumberSummary = false;
    private boolean includeItemCategory = false;
    private ShowShadowedOption shadowOption = null;

    /**
     * Specifies that the response should include only the items owned by the given library ID.
     *
     * @param filter the library ID or search library group ID to filter by
     * @return this object
     */
    public OptionalParams libraryFilter(String filter) {
      this.libraryFilter = filter;
      return this;
    }

    /**
     * Specifies that the response should include a {@link TitleAvailabilityInfo} object.
     *
     * @return this object
     */
    public OptionalParams includeAvailabilityInfo() {
      this.includeAvailabilityInfo = true;
      return this;
    }

    /**
     * Specifies that the response should include {@link CallInfo} and {@link ItemInfo} objects.
     *
     * @return this object
     */
    public OptionalParams includeItemInfo() {
      this.includeItemInfo = true;
      return this;
    }

    /**
     * Specifies that the response should include cataloging data.
     *
     * @return this object
     */
    public OptionalParams includeCatalogingInfo() {
      this.includeCatalogingInfo = true;
      return this;
    }

    /**
     * Specifies that the response should include {@link TitleOrderInfo} objects if applicable.
     *
     * @return this object
     */
    public OptionalParams includeOrderInfo() {
      this.includeCatalogingInfo = true;
      return this;
    }

    /**
     * Specifies that the response should include OPAC data such as title and author.
     *
     * @return this object
     */
    public OptionalParams includeOPACInfo() {
      this.includeOPACInfo = true;
      return this;
    }

    /**
     * Specifies that the response should include {@link BoundwithLinkInfo} objects if applicable.
     *
     * @return this object
     */
    public OptionalParams includeBoundTogether() {
      this.includeBoundTogether = true;
      return this;
    }

    /**
     * Specifies that the response should include {@link MarcHoldingsInfo} objects if applicable.
     *
     * @return this object
     */
    public OptionalParams includeMarcHoldings() {
      this.includeMarcHoldings = true;
      return this;
    }

    /**
     * Specifies whether MARC fields should be included, and if so, what level of detail should be
     * displayed.
     *
     * @param filter the detail level of MARC data to display ({@code NONE} is assumed by default)
     * @return this object
     */
    public OptionalParams marcEntryFilter(MarcEntryFilter filter) {
      this.marcEntryFilter = filter;
      return this;
    }

    /**
     * Specifies that a specific MARC field should be included.
     *
     * @param entryId the tag identifying the MARC field to display
     * @return this object
     */
    public OptionalParams marcEntryId(String entryId) {
      this.marcEntryId = entryId;
      return this;
    }

    /**
     * Specifies that the response should include related search information.
     *
     * @return this object
     */
    public OptionalParams includeRelatedSearchInfo() {
      this.includeRelatedSearchInfo = true;
      return this;
    }

    /**
     * Specifies that the response should include a {@link CallSummary} object.
     *
     * @return this object
     */
    public OptionalParams includeCallNumberSummary() {
      this.includeCallNumberSummary = true;
      return this;
    }

    /**
     * Specifies that the response should include item category data.
     *
     * @return this object
     */
    public OptionalParams includeItemCategory() {
      this.includeItemCategory = true;
      return this;
    }

    /**
     * Specifies whether the response should include only shadowed items, unshadowed items, or both.
     *
     * @param option which display option is preferred ({@code NONE} is assumed by default)
     * @return this object
     */
    public OptionalParams includeShadowed(ShowShadowedOption option) {
      this.shadowOption = option;
      return this;
    }

    /**
     * Adds the non-null members of this object to the provided webTarget as query parameters.
     *
     * @param webTarget the WebTarget to which the parameters should be applied
     * @return a new WebTarget instance with the needed parameters
     */
    public WebTarget applyOptionalParams(WebTarget webTarget) {
      webTarget =
          webTarget
              .queryParam(QUERYPARAM_INCLUDE_AVAILABILITY_INFO, includeAvailabilityInfo)
              .queryParam(QUERYPARAM_INCLUDE_BOUND_TOGETHER, includeBoundTogether)
              .queryParam(QUERYPARAM_INCLUDE_CALL_NUMBER_SUMMARY, includeCallNumberSummary)
              .queryParam(QUERYPARAM_INCLUDE_CATALOGING_INFO, includeCatalogingInfo)
              .queryParam(QUERYPARAM_INCLUDE_ITEM_CATEGORY, includeItemCategory)
              .queryParam(QUERYPARAM_INCLUDE_ITEM_INFO, includeItemInfo)
              .queryParam(QUERYPARAM_INCLUDE_MARC_HOLDINGS, includeMarcHoldings)
              .queryParam(QUERYPARAM_INCLUDE_OPAC_INFO, includeOPACInfo)
              .queryParam(QUERYPARAM_INCLUDE_ORDER_INFO, includeOrderInfo)
              .queryParam(QUERYPARAM_INCLUDE_RELATED_SEARCH_INFO, includeRelatedSearchInfo);

      if (shadowOption != null) {
        webTarget = webTarget.queryParam(QUERYPARAM_INCLUDE_SHADOWED, shadowOption);
      }

      if (libraryFilter != null) {
        webTarget = webTarget.queryParam(QUERYPARAM_LIBRARY_FILTER, libraryFilter);
      }

      if (marcEntryFilter != null) {
        webTarget = webTarget.queryParam(QUERYPARAM_MARC_ENTRY_FILTER, marcEntryFilter);
      }

      if (marcEntryId != null) {
        webTarget = webTarget.queryParam(QUERYPARAM_MARC_ENTRY_ID, marcEntryId);
      }

      return webTarget;
    }

    /**
     * Returns the non-null members of this object as an multivalued map.
     *
     * @return the map containing query parameters
     */
    public MultivaluedMap<String, Object> asMap() {
      MultivaluedMap<String, Object> map = new MultivaluedHashMap<>();

      map.add(QUERYPARAM_INCLUDE_AVAILABILITY_INFO, includeAvailabilityInfo);
      map.add(QUERYPARAM_INCLUDE_BOUND_TOGETHER, includeBoundTogether);
      map.add(QUERYPARAM_INCLUDE_CALL_NUMBER_SUMMARY, includeCallNumberSummary);
      map.add(QUERYPARAM_INCLUDE_CATALOGING_INFO, includeCatalogingInfo);
      map.add(QUERYPARAM_INCLUDE_ITEM_CATEGORY, includeItemCategory);
      map.add(QUERYPARAM_INCLUDE_ITEM_INFO, includeItemInfo);
      map.add(QUERYPARAM_INCLUDE_MARC_HOLDINGS, includeMarcHoldings);
      map.add(QUERYPARAM_INCLUDE_OPAC_INFO, includeOPACInfo);
      map.add(QUERYPARAM_INCLUDE_ORDER_INFO, includeOrderInfo);
      map.add(QUERYPARAM_INCLUDE_RELATED_SEARCH_INFO, includeRelatedSearchInfo);

      if (shadowOption != null) {
        map.add(QUERYPARAM_INCLUDE_SHADOWED, shadowOption);
      }

      if (libraryFilter != null) {
        map.add(QUERYPARAM_LIBRARY_FILTER, libraryFilter);
      }

      if (marcEntryFilter != null) {
        map.add(QUERYPARAM_MARC_ENTRY_FILTER, marcEntryFilter);
      }

      if (marcEntryId != null) {
        map.add(QUERYPARAM_MARC_ENTRY_ID, marcEntryId);
      }

      return map;
    }
  }
}
