package edu.byu.hbll.symws.client.model;

import java.math.BigDecimal;
import java.util.Currency;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * JAXB bean representing an amount of money.
 *
 * <p>From the SYMWS documentation: "The Money type is a decimal value with a required attribute
 * specifying the ISO-4217 value (for example, "USD")."
 */
public class Money {

  @XmlAttribute private Currency currency;
  @XmlValue private BigDecimal amount;

  /**
   * Returns the currency.
   *
   * @return the currency
   */
  public Currency getCurrency() {
    return currency;
  }

  /**
   * Returns the amount.
   *
   * @return the amount
   */
  public BigDecimal getAmount() {
    return amount;
  }
}
