package edu.byu.hbll.symws.client.service;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.SymwsClientException;
import edu.byu.hbll.symws.client.SymwsUtil;
import edu.byu.hbll.symws.client.model.HoldRange;
import edu.byu.hbll.symws.client.model.HoldType;
import edu.byu.hbll.symws.client.model.PlaceMyHoldResponse;
import java.time.LocalDate;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * Client for placing a hold belonging to the current user session in SYMWS.
 *
 * <p>This class is thread-safe, conditional upon the safety of the underlying service itself.
 */
public class PlaceMyHoldClient {

  /**
   * The path of this service, relative to the root URI provided in {@link
   * SymwsClientConfig#getRootUri()}.
   */
  public static final String SERVICE_PATH = "/rest/patron/placeMyHold";

  /** The name/key of the "titleKey" query parameter. */
  public static final String QUERYPARAM_TITLE_KEY = "titleKey";

  /** The name/key of the "itemID" query parameter. */
  public static final String QUERYPARAM_ITEM_ID = "itemID";

  private final SymwsClientConfig symwsClientConfig;
  private final MultivaluedMap<String, Object> optionalParams;

  /**
   * Constructs a new instance.
   *
   * @param symwsClientConfig the client configuration to use for all requests made using this
   *     client
   * @param optionalParams the optional parameters to send with all requests made using this client
   */
  public PlaceMyHoldClient(SymwsClientConfig symwsClientConfig, OptionalParams optionalParams) {
    this.symwsClientConfig = symwsClientConfig;
    this.optionalParams = (optionalParams == null ? new OptionalParams() : optionalParams).asMap();
  }

  /**
   * Creates a new title-level Symphony hold.
   *
   * @param titleKey the title or catalog key of the material being requested
   * @return the details of the newly created hold
   * @throws SymwsClientException if the web service returns an error
   */
  public PlaceMyHoldResponse placeTitleHold(long titleKey) throws SymwsClientException {
    SymwsUtil.requireGreaterThanOrEqualTo(titleKey, 0L, "titleKey cannot be negative");
    return placeHold(QUERYPARAM_TITLE_KEY, Long.toString(titleKey));
  }

  /**
   * Creates a new item/copy-level Symphony hold.
   *
   * @param itemId the item ID or barcode of the material being requested
   * @return the details of the newly created hold
   * @throws SymwsClientException if the web service returns an error
   */
  public PlaceMyHoldResponse placeItemHold(String itemId) throws SymwsClientException {
    SymwsUtil.requireNonBlank(itemId, "itemId cannot be null or blank");
    return placeHold(QUERYPARAM_ITEM_ID, itemId);
  }

  /**
   * Backend method used in common by other methods to actually call the placeMyHold service.
   *
   * @param idType the type of hold being placed; should be {@link #QUERYPARAM_TITLE_KEY} or {@link
   *     #QUERYPARAM_ITEM_ID}
   * @param idValue the titleKey or itemID corresponding with the appropriate identifierKey
   * @return the details of the newly created hold
   * @throws SymwsClientException if the web service returns an error
   */
  private PlaceMyHoldResponse placeHold(String idType, String idValue) throws SymwsClientException {
    // Generics on right side must be explicitly declared to prevent constructor ambiguity.
    MultivaluedMap<String, Object> queryParams =
        new MultivaluedHashMap<String, Object>(optionalParams);
    queryParams.add(idType, idValue);

    Response response = SymwsUtil.request(symwsClientConfig, SERVICE_PATH, queryParams);
    return SymwsUtil.parseResponse(response, PlaceMyHoldResponse.class);
  }

  /** Optional query parameters which can be supplied with a placeMyHold or createMyHold request. */
  public static class OptionalParams {

    /** The name/key of the "callNumber" query parameter. */
    public static final String QUERYPARAM_CALL_NUMBER = "callNumber";

    /** The name/key of the "holdType" query parameter. */
    public static final String QUERYPARAM_HOLD_TYPE = "holdType";

    /** The name/key of the "holdRange" query parameter. */
    public static final String QUERYPARAM_HOLD_RANGE = "holdRange";

    /** The name/key of the "pickupLibraryID" query parameter. */
    public static final String QUERYPARAM_PICKUP_LIBRARY_ID = "pickupLibraryID";

    /** The name/key of the "mailServiceID" query parameter. */
    public static final String QUERYPARAM_MAIL_SERVICE_ID = "mailServiceID";

    /** The name/key of the "expiresDate" query parameter. */
    public static final String QUERYPARAM_EXPIRES_DATE = "expiresDate";

    /** The name/key of the "suspendStartDate" query parameter. */
    public static final String QUERYPARAM_SUSPEND_START_DATE = "suspendStartDate";

    /** The name/key of the "suspendEndDate" query parameter. */
    public static final String QUERYPARAM_SUSPEND_END_DATE = "suspendEndDate";

    /** The name/key of the "comment" query parameter. */
    public static final String QUERYPARAM_COMMENT = "comment";

    private String callNumber;
    private HoldType holdType;
    private HoldRange holdRange;
    private String pickupLibraryId;
    private String mailServiceId;
    private LocalDate expiresDate;
    private LocalDate suspendStartDate;
    private LocalDate suspendEndDate;
    private String comment;

    /**
     * From the SYMWS documentation: "Specifies the call number for the item being placed on hold".
     *
     * @param callNumber the callnumber of the item being placed on hold
     * @return this object
     */
    public OptionalParams callNumber(String callNumber) {
      this.callNumber = callNumber;
      return this;
    }

    /**
     * From the SYMWS documentation: "Specifies the enumerated hold type value".
     *
     * @param holdType the type of hold being requested
     * @return this object
     */
    public OptionalParams holdType(HoldType holdType) {
      this.holdType = holdType;
      return this;
    }

    /**
     * From the SYMWS documentation: "Specifies the enumerated hold range value".
     *
     * @param holdRange the range/scope of the hold being requested
     * @return this object
     */
    public OptionalParams holdRange(HoldRange holdRange) {
      this.holdRange = holdRange;
      return this;
    }

    /**
     * From the SYMWS documentation: "Specifies the pickup library Library ID if the hold will be
     * picked up by the patron".
     *
     * @param pickupLibraryId the Symphony library ID where the hold will be picked up
     * @return this object
     */
    public OptionalParams pickupLibraryId(String pickupLibraryId) {
      this.pickupLibraryId = pickupLibraryId;
      return this;
    }

    /**
     * From the SYMWS documentation: "Specifies the Mail Service Tracking ID if the hold will be
     * delivered by mail".
     *
     * @param mailServiceId the tracking ID for this hold
     * @return this object
     */
    public OptionalParams mailServiceId(String mailServiceId) {
      this.mailServiceId = mailServiceId;
      return this;
    }

    /**
     * From the SYMWS documentation: "Specifies the hold expiration date".
     *
     * @param expiresDate the date on which this hold should expire if unfulfilled
     * @return this object
     */
    public OptionalParams expiresDate(LocalDate expiresDate) {
      this.expiresDate = expiresDate;
      return this;
    }

    /**
     * From the SYMWS documentation: "Specifies the start date of the hold suspension".
     *
     * @param suspendStartDate the date on which this hold should be suspended
     * @return this object
     */
    public OptionalParams suspendStartDate(LocalDate suspendStartDate) {
      this.suspendStartDate = suspendStartDate;
      return this;
    }

    /**
     * From the SYMWS documentation: "Specifies the date the hold suspension ends".
     *
     * @param suspendEndDate the date on which this hold should be unsuspended
     * @return this object
     */
    public OptionalParams suspendEndDate(LocalDate suspendEndDate) {
      this.suspendEndDate = suspendEndDate;
      return this;
    }

    /**
     * From the SYMWS documentation: "Specifies optional text to display with the hold information".
     *
     * @param comment the custom message to attach to this hold
     * @return this object
     */
    public OptionalParams comment(String comment) {
      this.comment = comment;
      return this;
    }

    /**
     * Returns the non-null members of this object as an multivalued map.
     *
     * @return the map containing query parameters
     */
    public MultivaluedMap<String, Object> asMap() {
      MultivaluedMap<String, Object> map = new MultivaluedHashMap<>();

      if (callNumber != null) {
        map.add(QUERYPARAM_CALL_NUMBER, callNumber);
      }

      if (holdType != null) {
        map.add(QUERYPARAM_HOLD_TYPE, holdType);
      }

      if (holdRange != null) {
        map.add(QUERYPARAM_HOLD_RANGE, holdRange);
      }

      if (pickupLibraryId != null) {
        map.add(QUERYPARAM_PICKUP_LIBRARY_ID, pickupLibraryId);
      }

      if (mailServiceId != null) {
        map.add(QUERYPARAM_MAIL_SERVICE_ID, mailServiceId);
      }

      if (expiresDate != null) {
        map.add(QUERYPARAM_EXPIRES_DATE, holdRange);
      }

      if (suspendStartDate != null) {
        map.add(QUERYPARAM_SUSPEND_START_DATE, suspendStartDate);
      }

      if (suspendEndDate != null) {
        map.add(QUERYPARAM_SUSPEND_END_DATE, suspendEndDate);
      }

      if (comment != null) {
        map.add(QUERYPARAM_COMMENT, comment);
      }

      return map;
    }
  }
}
