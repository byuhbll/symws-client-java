package edu.byu.hbll.symws.client.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/** JAXB bean representing an item checkout/charge. */
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemCheckoutInfo {

  @XmlElement(nillable = true)
  private String userID;

  @XmlElement(nillable = true)
  private String userName;

  @XmlElement(nillable = true)
  private LocalDateTime checkoutDate;

  @XmlElement(nillable = true)
  private LocalDateTime dueDate;

  @XmlElement(nillable = true, name = "CirculationRuleID")
  private String circulationRuleId;

  @XmlElement(nillable = true, name = "CirculationRuleDescription")
  private String circulationRuleDescription;

  @XmlElement(nillable = true, name = "LocationID")
  private String locationId;

  @XmlElement(nillable = true, name = "LocationDescription")
  private String locationDescription;

  @XmlElement(nillable = true)
  private LocalDate renewedDate;

  @XmlElement(nillable = true)
  private Integer numberOfRenewals;

  @XmlElement(nillable = true)
  private LocalDate lastNoticeSent;

  @XmlElement(nillable = true)
  private Integer overdueNoticeCount;

  @XmlElement(nillable = true)
  private LocalDate recalledDate;

  @XmlElement(nillable = true)
  private Integer recallNoticeCount;

  @XmlElement(nillable = true)
  private LocalDate claimsReturnedDate;

  @XmlElement(nillable = true)
  private Money amountOwed;

  /**
   * Returns the userID.
   *
   * @return the userID
   */
  public String getUserID() {
    return userID;
  }

  /**
   * Returns the userName.
   *
   * @return the userName
   */
  public String getUserName() {
    return userName;
  }

  /**
   * Returns the checkoutDate.
   *
   * @return the checkoutDate
   */
  public LocalDateTime getCheckoutDate() {
    return checkoutDate;
  }

  /**
   * Returns the dueDate.
   *
   * @return the dueDate
   */
  public LocalDateTime getDueDate() {
    return dueDate;
  }

  /**
   * Returns the circulationRuleId.
   *
   * @return the circulationRuleId
   */
  public String getCirculationRuleId() {
    return circulationRuleId;
  }

  /**
   * Returns the circulationRuleDescription.
   *
   * @return the circulationRuleDescription
   */
  public String getCirculationRuleDescription() {
    return circulationRuleDescription;
  }

  /**
   * Returns the locationId.
   *
   * @return the locationId
   */
  public String getLocationId() {
    return locationId;
  }

  /**
   * Returns the locationDescription.
   *
   * @return the locationDescription
   */
  public String getLocationDescription() {
    return locationDescription;
  }

  /**
   * Returns the renewedDate.
   *
   * @return the renewedDate
   */
  public LocalDate getRenewedDate() {
    return renewedDate;
  }

  /**
   * Returns the numberOfRenewals.
   *
   * @return the numberOfRenewals
   */
  public Integer getNumberOfRenewals() {
    return numberOfRenewals;
  }

  /**
   * Returns the lastNoticeSent.
   *
   * @return the lastNoticeSent
   */
  public LocalDate getLastNoticeSent() {
    return lastNoticeSent;
  }

  /**
   * Returns the overdueNoticeCount.
   *
   * @return the overdueNoticeCount
   */
  public Integer getOverdueNoticeCount() {
    return overdueNoticeCount;
  }

  /**
   * Returns the recalledDate.
   *
   * @return the recalledDate
   */
  public LocalDate getRecalledDate() {
    return recalledDate;
  }

  /**
   * Returns the recallNoticeCount.
   *
   * @return the recallNoticeCount
   */
  public Integer getRecallNoticeCount() {
    return recallNoticeCount;
  }

  /**
   * Returns the claimsReturnedDate.
   *
   * @return the claimsReturnedDate
   */
  public LocalDate getClaimsReturnedDate() {
    return claimsReturnedDate;
  }

  /**
   * Returns the amountOwed.
   *
   * @return the amountOwed
   */
  public Money getAmountOwed() {
    return amountOwed;
  }
}
