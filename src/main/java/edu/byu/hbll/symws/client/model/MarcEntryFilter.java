package edu.byu.hbll.symws.client.model;

/** Enumeration of the possible levels of describing a catalog record. */
public enum MarcEntryFilter {
  ALL,
  BRIEF,
  FULL,
  NONE,
  TEMPLATE;
}
