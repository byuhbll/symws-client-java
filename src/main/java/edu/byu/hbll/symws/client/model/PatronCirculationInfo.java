package edu.byu.hbll.symws.client.model;

/**
 * JAXB bean representing at a glance metrics of a user/patron's account.
 *
 * <p>From the SYMWS documentation: "The PatronCirculationInfo type displays sepcific circulation
 * information for a particular patron."
 */
public class PatronCirculationInfo {

  private Integer numberOfCheckouts;
  private Integer numberOfClaimsReturned;
  private Integer numberofBookings;
  private Integer numberOfRequests;
  private Integer numberOfUnansweredRequests;
  private Integer numberOfHolds;
  private Integer numberOfAvailableHolds;
  private Integer numberOfFees;
  private Integer numberOfFines;
  private Money estimatedFines;
  private Integer estimatedOverdues;
  private Money creditBalance; // Optional
  private CheckoutHistoryRule checkoutHistoryRule; // Optional
  private Integer numberOfCheckoutsAllowed; // Optional
  private Boolean numberOfCheckoutsAllowedUnlimited; // Optional

  /**
   * Returns the numberOfCheckouts.
   *
   * @return the numberOfCheckouts
   */
  public Integer getNumberOfCheckouts() {
    return numberOfCheckouts;
  }

  /**
   * Returns the numberOfClaimsReturned.
   *
   * @return the numberOfClaimsReturned
   */
  public Integer getNumberOfClaimsReturned() {
    return numberOfClaimsReturned;
  }

  /**
   * Returns the numberofBookings.
   *
   * @return the numberofBookings
   */
  public Integer getNumberofBookings() {
    return numberofBookings;
  }

  /**
   * Returns the numberOfRequests.
   *
   * @return the numberOfRequests
   */
  public Integer getNumberOfRequests() {
    return numberOfRequests;
  }

  /**
   * Returns the numberOfUnansweredRequests.
   *
   * @return the numberOfUnansweredRequests
   */
  public Integer getNumberOfUnansweredRequests() {
    return numberOfUnansweredRequests;
  }

  /**
   * Returns the numberOfHolds.
   *
   * @return the numberOfHolds
   */
  public Integer getNumberOfHolds() {
    return numberOfHolds;
  }

  /**
   * Returns the numberOfAvailableHolds.
   *
   * @return the numberOfAvailableHolds
   */
  public Integer getNumberOfAvailableHolds() {
    return numberOfAvailableHolds;
  }

  /**
   * Returns the numberOfFees.
   *
   * @return the numberOfFees
   */
  public Integer getNumberOfFees() {
    return numberOfFees;
  }

  /**
   * Returns the numberOfFines.
   *
   * @return the numberOfFines
   */
  public Integer getNumberOfFines() {
    return numberOfFines;
  }

  /**
   * Returns the estimatedFines.
   *
   * @return the estimatedFines
   */
  public Money getEstimatedFines() {
    return estimatedFines;
  }

  /**
   * Returns the estimatedOverdues.
   *
   * @return the estimatedOverdues
   */
  public Integer getEstimatedOverdues() {
    return estimatedOverdues;
  }

  /**
   * Returns the creditBalance.
   *
   * @return the creditBalance
   */
  public Money getCreditBalance() {
    return creditBalance;
  }

  /**
   * Returns the checkoutHistoryRule.
   *
   * @return the checkoutHistoryRule
   */
  public CheckoutHistoryRule getCheckoutHistoryRule() {
    return checkoutHistoryRule;
  }

  /**
   * Returns the numberOfCheckoutsAllowed.
   *
   * @return the numberOfCheckoutsAllowed
   */
  public Integer getNumberOfCheckoutsAllowed() {
    return numberOfCheckoutsAllowed;
  }

  /**
   * Returns the numberOfCheckoutsAllowedUnlimited.
   *
   * @return the numberOfCheckoutsAllowedUnlimited
   */
  public Boolean getNumberOfCheckoutsAllowedUnlimited() {
    return numberOfCheckoutsAllowedUnlimited;
  }
}
