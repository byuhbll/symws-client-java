package edu.byu.hbll.symws.client.model;

/**
 * Enumeration of possible fee types.
 *
 * <p>From the SYMWS documentation: "The FeeInfoFilter type contains enumeration values that you can
 * use in the lookupMyAccountInfo operation request to limit your results when you retrieve fee
 * information."
 */
public enum FeeInfoFilter {
  UNPAID_FEES,
  UNPAID_FEES_AND_PAYMENTS,
  PAID_FEES,
  PAID_FEES_AND_PAYMENTS,
  ALL_FEES,
  ALL_FEES_AND_PAYMENTS;
}
