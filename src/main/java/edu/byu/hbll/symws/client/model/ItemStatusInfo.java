package edu.byu.hbll.symws.client.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/** JAXB bean representing an item checkout/charge. */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "LookupItemStatusResponse")
public class ItemStatusInfo {

  @XmlElement(nillable = true, name = "itemID")
  private String itemId; // Optional

  @XmlElement(nillable = true)
  private String callNumber; // Optional

  @XmlElement(nillable = true)
  private String displayableCallNumber; // Optional

  @XmlElement(nillable = true)
  private Integer copyNumber; // Optional

  @XmlElement(nillable = true)
  private String author; // Optional

  @XmlElement(nillable = true)
  private String title; // Optional

  @XmlElement(nillable = true)
  private List<ItemStatus> itemStatus = new ArrayList<>();

  @XmlElement(nillable = true, name = "libraryID")
  private String libraryId; // Optional

  @XmlElement(nillable = true)
  private String libraryDescription; // Optional

  @XmlElement(nillable = true, name = "currentLocationID")
  private String currentLocationId; // Optional

  @XmlElement(nillable = true)
  private String currentLocationDescription; // Optional

  @XmlElement(nillable = true)
  private Boolean transitNeeded;

  @XmlElement(nillable = true, name = "transitDestinationID")
  private String transitDestinationId; // Optional

  @XmlElement(nillable = true)
  private String transitDestinationDescription; // Optional

  @XmlElement(nillable = true, name = "transitReasonID")
  private String transitReasonId; // Optional

  @XmlElement(nillable = true)
  private Boolean floatingTransitNeeded; // Optional

  @XmlElement(nillable = true, name = "circSetID")
  private String circSetId; // Optional

  @XmlElement(nillable = true)
  private ItemCheckoutInfo itemCheckoutInfo;

  /**
   * Returns the itemId.
   *
   * @return the itemId
   */
  public String getItemId() {
    return itemId;
  }

  /**
   * Returns the callNumber.
   *
   * @return the callNumber
   */
  public String getCallNumber() {
    return callNumber;
  }

  /**
   * Returns the displayableCallNumber.
   *
   * @return the displayableCallNumber
   */
  public String getDisplayableCallNumber() {
    return displayableCallNumber;
  }

  /**
   * Returns the copyNumber.
   *
   * @return the copyNumber
   */
  public Integer getCopyNumber() {
    return copyNumber;
  }

  /**
   * Returns the author.
   *
   * @return the author
   */
  public String getAuthor() {
    return author;
  }

  /**
   * Returns the title.
   *
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * Returns the itemStatus.
   *
   * @return the itemStatus
   */
  public List<ItemStatus> getItemStatus() {
    return itemStatus;
  }

  /**
   * Returns the libraryId.
   *
   * @return the libraryId
   */
  public String getLibraryId() {
    return libraryId;
  }

  /**
   * Returns the libraryDescription.
   *
   * @return the libraryDescription
   */
  public String getLibraryDescription() {
    return libraryDescription;
  }

  /**
   * Returns the currentLocationId.
   *
   * @return the currentLocationId
   */
  public String getCurrentLocationId() {
    return currentLocationId;
  }

  /**
   * Returns the currentLocationDescription.
   *
   * @return the currentLocationDescription
   */
  public String getCurrentLocationDescription() {
    return currentLocationDescription;
  }

  /**
   * Returns the transitNeeded.
   *
   * @return the transitNeeded
   */
  public Boolean getTransitNeeded() {
    return transitNeeded;
  }

  /**
   * Returns the transitDestinationId.
   *
   * @return the transitDestinationId
   */
  public String getTransitDestinationId() {
    return transitDestinationId;
  }

  /**
   * Returns the transitDestinationDescription.
   *
   * @return the transitDestinationDescription
   */
  public String getTransitDestinationDescription() {
    return transitDestinationDescription;
  }

  /**
   * Returns the transitReasonId.
   *
   * @return the transitReasonId
   */
  public String getTransitReasonId() {
    return transitReasonId;
  }

  /**
   * Returns the floatingTransitNeeded.
   *
   * @return the floatingTransitNeeded
   */
  public Boolean getFloatingTransitNeeded() {
    return floatingTransitNeeded;
  }

  /**
   * Returns the circSetId.
   *
   * @return the circSetId
   */
  public String getCircSetId() {
    return circSetId;
  }

  /**
   * Returns the itemCheckoutInfo.
   *
   * @return the itemCheckoutInfo
   */
  public ItemCheckoutInfo getItemCheckoutInfo() {
    return itemCheckoutInfo;
  }
}
