package edu.byu.hbll.symws.client.model;

import javax.xml.bind.annotation.XmlElement;

/** JAXB bean representing boundwith data in a Symphony catalog record. */
public class BoundwithLinkInfo {

  @XmlElement private Boolean linkedAsParent;
  @XmlElement private String callNumber;

  @XmlElement(name = "libraryID")
  private String libraryId;

  @XmlElement(name = "itemID")
  private String itemId; // Optional

  @XmlElement private BriefTitleInfo linkedTitle;

  /**
   * Returns the linkedAsParent.
   *
   * @return the linkedAsParent
   */
  public Boolean getLinkedAsParent() {
    return linkedAsParent;
  }

  /**
   * Returns the callNumber.
   *
   * @return the callNumber
   */
  public String getCallNumber() {
    return callNumber;
  }

  /**
   * Returns the libraryId.
   *
   * @return the libraryId
   */
  public String getLibraryId() {
    return libraryId;
  }

  /**
   * Returns the itemId.
   *
   * @return the itemId
   */
  public String getItemId() {
    return itemId;
  }

  /**
   * Returns the linkedTitle.
   *
   * @return the linkedTitle
   */
  public BriefTitleInfo getLinkedTitle() {
    return linkedTitle;
  }
}
