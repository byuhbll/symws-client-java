package edu.byu.hbll.symws.client.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing address data.
 *
 * <p>From the SYMWS documentation: "The PatronAddressInfo type displays address information for a
 * particular patron."
 */
public class PatronAddressInfo {

  @XmlElement private Integer primaryAddress; // Optional

  @XmlElement(name = "Address1Info")
  private List<AddressInfo> address1Info = new ArrayList<>();

  @XmlElement(name = "Address2Info")
  private List<AddressInfo> address2Info = new ArrayList<>();

  @XmlElement(name = "Address3Info")
  private List<AddressInfo> address3Info = new ArrayList<>();

  /**
   * Returns the primaryAddress.
   *
   * @return the primaryAddress
   */
  public Integer getPrimaryAddress() {
    return primaryAddress;
  }

  /**
   * Returns the address1Info.
   *
   * @return the address1Info
   */
  public List<AddressInfo> getAddress1Info() {
    return address1Info;
  }

  /**
   * Returns the address2Info.
   *
   * @return the address2Info
   */
  public List<AddressInfo> getAddress2Info() {
    return address2Info;
  }

  /**
   * Returns the address3Info.
   *
   * @return the address3Info
   */
  public List<AddressInfo> getAddress3Info() {
    return address3Info;
  }
}
