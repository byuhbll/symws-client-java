package edu.byu.hbll.symws.client.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing a call number summary.
 *
 * <p>From the SYMWS documentation: "The CallSummary type displays call number information for a
 * title."
 */
public class CallSummary {

  @XmlElement(name = "itemID")
  private String itemId;

  @XmlElement private String callNumber; // Optional
  @XmlElement private String analyticZ; // Optional
  @XmlElement private String heldAtLibrariesDisplay; // Optional

  /**
   * Returns the itemId.
   *
   * @return the itemId
   */
  public String getItemId() {
    return itemId;
  }

  /**
   * Returns the callNumber.
   *
   * @return the callNumber
   */
  public String getCallNumber() {
    return callNumber;
  }

  /**
   * Returns the analyticZ.
   *
   * @return the analyticZ
   */
  public String getAnalyticZ() {
    return analyticZ;
  }

  /**
   * Returns the heldAtLibrariesDisplay.
   *
   * @return the heldAtLibrariesDisplay
   */
  public String getHeldAtLibrariesDisplay() {
    return heldAtLibrariesDisplay;
  }
}
