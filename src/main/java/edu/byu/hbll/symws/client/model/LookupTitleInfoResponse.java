package edu.byu.hbll.symws.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/** JAXB bean representing the wrapper around a lookupTitleInfo response. */
@XmlRootElement(name = "LookupTitleInfoResponse")
public class LookupTitleInfoResponse {

  @XmlElement(name = "TitleInfo")
  private TitleInfo titleInfo;

  /**
   * Returns the titleInfo.
   *
   * @return the titleInfo
   */
  public TitleInfo getTitleInfo() {
    return titleInfo;
  }
}
