package edu.byu.hbll.symws.client.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing boundwith data in a Symphony catalog record.
 *
 * <p>From the SWYMWS documentation: "The BriefTitleInfo type displays basic title information for
 * an item."
 */
public class BriefTitleInfo {

  @XmlElement(name = "titleID")
  private Long titleId; // Optional

  @XmlElement private String materialType; // Optional
  @XmlElement private String title;
  @XmlElement private String author; // Optional
  @XmlElement private String line3; // Optional
  @XmlElement private String line4; // Optional
  @XmlElement private Integer yearOfPublication; // Optional
  @XmlElement private String edition; // Optional
  @XmlElement private Integer copiesOnOrder; // Optional

  @XmlElement(name = "ISBN")
  private List<String> isbn = new ArrayList<>();

  @XmlElement(name = "SICI")
  private List<String> sici = new ArrayList<>();

  @XmlElement(name = "UPC")
  private List<String> upc = new ArrayList<>();

  @XmlElement(name = "OCLCControlNumber")
  private String oclcControlNumber; // Optional

  @XmlElement private String url; // Optional
  @XmlElement private String message; // Optional

  @XmlElement(name = "TitleAvailabilityInfo")
  private TitleAvailabilityInfo titleAvailabilityInfo; // Optional

  /**
   * Returns the titleId.
   *
   * @return the titleId
   */
  public Long getTitleId() {
    return titleId;
  }

  /**
   * Returns the materialType.
   *
   * @return the materialType
   */
  public String getMaterialType() {
    return materialType;
  }

  /**
   * Returns the title.
   *
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * Returns the author.
   *
   * @return the author
   */
  public String getAuthor() {
    return author;
  }

  /**
   * Returns the line3.
   *
   * @return the line3
   */
  public String getLine3() {
    return line3;
  }

  /**
   * Returns the line4.
   *
   * @return the line4
   */
  public String getLine4() {
    return line4;
  }

  /**
   * Returns the yearOfPublication.
   *
   * @return the yearOfPublication
   */
  public Integer getYearOfPublication() {
    return yearOfPublication;
  }

  /**
   * Returns the edition.
   *
   * @return the edition
   */
  public String getEdition() {
    return edition;
  }

  /**
   * Returns the copiesOnOrder.
   *
   * @return the copiesOnOrder
   */
  public Integer getCopiesOnOrder() {
    return copiesOnOrder;
  }

  /**
   * Returns the isbn.
   *
   * @return the isbn
   */
  public List<String> getIsbn() {
    return isbn;
  }

  /**
   * Returns the sici.
   *
   * @return the sici
   */
  public List<String> getSici() {
    return sici;
  }

  /**
   * Returns the upc.
   *
   * @return the upc
   */
  public List<String> getUpc() {
    return upc;
  }

  /**
   * Returns the oclcControlNumber.
   *
   * @return the oclcControlNumber
   */
  public String getOclcControlNumber() {
    return oclcControlNumber;
  }

  /**
   * Returns the url.
   *
   * @return the url
   */
  public String getUrl() {
    return url;
  }

  /**
   * Returns the message.
   *
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * Returns the titleAvailabilityInfo.
   *
   * @return the titleAvailabilityInfo
   */
  public TitleAvailabilityInfo getTitleAvailabilityInfo() {
    return titleAvailabilityInfo;
  }
}
