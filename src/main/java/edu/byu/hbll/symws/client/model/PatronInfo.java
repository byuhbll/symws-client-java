package edu.byu.hbll.symws.client.model;

import java.time.LocalDate;
import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing personal information in a Symphony user or patron record.
 *
 * <p>From the SYMWS documentation: "The PatronInfo type displays information about a particular
 * patron."
 */
public class PatronInfo {

  @XmlElement private String userKey;

  @XmlElement(name = "userID")
  private String userId;

  @XmlElement(name = "alternativeID")
  private String alternativeId; // Optional

  @XmlElement(name = "webAuthID")
  private String webAuthId; // Optional

  @XmlElement(name = "groupID")
  private String groupId; // Optional

  @XmlElement private String groupDescription; // Optional
  @XmlElement private String displayName; // Optional
  @XmlElement private LocalDate birthDate; // Optional

  @XmlElement(name = "patronLibraryID")
  private String patronLibraryId; // Optional

  @XmlElement private String patronLibraryDescription; // Optional
  @XmlElement private String department; // Optional
  @XmlElement private String preferredLanguage; // Optional

  /**
   * Returns the userKey.
   *
   * @return the userKey
   */
  public String getUserKey() {
    return userKey;
  }

  /**
   * Returns the userId.
   *
   * @return the userId
   */
  public String getUserId() {
    return userId;
  }

  /**
   * Returns the alternativeId.
   *
   * @return the alternativeId
   */
  public String getAlternativeId() {
    return alternativeId;
  }

  /**
   * Returns the webAuthId.
   *
   * @return the webAuthId
   */
  public String getWebAuthId() {
    return webAuthId;
  }

  /**
   * Returns the groupId.
   *
   * @return the groupId
   */
  public String getGroupId() {
    return groupId;
  }

  /**
   * Returns the groupDescription.
   *
   * @return the groupDescription
   */
  public String getGroupDescription() {
    return groupDescription;
  }

  /**
   * Returns the displayName.
   *
   * @return the displayName
   */
  public String getDisplayName() {
    return displayName;
  }

  /**
   * Returns the birthDate.
   *
   * @return the birthDate
   */
  public LocalDate getBirthDate() {
    return birthDate;
  }

  /**
   * Returns the patronLibraryId.
   *
   * @return the patronLibraryId
   */
  public String getPatronLibraryId() {
    return patronLibraryId;
  }

  /**
   * Returns the patronLibraryDescription.
   *
   * @return the patronLibraryDescription
   */
  public String getPatronLibraryDescription() {
    return patronLibraryDescription;
  }

  /**
   * Returns the department.
   *
   * @return the department
   */
  public String getDepartment() {
    return department;
  }

  /**
   * Returns the preferredLanguage.
   *
   * @return the preferredLanguage
   */
  public String getPreferredLanguage() {
    return preferredLanguage;
  }
}
