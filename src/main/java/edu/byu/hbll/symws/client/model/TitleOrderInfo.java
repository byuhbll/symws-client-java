package edu.byu.hbll.symws.client.model;

import java.time.LocalDate;
import javax.xml.bind.annotation.XmlElement;

/** JAXB bean representing order information for a catalog record. */
public class TitleOrderInfo {

  @XmlElement(name = "orderLibraryID")
  private String orderLibraryId;

  @XmlElement private Integer copiesOrdered;
  @XmlElement private String volumesOrdered; // Optional
  @XmlElement private String orderNote; // Optional
  @XmlElement private LocalDate orderDateReceived; // Optional

  /**
   * Returns the orderLibraryId.
   *
   * @return the orderLibraryId
   */
  public String getOrderLibraryId() {
    return orderLibraryId;
  }

  /**
   * Sets the orderLibraryId.
   *
   * @param orderLibraryId the orderLibraryId to set
   */
  public void setOrderLibraryId(String orderLibraryId) {
    this.orderLibraryId = orderLibraryId;
  }

  /**
   * Returns the copiesOrdered.
   *
   * @return the copiesOrdered
   */
  public Integer getCopiesOrdered() {
    return copiesOrdered;
  }

  /**
   * Sets the copiesOrdered.
   *
   * @param copiesOrdered the copiesOrdered to set
   */
  public void setCopiesOrdered(Integer copiesOrdered) {
    this.copiesOrdered = copiesOrdered;
  }

  /**
   * Returns the volumesOrdered.
   *
   * @return the volumesOrdered
   */
  public String getVolumesOrdered() {
    return volumesOrdered;
  }

  /**
   * Sets the volumesOrdered.
   *
   * @param volumesOrdered the volumesOrdered to set
   */
  public void setVolumesOrdered(String volumesOrdered) {
    this.volumesOrdered = volumesOrdered;
  }

  /**
   * Returns the orderNote.
   *
   * @return the orderNote
   */
  public String getOrderNote() {
    return orderNote;
  }

  /**
   * Sets the orderNote.
   *
   * @param orderNote the orderNote to set
   */
  public void setOrderNote(String orderNote) {
    this.orderNote = orderNote;
  }

  /**
   * Returns the orderDateReceived.
   *
   * @return the orderDateReceived
   */
  public LocalDate getOrderDateReceived() {
    return orderDateReceived;
  }

  /**
   * Sets the orderDateReceived.
   *
   * @param orderDateReceived the orderDateReceived to set
   */
  public void setOrderDateReceived(LocalDate orderDateReceived) {
    this.orderDateReceived = orderDateReceived;
  }
}
