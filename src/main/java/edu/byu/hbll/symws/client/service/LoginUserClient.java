package edu.byu.hbll.symws.client.service;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.SymwsClientException;
import edu.byu.hbll.symws.client.SymwsUtil;
import edu.byu.hbll.symws.client.model.LoginUserResponse;
import java.util.Objects;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * Client for creating user sessions in SYMWS.
 *
 * <p>This class is thread-safe, conditional upon the safety of the underlying service itself.
 */
public class LoginUserClient {

  /**
   * The path of this service, relative to the root URI provided in {@link
   * SymwsClientConfig#getRootUri()}.
   */
  public static final String SERVICE_PATH = "/rest/security/loginUser";

  /** The name/key of the "login" query parameter. */
  public static final String QUERYPARAM_LOGIN = "login";

  /** The name/key of the "password" query parameter. */
  public static final String QUERYPARAM_PASSWORD = "password";

  private final SymwsClientConfig symwsClientConfig;

  /**
   * Constructs a new instance.
   *
   * @param symwsClientConfig the client configuration to use for all requests made using this
   *     client
   */
  public LoginUserClient(SymwsClientConfig symwsClientConfig) {
    this.symwsClientConfig = Objects.requireNonNull(symwsClientConfig);
  }

  /**
   * Creates a native SYMWS session for the given user.
   *
   * @param user the userId or alternateId of the user for which to create a native SYMWS session
   * @param password the PIN/password for the user for which to to create a native SYMWS session
   * @return the parsed response
   * @throws SymwsClientException if the request is not successful
   */
  public LoginUserResponse login(String user, String password) throws SymwsClientException {
    MultivaluedMap<String, Object> queryParams = new MultivaluedHashMap<>(2);
    queryParams.add(QUERYPARAM_LOGIN, SymwsUtil.requireNonBlank(user));
    queryParams.add(QUERYPARAM_PASSWORD, password);

    Response response = SymwsUtil.request(symwsClientConfig, SERVICE_PATH, queryParams);
    return SymwsUtil.parseResponse(response, LoginUserResponse.class);
  }
}
