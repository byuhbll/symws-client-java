package edu.byu.hbll.symws.client.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing a fee data associated with an item.
 *
 * <p>From the SYMWS documentation: "The FeeItemInfo type displays any fee information that is
 * associated with an item (for example, an overdue book)."
 */
public class FeeItemInfo {

  @XmlElement private Long titleKey;

  @XmlElement(name = "itemID")
  private String itemId; // Optional

  @XmlElement private String itemTypeDescription; // Optional
  @XmlElement private String callNumber;
  @XmlElement private Integer copyNumber; // Optional

  @XmlElement(name = "itemLibraryID")
  private String itemLibraryId;

  @XmlElement private String itemLibraryDescription; // Optional
  @XmlElement private String title; // Optional
  @XmlElement private String author; // Optional
  @XmlElement private LocalDateTime checkoutDate; // Optional
  @XmlElement private LocalDateTime dueDate; // Optional
  @XmlElement private LocalDate recallDate; // Optional
  @XmlElement private LocalDateTime renewedDate; // Optional

  @XmlElement(name = "circulationRuleID")
  private String circulationRuleId; // Optional

  @XmlElement private String circulationRuleDescription; // Optional

  /**
   * Returns the titleKey.
   *
   * @return the titleKey
   */
  public Long getTitleKey() {
    return titleKey;
  }

  /**
   * Returns the itemId.
   *
   * @return the itemId
   */
  public String getItemId() {
    return itemId;
  }

  /**
   * Returns the itemTypeDescription.
   *
   * @return the itemTypeDescription
   */
  public String getItemTypeDescription() {
    return itemTypeDescription;
  }

  /**
   * Returns the callNumber.
   *
   * @return the callNumber
   */
  public String getCallNumber() {
    return callNumber;
  }

  /**
   * Returns the copyNumber.
   *
   * @return the copyNumber
   */
  public Integer getCopyNumber() {
    return copyNumber;
  }

  /**
   * Returns the itemLibraryId.
   *
   * @return the itemLibraryId
   */
  public String getItemLibraryId() {
    return itemLibraryId;
  }

  /**
   * Returns the itemLibraryDescription.
   *
   * @return the itemLibraryDescription
   */
  public String getItemLibraryDescription() {
    return itemLibraryDescription;
  }

  /**
   * Returns the title.
   *
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * Returns the author.
   *
   * @return the author
   */
  public String getAuthor() {
    return author;
  }

  /**
   * Returns the checkoutDate.
   *
   * @return the checkoutDate
   */
  public LocalDateTime getCheckoutDate() {
    return checkoutDate;
  }

  /**
   * Returns the dueDate.
   *
   * @return the dueDate
   */
  public LocalDateTime getDueDate() {
    return dueDate;
  }

  /**
   * Returns the recallDate.
   *
   * @return the recallDate
   */
  public LocalDate getRecallDate() {
    return recallDate;
  }

  /**
   * Returns the renewedDate.
   *
   * @return the renewedDate
   */
  public LocalDateTime getRenewedDate() {
    return renewedDate;
  }

  /**
   * Returns the circulationRuleId.
   *
   * @return the circulationRuleId
   */
  public String getCirculationRuleId() {
    return circulationRuleId;
  }

  /**
   * Returns the circulationRuleDescription.
   *
   * @return the circulationRuleDescription
   */
  public String getCirculationRuleDescription() {
    return circulationRuleDescription;
  }
}
