package edu.byu.hbll.symws.client.model;

import java.util.Optional;
import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing an address line.
 *
 * <p>From the SYMWS documentation: "The AddressInfo type displays patron-specific address
 * information for each of the addressInfo elements in the {@link PatronAddressInfo} data type".
 */
public class AddressInfo {

  @XmlElement(name = "addressPolicyID")
  private String addressPolicyId;

  @XmlElement private String addressPolicyDescription;
  @XmlElement private String addressValue; // Optional

  /**
   * Returns the addressPolicyId.
   *
   * @return the addressPolicyId
   */
  public String getAddressPolicyId() {
    return addressPolicyId;
  }

  /**
   * Returns the addressPolicyDescription.
   *
   * @return the addressPolicyDescription
   */
  public String getAddressPolicyDescription() {
    return addressPolicyDescription;
  }

  /**
   * Returns the addressValue.
   *
   * @return the addressValue, or empty if none is set
   */
  public Optional<String> getAddressValue() {
    return Optional.ofNullable(addressValue);
  }
}
