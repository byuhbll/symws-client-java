package edu.byu.hbll.symws.client.model;

import java.time.LocalDate;
import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing user suspension data.
 *
 * <p>From the SYMWS documentation: "The UserSuspensionInfo type displays information about a
 * patron's suspension."
 */
public class UserSuspensionInfo {

  @XmlElement private LocalDate dateSuspended;
  @XmlElement private LocalDate dateSuspensionEnds;
  @XmlElement private Long suspensionKey;
  @XmlElement private String linkedSuspensionKey;

  @XmlElement(name = "suspensionLibraryID")
  private String suspensionLibraryId;

  @XmlElement private String suspensionLibraryDescription;

  @XmlElement(name = "suspensionReasonID")
  private String suspensionReasonId;

  @XmlElement private String suspensionReasonDescription;
  @XmlElement private String suspensionType;
  @XmlElement private Integer numberOfUnits;
  @XmlElement private String title; // Optional

  @XmlElement(name = "itemID")
  private String itemId; // Optional

  /**
   * Returns the dateSuspended.
   *
   * @return the dateSuspended
   */
  public LocalDate getDateSuspended() {
    return dateSuspended;
  }

  /**
   * Returns the dateSuspensionEnds.
   *
   * @return the dateSuspensionEnds
   */
  public LocalDate getDateSuspensionEnds() {
    return dateSuspensionEnds;
  }

  /**
   * Returns the suspensionKey.
   *
   * @return the suspensionKey
   */
  public Long getSuspensionKey() {
    return suspensionKey;
  }

  /**
   * Returns the linkedSuspensionKey.
   *
   * @return the linkedSuspensionKey
   */
  public String getLinkedSuspensionKey() {
    return linkedSuspensionKey;
  }

  /**
   * Returns the suspensionLibraryId.
   *
   * @return the suspensionLibraryId
   */
  public String getSuspensionLibraryId() {
    return suspensionLibraryId;
  }

  /**
   * Returns the suspensionLibraryDescription.
   *
   * @return the suspensionLibraryDescription
   */
  public String getSuspensionLibraryDescription() {
    return suspensionLibraryDescription;
  }

  /**
   * Returns the suspensionReasonId.
   *
   * @return the suspensionReasonId
   */
  public String getSuspensionReasonId() {
    return suspensionReasonId;
  }

  /**
   * Returns the suspensionReasonDescription.
   *
   * @return the suspensionReasonDescription
   */
  public String getSuspensionReasonDescription() {
    return suspensionReasonDescription;
  }

  /**
   * Returns the suspensionType.
   *
   * @return the suspensionType
   */
  public String getSuspensionType() {
    return suspensionType;
  }

  /**
   * Returns the numberOfUnits.
   *
   * @return the numberOfUnits
   */
  public Integer getNumberOfUnits() {
    return numberOfUnits;
  }

  /**
   * Returns the title.
   *
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * Returns the itemId.
   *
   * @return the itemId
   */
  public String getItemId() {
    return itemId;
  }
}
