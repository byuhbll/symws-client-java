package edu.byu.hbll.symws.client.service;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.SymwsClientException;
import edu.byu.hbll.symws.client.SymwsUtil;
import edu.byu.hbll.symws.client.model.LookupSessionInfoResponse;
import javax.ws.rs.core.Response;

/**
 * Client for looking up information about the current SYMWS session.
 *
 * <p>This class is thread-safe, conditional upon the safety of the underlying service itself.
 */
public class LookupSessionInfoClient {

  /**
   * The path of this service, relative to the root URI provided in {@link
   * SymwsClientConfig#getRootUri()}.
   */
  public static final String SERVICE_PATH = "/rest/patron/lookupPatronInfo";

  private final SymwsClientConfig symwsClientConfig;

  /**
   * Constructs a new instance.
   *
   * @param symwsClientConfig the client configuration to use for all requests made using this
   *     client; this client configuration MUST have an active session.
   */
  public LookupSessionInfoClient(SymwsClientConfig symwsClientConfig) {
    this.symwsClientConfig = symwsClientConfig;
  }

  /**
   * Lookup the session metadata for the current session. Unlike most other clients, this client
   * will not attempt to retry the request if it fails due to session unavailability or expiration.
   *
   * @return the session metadata
   * @throws SymwsClientException if the request is not successful
   */
  public LookupSessionInfoResponse lookup() throws SymwsClientException {
    Response response = SymwsUtil.requestWithoutRetry(symwsClientConfig, SERVICE_PATH, null);
    return SymwsUtil.parseResponse(response, LookupSessionInfoResponse.class);
  }
}
