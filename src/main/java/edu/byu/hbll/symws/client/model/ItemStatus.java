package edu.byu.hbll.symws.client.model;

/**
 * Enumeration of possible item statuses.
 *
 * <p>From the SYMWS documentation: "The ItemStatus type values provide information about the status
 * of an item."
 */
public enum ItemStatus {
  ACCOUNTABLE,
  AVAILABLE_SOON,
  BOOKED,
  BORROWED,
  CHARGED,
  DISPATCHED,
  HOLD_CANCELLED,
  LOST_CHARGED,
  HOST_SUSPENDED,
  INTRANSIT,
  ONHOLD,
  ONRESERVE,
  ONSHELF;
}
