package edu.byu.hbll.symws.client.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing availability data for a catalog record.
 *
 * <p>From the SYMWS documentation: "TitleAvailabilityInfo contains information about a particular
 * item’s availability."
 */
public class TitleAvailabilityInfo {

  @XmlElement private Integer totalCopiesAvailable;
  @XmlElement private List<String> librariesWithAvailableCopies = new ArrayList<>(); // Optional

  @XmlElement(name = "totalResvCppiesAvailable")
  private Integer totalReserveCopiesAvailable;

  @XmlElement(name = "libraryWithAvailableResvCopies")
  private List<String> librariesWithAvailableReserveCopies = new ArrayList<>();

  @XmlElement private String locationOfFirstAvailableItem; // Optional
  @XmlElement private Boolean holdable;
  @XmlElement private Boolean bookable;

  /**
   * Returns the totalCopiesAvailable.
   *
   * @return the totalCopiesAvailable
   */
  public Integer getTotalCopiesAvailable() {
    return totalCopiesAvailable;
  }

  /**
   * Returns the librariesWithAvailableCopies.
   *
   * @return the librariesWithAvailableCopies
   */
  public List<String> getLibrariesWithAvailableCopies() {
    return librariesWithAvailableCopies;
  }

  /**
   * Returns the totalReserveCopiesAvailable.
   *
   * @return the totalReserveCopiesAvailable
   */
  public Integer getTotalReserveCopiesAvailable() {
    return totalReserveCopiesAvailable;
  }

  /**
   * Returns the librariesWithAvailableReserveCopies.
   *
   * @return the librariesWithAvailableReserveCopies
   */
  public List<String> getLibrariesWithAvailableReserveCopies() {
    return librariesWithAvailableReserveCopies;
  }

  /**
   * Returns the locationOfFirstAvailableItem.
   *
   * @return the locationOfFirstAvailableItem
   */
  public String getLocationOfFirstAvailableItem() {
    return locationOfFirstAvailableItem;
  }

  /**
   * Returns the holdable.
   *
   * @return the holdable
   */
  public Boolean getHoldable() {
    return holdable;
  }

  /**
   * Returns the bookable.
   *
   * @return the bookable
   */
  public Boolean getBookable() {
    return bookable;
  }
}
