package edu.byu.hbll.symws.client.model;

import java.time.LocalDate;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/** JAXB bean representing the response to a request to place a new hold. */
@XmlRootElement(name = "PlaceMyHoldResponse")
public class PlaceMyHoldResponse {

  @XmlElement private Long holdKey;
  @XmlElement private String title;
  @XmlElement private String author;
  @XmlElement private Boolean itemAvailable;
  @XmlElement private String callNumber;

  @XmlElement(name = "itemID")
  private String itemId;

  @XmlElement private LocalDate dateHoldExpires;
  @XmlElement private LocalDate dateHoldExpiresOnShelf;

  /**
   * Returns the holdKey.
   *
   * @return the holdKey
   */
  public Long getHoldKey() {
    return holdKey;
  }

  /**
   * Returns the title.
   *
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * Returns the author.
   *
   * @return the author
   */
  public String getAuthor() {
    return author;
  }

  /**
   * Returns the itemAvailable.
   *
   * @return the itemAvailable
   */
  public Boolean getItemAvailable() {
    return itemAvailable;
  }

  /**
   * Returns the callNumber.
   *
   * @return the callNumber
   */
  public String getCallNumber() {
    return callNumber;
  }

  /**
   * Returns the itemId.
   *
   * @return the itemId
   */
  public String getItemId() {
    return itemId;
  }

  /**
   * Returns the dateHoldExpires.
   *
   * @return the dateHoldExpires
   */
  public LocalDate getDateHoldExpires() {
    return dateHoldExpires;
  }

  /**
   * Returns the dateHoldExpiresOnShelf.
   *
   * @return the dateHoldExpiresOnShelf
   */
  public LocalDate getDateHoldExpiresOnShelf() {
    return dateHoldExpiresOnShelf;
  }
}
