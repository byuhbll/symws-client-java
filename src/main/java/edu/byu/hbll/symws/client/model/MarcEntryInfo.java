package edu.byu.hbll.symws.client.model;

import javax.xml.bind.annotation.XmlElement;

/** JAXB bean representing a single MARC tag or entry. */
public class MarcEntryInfo {

  @XmlElement private String label; // Optional
  @XmlElement private String alternateLabel; // Optional

  @XmlElement(name = "entryID")
  private String entryId;

  @XmlElement private String indicators;
  @XmlElement private String text;
  @XmlElement private String unformattedText; // Optional
  @XmlElement private String entryTypeCodes; // Optional
  @XmlElement private String url; // Optional
  @XmlElement private String relatedLookupText; // Optional

  @XmlElement(name = "relatedLookupIndexID")
  private String relatedLookupIndexId; // Optional

  @XmlElement(name = "relatedLookupHeadingTypeID")
  private String relatedLookupHeadingTypeId; // Optional

  /**
   * Returns the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * Returns the alternateLabel.
   *
   * @return the alternateLabel
   */
  public String getAlternateLabel() {
    return alternateLabel;
  }

  /**
   * Returns the entryId.
   *
   * @return the entryId
   */
  public String getEntryId() {
    return entryId;
  }

  /**
   * Returns the indicators.
   *
   * @return the indicators
   */
  public String getIndicators() {
    return indicators;
  }

  /**
   * Returns the text.
   *
   * @return the text
   */
  public String getText() {
    return text;
  }

  /**
   * Returns the unformattedText.
   *
   * @return the unformattedText
   */
  public String getUnformattedText() {
    return unformattedText;
  }

  /**
   * Returns the entryTypeCodes.
   *
   * @return the entryTypeCodes
   */
  public String getEntryTypeCodes() {
    return entryTypeCodes;
  }

  /**
   * Returns the url.
   *
   * @return the url
   */
  public String getUrl() {
    return url;
  }

  /**
   * Returns the relatedLookupText.
   *
   * @return the relatedLookupText
   */
  public String getRelatedLookupText() {
    return relatedLookupText;
  }

  /**
   * Returns the relatedLookupIndexId.
   *
   * @return the relatedLookupIndexId
   */
  public String getRelatedLookupIndexId() {
    return relatedLookupIndexId;
  }

  /**
   * Returns the relatedLookupHeadingTypeId.
   *
   * @return the relatedLookupHeadingTypeId
   */
  public String getRelatedLookupHeadingTypeId() {
    return relatedLookupHeadingTypeId;
  }
}
