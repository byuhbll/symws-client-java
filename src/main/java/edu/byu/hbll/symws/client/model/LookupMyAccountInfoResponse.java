package edu.byu.hbll.symws.client.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class extends {@link LookupPatronInfoResponse} with an {@link XmlRootElement} annotation
 * specific to the lookupMyAccountInfo service.
 */
@XmlRootElement(name = "LookupMyAccountInfoResponse")
public class LookupMyAccountInfoResponse extends LookupPatronInfoResponse {}
