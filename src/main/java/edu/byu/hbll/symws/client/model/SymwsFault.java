package edu.byu.hbll.symws.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JAXB bean representing a SYMWS fault that may (but not always) be included in response to a
 * failed request.
 *
 * <p>Known fault codes are represented as class constants, which can be used to easily test if a
 * fault matches a particular type.
 */
@XmlRootElement(name = "Fault")
public class SymwsFault {

  /** Fault code associated with a failure to create a new SYMWS session. */
  public static final String UNABLE_TO_ACQUIRE_SESSION =
      "com.sirsidynix.hatapi.exceptions.HatSessionException.unableToAcquireSession";

  /** Fault code associated with a duplicate attempt to create/place a hold. */
  public static final String USER_ALREADY_HAS_A_HOLD_ON_THIS_MATERIAL =
      "com.sirsidynix.symws.service.exceptions.ErrorResponseException.$(722)";

  /**
   * Fault code associated with an attempt to use a patron-level session to access a privileged
   * (staff-level) service.
   */
  public static final String ACCESS_DENIED =
      "com.sirsidynix.symws.service.exceptions.SecurityServiceException.accessDenied";

  /**
   * Fault code associated with a failed attempt to retrieve the item status for an item not in the
   * catalog.
   */
  public static final String ITEM_NOT_FOUND_IN_CATALOG =
      "com.sirsidynix.ilsws.util.exceptions.ErrorResponseException.$(7)";

  /**
   * Fault code associated with a failed attempt to authenticate a user (in the new ILSWS format).
   */
  public static final String UNABLE_TO_LOGIN_ILSWS =
      "com.sirsidynix.ilsws.util.exceptions.SecurityServiceException.unableToLogin";

  /**
   * Fault code associated with a failed attempt to authenticate a user (in the old SYMWS format).
   */
  public static final String UNABLE_TO_LOGIN_SYMWS =
      "com.sirsidynix.symws.service.exceptions.SecurityServiceException.unableToLogin";

  /** Fault code associated with failing to provide a required query parameter to a service. */
  public static final String REQUIRED_FIELD_MISSING =
      "com.sirsidynix.ilsws.util.exceptions.ErrorResponseException.#UA$(22)";

  /**
   * Fault code associated with a failed attempt to retrieve the user account for a user not in the
   * catalog.
   */
  public static final String USER_NOT_FOUND =
      "com.sirsidynix.ilsws.util.exceptions.ErrorResponseException.$(2)";

  /** Fault code associated with an expired session. */
  public static final String SESSION_TIMED_OUT =
      "com.sirsidynix.symws.service.exceptions.SecurityServiceException.sessionTimedOut";

  @XmlElement private String code;

  @XmlElement private String string;

  /**
   * Returns the code.
   *
   * @return the code
   */
  public String getCode() {
    return code;
  }

  /**
   * Returns the string.
   *
   * @return the string
   */
  public String getString() {
    return string;
  }
}
