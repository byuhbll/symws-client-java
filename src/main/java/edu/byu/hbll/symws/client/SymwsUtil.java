package edu.byu.hbll.symws.client;

import edu.byu.hbll.symws.client.model.SymwsFault;
import edu.byu.hbll.xml.XmlUtils;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.Supplier;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.JAXBException;
import javax.xml.transform.dom.DOMSource;
import org.w3c.dom.Document;

/** Helper methods used in common by other classes in this package. */
public class SymwsUtil {

  /**
   * The standard message to send when a session is required by a SYMWS client object, but no
   * session data is found.
   */
  public static final String SESSION_REQUIRED_MSG =
      "this service requires a SYMWS session, but the provided"
          + " SymwsClientConfig has no session.";

  private static final List<String> DEAD_SESSION_FAULT_CODES =
      Arrays.asList(
          SymwsFault.SESSION_TIMED_OUT,
          SymwsFault.UNABLE_TO_LOGIN_ILSWS,
          SymwsFault.UNABLE_TO_LOGIN_SYMWS);

  private static final Client client = ClientBuilder.newClient();

  /**
   * Base method used by clients to make a request in SYMWS.
   *
   * <p>If the underlying service requires a SYMWS user session, and no session is currently active
   * in the provided {@link SymwsClientConfig}, an attempt will be made to create a new session and
   * retry the request.
   *
   * @param clientConfig the client configuration to use with this request
   * @param servicePath the path of the service, relative to {@link SymwsClientConfig#getRootUri()}
   * @param queryParams the query params to be included in this request
   * @return the JAX-RS response returned by this request
   * @throws SymwsClientException if the web service returns an error
   */
  public static Response request(
      SymwsClientConfig clientConfig, String servicePath, Map<String, List<Object>> queryParams)
      throws SymwsClientException {
    try {
      return requestWithoutRetry(clientConfig, servicePath, queryParams);
    } catch (SymwsClientException e) {
      // If the service failed because of a missing or expired session, then attempt to retry the
      // service with a
      // new session if possible.  If it fails a second time then go ahead and throw the exception.
      if (e.hasFault()
          && DEAD_SESSION_FAULT_CODES.contains(e.getFault().getCode())
          && clientConfig.newSession()) {
        return requestWithoutRetry(clientConfig, servicePath, queryParams);
      } else {
        throw e;
      }
    }
  }

  /**
   * Helper method used by {@link #request(SymwsClientConfig, String, Map)} to actually make a
   * request attempt.
   *
   * @param clientConfig the client configuration to use with this request; must not be null
   * @param servicePath the path of the service, relative to {@link SymwsClientConfig#getRootUri()};
   *     must not be null or blank
   * @param queryParams the query params to be included in this request; may be null or empty
   * @return the JAX-RS response returned by this request
   * @throws SymwsClientException if the web service returns an error
   */
  public static Response requestWithoutRetry(
      SymwsClientConfig clientConfig, String servicePath, Map<String, List<Object>> queryParams)
      throws SymwsClientException {
    Objects.requireNonNull(clientConfig);
    SymwsUtil.requireNonBlank(servicePath);
    if (queryParams == null) {
      queryParams = Collections.emptyMap();
    }
    URI uri = UriBuilder.fromUri(clientConfig.getRootUri()).path(servicePath).build();

    try {
      WebTarget target = client.target(uri);
      for (Entry<String, List<Object>> entry : queryParams.entrySet()) {
        target = target.queryParam(entry.getKey(), entry.getValue().toArray(new Object[0]));
      }
      Invocation.Builder request =
          target.request().header(SymwsHeaders.X_SIRS_CLIENT_ID, clientConfig.getClientId());
      if (clientConfig.getSessionToken().isPresent()) {
        request =
            request.header(SymwsHeaders.X_SIRS_SESSION_TOKEN, clientConfig.getSessionToken().get());
      }
      Response response = request.get();

      if (response.getStatus() != Status.OK.getStatusCode()) {
        throw new SymwsClientException(uri, response);
      }

      return response;
    } catch (ProcessingException e) {
      throw new SymwsClientException(uri, e);
    }
  }

  /**
   * Parses a JAX-RS {@link Response} into a JAXB bean
   *
   * <p>The {@link Response} is consumed in the process and cannot be read again.
   *
   * @param <T> the class of the JAXB bean which should be returned
   * @param response the JAX-RS response to be consumed and parsed
   * @param target the class of the JAXB bean which should be returned
   * @return a new instance of the target bean, populated with the parsed data
   */
  public static <T> T parseResponse(Response response, Class<T> target) {
    Document xml = XmlUtils.removeNamespaces(response.readEntity(Document.class));
    try {
      return XmlUtils.unmarshal(new DOMSource(xml), target);
    } catch (JAXBException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Checks that the specified {@link CharSequence} is not null or empty.
   *
   * @param <T> the type of the {@link CharSequence} being validated
   * @param cs the {@link CharSequence} to check for nullity and emptiness
   * @return cs if not null or empty
   * @throws NullPointerException if cs is null
   * @throws IllegalArgumentException if cs is empty
   */
  public static <T extends CharSequence> T requireNonEmpty(T cs) {
    return requireNonEmpty(cs, (String) null);
  }

  /**
   * Checks that the specified {@link CharSequence} is not null or empty and throws a customized
   * {@link NullPointerException} or {@link IllegalArgumentException} if it is.
   *
   * @param <T> the type of the {@link CharSequence} being validated
   * @param cs the {@link CharSequence} to check for nullity and emptiness
   * @param message detail message to be used in the event that an exception is thrown
   * @return cs if not null or empty
   * @throws NullPointerException if cs is null
   * @throws IllegalArgumentException if cs is empty
   */
  public static <T extends CharSequence> T requireNonEmpty(T cs, String message) {
    Objects.requireNonNull(cs, message);
    if (cs.length() == 0) {
      throw new IllegalArgumentException(message);
    }
    return cs;
  }

  /**
   * Checks that the specified {@link CharSequence} is not null or empty and throws a customized
   * {@link NullPointerException} or {@link IllegalArgumentException} if it is.
   *
   * <p>Unlike the method {@link #requireNonEmpty(CharSequence, String)}, this method allows
   * creation of the message to be deferred until after the validation is made. While this may
   * confer a performance advantage in the passing case, when deciding to call this method care
   * should be taken that the costs of creating the message supplier are less than the cost of just
   * creating the string message directly.
   *
   * @param <T> the type of the {@link CharSequence} being validated
   * @param cs the {@link CharSequence} to check for nullity and emptiness
   * @param messageSupplier supplier of the detail message to be used in the event that an exception
   *     is thrown
   * @return cs if not null or empty
   * @throws NullPointerException if cs is null
   * @throws IllegalArgumentException if cs is empty
   */
  public static <T extends CharSequence> T requireNonEmpty(T cs, Supplier<String> messageSupplier) {
    Objects.requireNonNull(cs, messageSupplier.get());
    if (cs.length() == 0) {
      throw new IllegalArgumentException(messageSupplier.get());
    }
    return cs;
  }

  /**
   * Checks that the specified {@link CharSequence} is not null, empty, or blank (containing only
   * whitespace).
   *
   * @param <T> the type of the {@link CharSequence} being validated
   * @param cs the {@link CharSequence} to check for nullity and emptiness
   * @return cs if not null or empty
   * @throws NullPointerException if cs is null
   * @throws IllegalArgumentException if cs is empty
   */
  public static <T extends CharSequence> T requireNonBlank(T cs) {
    return requireNonBlank(cs, (String) null);
  }

  /**
   * Checks that the specified {@link CharSequence} is not null or empty and throws a customized
   * {@link NullPointerException} or {@link IllegalArgumentException} if it is.
   *
   * @param <T> the type of the {@link CharSequence} being validated
   * @param cs the {@link CharSequence} to check for nullity and emptiness
   * @param message detail message to be used in the event that an exception is thrown
   * @return cs if not null or empty
   * @throws NullPointerException if cs is null
   * @throws IllegalArgumentException if cs is empty
   */
  public static <T extends CharSequence> T requireNonBlank(T cs, String message) {
    Objects.requireNonNull(cs, message);
    if (cs.length() == 0 || cs.chars().allMatch(Character::isWhitespace)) {
      throw new IllegalArgumentException(message);
    }
    return cs;
  }

  /**
   * Checks that the specified {@link CharSequence} is not null, empty, or blank (containing only
   * whitespace), and throws a customized {@link NullPointerException} or {@link
   * IllegalArgumentException} if it is.
   *
   * <p>Unlike the method {@link #requireNonEmpty(CharSequence, String)}, this method allows
   * creation of the message to be deferred until after the validation is made. While this may
   * confer a performance advantage in the passing case, when deciding to call this method care
   * should be taken that the costs of creating the message supplier are less than the cost of just
   * creating the string message directly.
   *
   * @param <T> the type of the {@link CharSequence} being validated
   * @param cs the {@link CharSequence} to check for nullity and emptiness
   * @param messageSupplier supplier of the detail message to be used in the event that an exception
   *     is thrown
   * @return cs if not null or empty
   * @throws NullPointerException if cs is null
   * @throws IllegalArgumentException if cs is empty
   */
  public static <T extends CharSequence> T requireNonBlank(T cs, Supplier<String> messageSupplier) {
    Objects.requireNonNull(cs, messageSupplier);
    if (cs.length() == 0 || cs.chars().allMatch(Character::isWhitespace)) {
      throw new IllegalArgumentException(messageSupplier.get());
    }
    return cs;
  }

  /**
   * Constructs a new instance; since this is a utility class, this constructor will never be used.
   */
  private SymwsUtil() {
    // Do nothing.
  }

  /**
   * Checks that the specified {@link Comparable} is not null and greater than or equal to another
   * value.
   *
   * @param <T> the type of the {@link Comparable} being validated
   * @param obj the {@link Comparable} to check for nullity and comparison
   * @param other the other value against which to compare obj
   * @return obj if not null or empty
   * @throws NullPointerException if obj or other is null
   * @throws IllegalArgumentException if obj is not greater than or equal to other
   */
  public static <T extends Comparable<T>> T requireGreaterThanOrEqualTo(T obj, T other) {
    return requireGreaterThanOrEqualTo(obj, other, (String) null);
  }

  /**
   * Checks that the specified {@link Comparable} is not null and greater than or equal to another
   * value and throws a customized {@link NullPointerException} or {@link IllegalArgumentException}
   * if it is not.
   *
   * @param <T> the type of the {@link Comparable} being validated
   * @param obj the {@link Comparable} to check for nullity and comparison
   * @param other the other value against which to compare obj
   * @param message detail message to be used in the event that an exception is thrown
   * @return obj if not null or empty
   * @throws NullPointerException if obj or other is null
   * @throws IllegalArgumentException if obj is not greater than or equal to other
   */
  public static <T extends Comparable<T>> T requireGreaterThanOrEqualTo(
      T obj, T other, String message) {
    Objects.requireNonNull(obj, message);
    Objects.requireNonNull(other, message);
    if (obj.compareTo(other) < 0) {
      throw new IllegalArgumentException(message);
    }
    return obj;
  }

  /**
   * Checks that the specified {@link Comparable} is not null and greater than or equal to another
   * value and throws a customized {@link NullPointerException} or {@link IllegalArgumentException}
   * if it is not.
   *
   * <p>Unlike the method {@link #requireGreaterThanOrEqualTo(Comparable, Comparable, String)}, this
   * method allows creation of the message to be deferred until after the validation is made. While
   * this may confer a performance advantage in the passing case, when deciding to call this method
   * care should be taken that the costs of creating the message supplier are less than the cost of
   * just creating the string message directly.
   *
   * @param <T> the type of the {@link Comparable} being validated
   * @param obj the {@link Comparable} to check for nullity and comparison
   * @param other the other value against which to compare obj
   * @param messageSupplier supplier of the detail message to be used in the event that an exception
   *     is thrown
   * @return obj if not null or empty
   * @throws NullPointerException if obj or other is null
   * @throws IllegalArgumentException if obj is not greater than or equal to other
   */
  public static <T extends Comparable<T>> T requireGreaterThanOrEqualTo(
      T obj, T other, Supplier<String> messageSupplier) {
    Objects.requireNonNull(obj, messageSupplier);
    Objects.requireNonNull(other, messageSupplier);
    if (obj.compareTo(other) < 0) {
      throw new IllegalArgumentException(messageSupplier.get());
    }
    return obj;
  }
}
