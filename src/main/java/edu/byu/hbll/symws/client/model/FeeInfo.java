package edu.byu.hbll.symws.client.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing a patron's fees.
 *
 * <p>From the SYMWS documentation: "The FeeInfo type displays information about a particular
 * patron's fees and bills."
 */
public class FeeInfo {

  @XmlElement private Integer billNumber;

  @XmlElement(name = "billReasonID")
  private String billReasonId;

  @XmlElement private String billReasonDescription; // Optional
  @XmlElement private Money amount;
  @XmlElement private Money taxAmount; // Optional
  @XmlElement private Money amountOutstanding;

  @XmlElement(name = "billLibraryID")
  private String billLibraryId; // Optional

  @XmlElement private String billLibraryDescription; // Optional
  @XmlElement private LocalDate dateBilled;
  @XmlElement private LocalDate dateCreated; // Optional
  @XmlElement private Integer numberOfPaymentsMade;
  @XmlElement private LocalDate lastPaymentDate; // Optional
  @XmlElement private List<FeePaymentInfo> feePaymentInfo = new ArrayList<>();
  @XmlElement private FeeItemInfo feeItemInfo; // Optional

  /**
   * Returns the billNumber.
   *
   * @return the billNumber
   */
  public Integer getBillNumber() {
    return billNumber;
  }

  /**
   * Returns the billReasonId.
   *
   * @return the billReasonId
   */
  public String getBillReasonId() {
    return billReasonId;
  }

  /**
   * Returns the billReasonDescription.
   *
   * @return the billReasonDescription
   */
  public String getBillReasonDescription() {
    return billReasonDescription;
  }

  /**
   * Returns the amount.
   *
   * @return the amount
   */
  public Money getAmount() {
    return amount;
  }

  /**
   * Returns the taxAmount.
   *
   * @return the taxAmount
   */
  public Money getTaxAmount() {
    return taxAmount;
  }

  /**
   * Returns the amountOutstanding.
   *
   * @return the amountOutstanding
   */
  public Money getAmountOutstanding() {
    return amountOutstanding;
  }

  /**
   * Returns the billLibraryId.
   *
   * @return the billLibraryId
   */
  public String getBillLibraryId() {
    return billLibraryId;
  }

  /**
   * Returns the billLibraryDescription.
   *
   * @return the billLibraryDescription
   */
  public String getBillLibraryDescription() {
    return billLibraryDescription;
  }

  /**
   * Returns the dateBilled.
   *
   * @return the dateBilled
   */
  public LocalDate getDateBilled() {
    return dateBilled;
  }

  /**
   * Returns the dateCreated.
   *
   * @return the dateCreated
   */
  public LocalDate getDateCreated() {
    return dateCreated;
  }

  /**
   * Returns the numberOfPaymentsMade.
   *
   * @return the numberOfPaymentsMade
   */
  public Integer getNumberOfPaymentsMade() {
    return numberOfPaymentsMade;
  }

  /**
   * Returns the lastPaymentDate.
   *
   * @return the lastPaymentDate
   */
  public LocalDate getLastPaymentDate() {
    return lastPaymentDate;
  }

  /**
   * Returns the feePaymentInfo.
   *
   * @return the feePaymentInfo
   */
  public List<FeePaymentInfo> getFeePaymentInfo() {
    return feePaymentInfo;
  }

  /**
   * Returns the feeItemInfo.
   *
   * @return the feeItemInfo
   */
  public FeeItemInfo getFeeItemInfo() {
    return feeItemInfo;
  }
}
