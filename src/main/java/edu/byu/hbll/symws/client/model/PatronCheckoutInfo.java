package edu.byu.hbll.symws.client.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing a current checkout.
 *
 * <p>From the SYMWS documentation: "The PatronCheckoutInfo type displays specific checkout
 * information for a particular patron."
 */
public class PatronCheckoutInfo {

  @XmlElement private Long titleKey; // Optional

  @XmlElement(name = "itemID")
  private String itemId;

  @XmlElement private String callNumber;
  @XmlElement private Integer copyNumber;
  @XmlElement private Integer pieces;
  @XmlElement private Integer numberOfPiecesReturned; // Optional
  @XmlElement private String title; // Optional
  @XmlElement private String author; // Optional

  @XmlElement(name = "checkoutLibraryID")
  private String checkoutLibraryId; // Optional

  @XmlElement private String checkoutLibraryDescription; // Optional

  @XmlElement(name = "itemLibraryID")
  private String itemLibraryId; // Optional

  @XmlElement private String itemLibraryDescription; // Optional

  @XmlElement(name = "itemTypeID")
  private String itemTypeId; // Optional

  @XmlElement private String itemTypeDescription; // Optional

  @XmlElement(name = "reserveCollectionID")
  private String reserveCollectionId; // Optional

  @XmlElement private String reserveCollectionDescription; // Optional
  @XmlElement private LocalDateTime checkoutDate;
  @XmlElement private LocalDateTime dueDate;
  @XmlElement private LocalDate recallDate; // Optional
  @XmlElement private LocalDateTime recallDueDate; // Optional
  @XmlElement private Integer recallNoticesSent; // Optional
  @XmlElement private LocalDateTime lastRenewalDate; // Optional
  @XmlElement private Integer renewals;
  @XmlElement private Integer unseenRenewals;
  @XmlElement private Integer renewalsRemaining; // Optional
  @XmlElement private Boolean renewalsRemainingUnlimited; // Optional
  @XmlElement private Integer unseenRenewalsRemaining; // Optional
  @XmlElement private Boolean unseenRenewalsRemainingUnlimited; // Optional
  @XmlElement private Boolean overdue;
  @XmlElement private Integer overdueNoticesSent;
  @XmlElement private LocalDate dateOfLastOverdueNotice; // Optional
  @XmlElement private Money fine; // Optional

  /**
   * Returns the titleKey.
   *
   * @return the titleKey
   */
  public Long getTitleKey() {
    return titleKey;
  }

  /**
   * Returns the itemId.
   *
   * @return the itemId
   */
  public String getItemId() {
    return itemId;
  }

  /**
   * Returns the callNumber.
   *
   * @return the callNumber
   */
  public String getCallNumber() {
    return callNumber;
  }

  /**
   * Returns the copyNumber.
   *
   * @return the copyNumber
   */
  public Integer getCopyNumber() {
    return copyNumber;
  }

  /**
   * Returns the pieces.
   *
   * @return the pieces
   */
  public Integer getPieces() {
    return pieces;
  }

  /**
   * Returns the numberOfPiecesReturned.
   *
   * @return the numberOfPiecesReturned
   */
  public Integer getNumberOfPiecesReturned() {
    return numberOfPiecesReturned;
  }

  /**
   * Returns the title.
   *
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * Returns the author.
   *
   * @return the author
   */
  public String getAuthor() {
    return author;
  }

  /**
   * Returns the checkoutLibraryId.
   *
   * @return the checkoutLibraryId
   */
  public String getCheckoutLibraryId() {
    return checkoutLibraryId;
  }

  /**
   * Returns the checkoutLibraryDescription.
   *
   * @return the checkoutLibraryDescription
   */
  public String getCheckoutLibraryDescription() {
    return checkoutLibraryDescription;
  }

  /**
   * Returns the itemLibraryId.
   *
   * @return the itemLibraryId
   */
  public String getItemLibraryId() {
    return itemLibraryId;
  }

  /**
   * Returns the itemLibraryDescription.
   *
   * @return the itemLibraryDescription
   */
  public String getItemLibraryDescription() {
    return itemLibraryDescription;
  }

  /**
   * Returns the itemTypeId.
   *
   * @return the itemTypeId
   */
  public String getItemTypeId() {
    return itemTypeId;
  }

  /**
   * Returns the itemTypeDescription.
   *
   * @return the itemTypeDescription
   */
  public String getItemTypeDescription() {
    return itemTypeDescription;
  }

  /**
   * Returns the reserveCollectionId.
   *
   * @return the reserveCollectionId
   */
  public String getReserveCollectionId() {
    return reserveCollectionId;
  }

  /**
   * Returns the reserveCollectionDescription.
   *
   * @return the reserveCollectionDescription
   */
  public String getReserveCollectionDescription() {
    return reserveCollectionDescription;
  }

  /**
   * Returns the checkoutDate.
   *
   * @return the checkoutDate
   */
  public LocalDateTime getCheckoutDate() {
    return checkoutDate;
  }

  /**
   * Returns the dueDate.
   *
   * @return the dueDate
   */
  public LocalDateTime getDueDate() {
    return dueDate;
  }

  /**
   * Returns the recallDate.
   *
   * @return the recallDate
   */
  public LocalDate getRecallDate() {
    return recallDate;
  }

  /**
   * Returns the recallDueDate.
   *
   * @return the recallDueDate
   */
  public LocalDateTime getRecallDueDate() {
    return recallDueDate;
  }

  /**
   * Returns the recallNoticesSent.
   *
   * @return the recallNoticesSent
   */
  public Integer getRecallNoticesSent() {
    return recallNoticesSent;
  }

  /**
   * Returns the lastRenewalDate.
   *
   * @return the lastRenewalDate
   */
  public LocalDateTime getLastRenewalDate() {
    return lastRenewalDate;
  }

  /**
   * Returns the renewals.
   *
   * @return the renewals
   */
  public Integer getRenewals() {
    return renewals;
  }

  /**
   * Returns the unseenRenewals.
   *
   * @return the unseenRenewals
   */
  public Integer getUnseenRenewals() {
    return unseenRenewals;
  }

  /**
   * Returns the renewalsRemaining.
   *
   * @return the renewalsRemaining
   */
  public Integer getRenewalsRemaining() {
    return renewalsRemaining;
  }

  /**
   * Returns the renewalsRemainingUnlimited.
   *
   * @return the renewalsRemainingUnlimited
   */
  public Boolean getRenewalsRemainingUnlimited() {
    return renewalsRemainingUnlimited;
  }

  /**
   * Returns the unseenRenewalsRemaining.
   *
   * @return the unseenRenewalsRemaining
   */
  public Integer getUnseenRenewalsRemaining() {
    return unseenRenewalsRemaining;
  }

  /**
   * Returns the unseenRenewalsRemainingUnlimited.
   *
   * @return the unseenRenewalsRemainingUnlimited
   */
  public Boolean getUnseenRenewalsRemainingUnlimited() {
    return unseenRenewalsRemainingUnlimited;
  }

  /**
   * Returns the overdue.
   *
   * @return the overdue
   */
  public Boolean getOverdue() {
    return overdue;
  }

  /**
   * Returns the overdueNoticesSent.
   *
   * @return the overdueNoticesSent
   */
  public Integer getOverdueNoticesSent() {
    return overdueNoticesSent;
  }

  /**
   * Returns the dateOfLastOverdueNotice.
   *
   * @return the dateOfLastOverdueNotice
   */
  public LocalDate getDateOfLastOverdueNotice() {
    return dateOfLastOverdueNotice;
  }

  /**
   * Returns the fine.
   *
   * @return the fine
   */
  public Money getFine() {
    return fine;
  }
}
