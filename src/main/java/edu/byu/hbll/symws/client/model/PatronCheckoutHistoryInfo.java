package edu.byu.hbll.symws.client.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing a previous checkout.
 *
 * <p>From the SYMWS documentation: "The PatronCheckoutHistoryInfo type displays specific checkout
 * (charge) history information for a particular patron."
 */
public class PatronCheckoutHistoryInfo {

  @XmlElement(name = "itemID")
  private String itemId;

  @XmlElement private String callNumber;
  @XmlElement private String displayableCallNumber;
  @XmlElement private Integer copyNumber;
  @XmlElement private String title; // Optional
  @XmlElement private String author; // Optional

  @XmlElement(name = "checkoutLibraryID")
  private String checkoutLibraryId; // Optional

  @XmlElement private String checkoutLibraryDescription; // Optional

  @XmlElement(name = "itemLibraryID")
  private String itemLibraryId; // Optional

  @XmlElement private String itemLibraryDescription; // Optional

  @XmlElement(name = "itemTypeID")
  private String itemTypeId; // Optional

  @XmlElement private String itemTypeDescription; // Optional

  @XmlElement(name = "reserveCollectionID")
  private String reserveCollectionId; // Optional

  @XmlElement private String reserveCollectionDescription; // Optional
  @XmlElement private LocalDateTime checkoutDate;
  @XmlElement private LocalDateTime dueDate;

  @XmlElement(name = "circulationRuleID")
  private String circulationRuleId; // Optional

  @XmlElement private String circulationRuleDescription; // Optional
  @XmlElement private LocalDate recallDate; // Optional
  @XmlElement private LocalDateTime recallDueDate; // Optional
  @XmlElement private Integer recallNoticesSent; // Optional
  @XmlElement private LocalDateTime lastRenewedDate; // Optional
  @XmlElement private Integer renewals;
  @XmlElement private Boolean wasOverdue;
  @XmlElement private Integer overdueNoticesSent;
  @XmlElement private LocalDate dateOfLastOverdueNotice; // Optional
  @XmlElement private Money fine; // Optional
  @XmlElement private LocalDateTime dateLoanHistoryDischarged; // Optional

  @XmlElement(name = "loanHistoryDischargingLibraryID")
  private String loanHistoryDischargingLibraryId; // Optional

  @XmlElement private String loanHistoryDischargingLibraryDescription; // Optional

  /**
   * Returns the itemId.
   *
   * @return the itemId
   */
  public String getItemId() {
    return itemId;
  }

  /**
   * Returns the callNumber.
   *
   * @return the callNumber
   */
  public String getCallNumber() {
    return callNumber;
  }

  /**
   * Returns the displayableCallNumber.
   *
   * @return the displayableCallNumber
   */
  public String getDisplayableCallNumber() {
    return displayableCallNumber;
  }

  /**
   * Returns the copyNumber.
   *
   * @return the copyNumber
   */
  public Integer getCopyNumber() {
    return copyNumber;
  }

  /**
   * Returns the title.
   *
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * Returns the author.
   *
   * @return the author
   */
  public String getAuthor() {
    return author;
  }

  /**
   * Returns the checkoutLibraryId.
   *
   * @return the checkoutLibraryId
   */
  public String getCheckoutLibraryId() {
    return checkoutLibraryId;
  }

  /**
   * Returns the checkoutLibraryDescription.
   *
   * @return the checkoutLibraryDescription
   */
  public String getCheckoutLibraryDescription() {
    return checkoutLibraryDescription;
  }

  /**
   * Returns the itemLibraryId.
   *
   * @return the itemLibraryId
   */
  public String getItemLibraryId() {
    return itemLibraryId;
  }

  /**
   * Returns the itemLibraryDescription.
   *
   * @return the itemLibraryDescription
   */
  public String getItemLibraryDescription() {
    return itemLibraryDescription;
  }

  /**
   * Returns the itemTypeId.
   *
   * @return the itemTypeId
   */
  public String getItemTypeId() {
    return itemTypeId;
  }

  /**
   * Returns the itemTypeDescription.
   *
   * @return the itemTypeDescription
   */
  public String getItemTypeDescription() {
    return itemTypeDescription;
  }

  /**
   * Returns the reserveCollectionId.
   *
   * @return the reserveCollectionId
   */
  public String getReserveCollectionId() {
    return reserveCollectionId;
  }

  /**
   * Returns the reserveCollectionDescription.
   *
   * @return the reserveCollectionDescription
   */
  public String getReserveCollectionDescription() {
    return reserveCollectionDescription;
  }

  /**
   * Returns the checkoutDate.
   *
   * @return the checkoutDate
   */
  public LocalDateTime getCheckoutDate() {
    return checkoutDate;
  }

  /**
   * Returns the dueDate.
   *
   * @return the dueDate
   */
  public LocalDateTime getDueDate() {
    return dueDate;
  }

  /**
   * Returns the circulationRuleId.
   *
   * @return the circulationRuleId
   */
  public String getCirculationRuleId() {
    return circulationRuleId;
  }

  /**
   * Returns the circulationRuleDescription.
   *
   * @return the circulationRuleDescription
   */
  public String getCirculationRuleDescription() {
    return circulationRuleDescription;
  }

  /**
   * Returns the recallDate.
   *
   * @return the recallDate
   */
  public LocalDate getRecallDate() {
    return recallDate;
  }

  /**
   * Returns the recallDueDate.
   *
   * @return the recallDueDate
   */
  public LocalDateTime getRecallDueDate() {
    return recallDueDate;
  }

  /**
   * Returns the recallNoticesSent.
   *
   * @return the recallNoticesSent
   */
  public Integer getRecallNoticesSent() {
    return recallNoticesSent;
  }

  /**
   * Returns the lastRenewedDate.
   *
   * @return the lastRenewedDate
   */
  public LocalDateTime getLastRenewedDate() {
    return lastRenewedDate;
  }

  /**
   * Returns the renewals.
   *
   * @return the renewals
   */
  public Integer getRenewals() {
    return renewals;
  }

  /**
   * Returns the wasOverdue.
   *
   * @return the wasOverdue
   */
  public Boolean getWasOverdue() {
    return wasOverdue;
  }

  /**
   * Returns the overdueNoticesSent.
   *
   * @return the overdueNoticesSent
   */
  public Integer getOverdueNoticesSent() {
    return overdueNoticesSent;
  }

  /**
   * Returns the dateOfLastOverdueNotice.
   *
   * @return the dateOfLastOverdueNotice
   */
  public LocalDate getDateOfLastOverdueNotice() {
    return dateOfLastOverdueNotice;
  }

  /**
   * Returns the fine.
   *
   * @return the fine
   */
  public Money getFine() {
    return fine;
  }

  /**
   * Returns the dateLoanHistoryDischarged.
   *
   * @return the dateLoanHistoryDischarged
   */
  public LocalDateTime getDateLoanHistoryDischarged() {
    return dateLoanHistoryDischarged;
  }

  /**
   * Returns the loanHistoryDischargingLibraryId.
   *
   * @return the loanHistoryDischargingLibraryId
   */
  public String getLoanHistoryDischargingLibraryId() {
    return loanHistoryDischargingLibraryId;
  }

  /**
   * Returns the loanHistoryDischargingLibraryDescription.
   *
   * @return the loanHistoryDischargingLibraryDescription
   */
  public String getLoanHistoryDischargingLibraryDescription() {
    return loanHistoryDischargingLibraryDescription;
  }
}
