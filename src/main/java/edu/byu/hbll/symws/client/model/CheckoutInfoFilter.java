package edu.byu.hbll.symws.client.model;

/**
 * Enumeration of possible checkout types.
 *
 * <p>From the SYMWS documentation: "The CheckoutInfoFilter type contains enumeration values that
 * you can use in the lookupMyAccountInfo operation request to limit your results when you retrieve
 * checkout information."
 */
public enum CheckoutInfoFilter {
  ALL,
  ALLEXCEPTLOST,
  ALLEXCEPTCLAIMEDRETURN,
  CLAIMEDRETURNED,
  LOST,
  OVERDUE,
  RENEWED,
  RECALLED;
}
