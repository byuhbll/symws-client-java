package edu.byu.hbll.symws.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/** JAXB bean representing a Symphony user or patron. */
@XmlRootElement(name = "LookupSessionInfoResponse")
public class LookupSessionInfoResponse {

  @XmlElement(name = "userID")
  private String userId;

  @XmlElement(name = "userAccessID")
  private String userAccessId;

  @XmlElement private String userAccessDescription;

  @XmlElement(name = "userEnvironmentID")
  private String userEnvironmentId;

  @XmlElement private String userEnvironmentDescription;

  @XmlElement(name = "userProfileID")
  private String userProfileId;

  @XmlElement private String userProfileDescription;

  @XmlElement(name = "userLibraryID")
  private String userLibraryId;

  @XmlElement private String userLibraryDescription;
  @XmlElement private String serverIPAddress;
  @XmlElement private String serverPlatform;

  @XmlElement(name = "ServerVersion")
  private String serverVersion;

  /**
   * Returns the userId.
   *
   * @return the userId
   */
  public String getUserId() {
    return userId;
  }

  /**
   * Returns the userAccessId.
   *
   * @return the userAccessId
   */
  public String getUserAccessId() {
    return userAccessId;
  }

  /**
   * Returns the userAccessDescription.
   *
   * @return the userAccessDescription
   */
  public String getUserAccessDescription() {
    return userAccessDescription;
  }

  /**
   * Returns the userEnvironmentId.
   *
   * @return the userEnvironmentId
   */
  public String getUserEnvironmentId() {
    return userEnvironmentId;
  }

  /**
   * Returns the userEnvironmentDescription.
   *
   * @return the userEnvironmentDescription
   */
  public String getUserEnvironmentDescription() {
    return userEnvironmentDescription;
  }

  /**
   * Returns the userProfileId.
   *
   * @return the userProfileId
   */
  public String getUserProfileId() {
    return userProfileId;
  }

  /**
   * Returns the userProfileDescription.
   *
   * @return the userProfileDescription
   */
  public String getUserProfileDescription() {
    return userProfileDescription;
  }

  /**
   * Returns the userLibraryId.
   *
   * @return the userLibraryId
   */
  public String getUserLibraryId() {
    return userLibraryId;
  }

  /**
   * Returns the userLibraryDescription.
   *
   * @return the userLibraryDescription
   */
  public String getUserLibraryDescription() {
    return userLibraryDescription;
  }

  /**
   * Returns the serverIPAddress.
   *
   * @return the serverIPAddress
   */
  public String getServerIPAddress() {
    return serverIPAddress;
  }

  /**
   * Returns the serverPlatform.
   *
   * @return the serverPlatform
   */
  public String getServerPlatform() {
    return serverPlatform;
  }

  /**
   * Returns the serverVersion.
   *
   * @return the serverVersion
   */
  public String getServerVersion() {
    return serverVersion;
  }
}
