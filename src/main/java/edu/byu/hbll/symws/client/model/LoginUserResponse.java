package edu.byu.hbll.symws.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/** JAXB bean representing a successful response to a user login request. */
@XmlRootElement(name = "LoginUserResponse")
public final class LoginUserResponse {

  @XmlElement(name = "userID")
  private String userId;

  @XmlElement private String sessionToken;

  /**
   * Returns the userId.
   *
   * @return the userId
   */
  public String getUserId() {
    return userId;
  }

  /**
   * Returns the sessionToken.
   *
   * @return the sessionToken
   */
  public String getSessionToken() {
    return sessionToken;
  }
}
