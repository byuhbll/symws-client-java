package edu.byu.hbll.symws.client.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing group-level checkout data.
 *
 * <p>From the SYMWS documentation: "The GroupMemberCheckoutInfo type displays checkout information
 * for each User Group member."
 */
public class GroupMemberCheckoutInfo extends PatronCheckoutInfo {

  @XmlElement(name = "userID")
  private String userId;

  @XmlElement private String userName;

  /**
   * Returns the userId.
   *
   * @return the userId
   */
  public String getUserId() {
    return userId;
  }

  /**
   * Returns the userName.
   *
   * @return the userName
   */
  public String getUserName() {
    return userName;
  }
}
