package edu.byu.hbll.symws.client.adaper;

import java.time.LocalDate;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/** {@link XmlAdapter} for marshalling and unmarshalling {@link LocalDate} objects. */
public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

  @Override
  public LocalDate unmarshal(String v) {
    return LocalDate.parse(v);
  }

  @Override
  public String marshal(LocalDate v) {
    return v.toString();
  }
}
