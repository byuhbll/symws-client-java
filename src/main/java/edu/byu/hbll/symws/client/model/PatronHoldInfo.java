package edu.byu.hbll.symws.client.model;

import java.time.LocalDate;
import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing a hold placed by a patron on an item.
 *
 * <p>From the SYMWS documentation: "The PatronHoldInfo type displays specific hold information for
 * a particular patron."
 */
public class PatronHoldInfo {

  /** The hold status integer corresponding with an inactive hold. */
  public static final int STATUS_INACTIVE = 1;

  /** The hold status integer corresponding with an active hold. */
  public static final int STATUS_ACTIVE = 2;

  @XmlElement private Long holdKey;
  @XmlElement private Long titleKey;

  @XmlElement(name = "itemID")
  private String itemId; // Optional

  @XmlElement private String callNumber; // Optional
  @XmlElement private String displayableCallNumber; // Optional

  @XmlElement(name = "blanketHoldID")
  private String blanketHoldId; // Optional

  @XmlElement private Integer blanketHoldCopiesNeeded; // Optional
  @XmlElement private Integer blanketHoldCopiesReceived; // Optional
  @XmlElement private LocalDate expiresDate; // Optional
  @XmlElement private Integer holdStatus;

  @XmlElement(name = "holdInactiveReasonID")
  private String holdInactiveReasonId; // Optional

  @XmlElement private HoldInactiveType holdInactiveType; // Optional

  @XmlElement(name = "itemTypeID")
  private String itemTypeId; // Optional

  @XmlElement private String itemTypeDescription; // Optional
  @XmlElement private String title;
  @XmlElement private String author; // Optional

  @XmlElement(name = "itemLibraryID")
  private String itemLibraryId; // Optional

  @XmlElement private String itemLibraryDescription; // Optional

  @XmlElement(name = "pickupLibraryID")
  private String pickupLibraryId; // Optional

  @XmlElement private String pickupLibraryDescription; // Optional
  @XmlElement private LocalDate placedDate;

  @XmlElement(name = "mailServiceID")
  private String mailServiceId; // Optional

  @XmlElement private String mailServiceDescription; // Optional
  @XmlElement private Boolean reserve;
  @XmlElement private String recallStatus;
  @XmlElement private Boolean available;
  @XmlElement private Boolean intransit;
  @XmlElement private LocalDate availableDate; // Optional
  @XmlElement private LocalDate availableExpiresDate; // Optional
  @XmlElement private Integer queuePosition;
  @XmlElement private Integer queueLength;
  @XmlElement private LocalDate suspendStartDate; // Optional
  @XmlElement private LocalDate suspendEndDate; // Optional
  @XmlElement private Boolean holdPlacedWithOverride; // Optional
  @XmlElement private Integer orderFiscalCycle; // Optional

  @XmlElement(name = "orderID")
  private String orderId; // Optional

  @XmlElement(name = "orderLibraryID")
  private String orderLibraryId; // Optional

  @XmlElement private String orderLibraryDescription; // Optional
  @XmlElement private String orderLine; // Optional

  /**
   * Returns the holdKey.
   *
   * @return the holdKey
   */
  public Long getHoldKey() {
    return holdKey;
  }

  /**
   * Returns the titleKey.
   *
   * @return the titleKey
   */
  public Long getTitleKey() {
    return titleKey;
  }

  /**
   * Returns the itemId.
   *
   * @return the itemId
   */
  public String getItemId() {
    return itemId;
  }

  /**
   * Returns the callNumber.
   *
   * @return the callNumber
   */
  public String getCallNumber() {
    return callNumber;
  }

  /**
   * Returns the displayableCallNumber.
   *
   * @return the displayableCallNumber
   */
  public String getDisplayableCallNumber() {
    return displayableCallNumber;
  }

  /**
   * Returns the blanketHoldId.
   *
   * @return the blanketHoldId
   */
  public String getBlanketHoldId() {
    return blanketHoldId;
  }

  /**
   * Returns the blanketHoldCopiesNeeded.
   *
   * @return the blanketHoldCopiesNeeded
   */
  public Integer getBlanketHoldCopiesNeeded() {
    return blanketHoldCopiesNeeded;
  }

  /**
   * Returns the blanketHoldCopiesReceived.
   *
   * @return the blanketHoldCopiesReceived
   */
  public Integer getBlanketHoldCopiesReceived() {
    return blanketHoldCopiesReceived;
  }

  /**
   * Returns the expiresDate.
   *
   * @return the expiresDate
   */
  public LocalDate getExpiresDate() {
    return expiresDate;
  }

  /**
   * Returns the holdStatus.
   *
   * @return the holdStatus
   */
  public Integer getHoldStatus() {
    return holdStatus;
  }

  /**
   * Returns the holdInactiveReasonId.
   *
   * @return the holdInactiveReasonId
   */
  public String getHoldInactiveReasonId() {
    return holdInactiveReasonId;
  }

  /**
   * Returns the holdInactiveType.
   *
   * @return the holdInactiveType
   */
  public HoldInactiveType getHoldInactiveType() {
    return holdInactiveType;
  }

  /**
   * Returns the itemTypeId.
   *
   * @return the itemTypeId
   */
  public String getItemTypeId() {
    return itemTypeId;
  }

  /**
   * Returns the itemTypeDescription.
   *
   * @return the itemTypeDescription
   */
  public String getItemTypeDescription() {
    return itemTypeDescription;
  }

  /**
   * Returns the title.
   *
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * Returns the author.
   *
   * @return the author
   */
  public String getAuthor() {
    return author;
  }

  /**
   * Returns the itemLibraryId.
   *
   * @return the itemLibraryId
   */
  public String getItemLibraryId() {
    return itemLibraryId;
  }

  /**
   * Returns the itemLibraryDescription.
   *
   * @return the itemLibraryDescription
   */
  public String getItemLibraryDescription() {
    return itemLibraryDescription;
  }

  /**
   * Returns the pickupLibraryId.
   *
   * @return the pickupLibraryId
   */
  public String getPickupLibraryId() {
    return pickupLibraryId;
  }

  /**
   * Returns the pickupLibraryDescription.
   *
   * @return the pickupLibraryDescription
   */
  public String getPickupLibraryDescription() {
    return pickupLibraryDescription;
  }

  /**
   * Returns the placedDate.
   *
   * @return the placedDate
   */
  public LocalDate getPlacedDate() {
    return placedDate;
  }

  /**
   * Returns the mailServiceId.
   *
   * @return the mailServiceId
   */
  public String getMailServiceId() {
    return mailServiceId;
  }

  /**
   * Returns the mailServiceDescription.
   *
   * @return the mailServiceDescription
   */
  public String getMailServiceDescription() {
    return mailServiceDescription;
  }

  /**
   * Returns the reserve.
   *
   * @return the reserve
   */
  public Boolean getReserve() {
    return reserve;
  }

  /**
   * Returns the recallStatus.
   *
   * @return the recallStatus
   */
  public String getRecallStatus() {
    return recallStatus;
  }

  /**
   * Returns the available.
   *
   * @return the available
   */
  public Boolean getAvailable() {
    return available;
  }

  /**
   * Returns the intransit.
   *
   * @return the intransit
   */
  public Boolean getIntransit() {
    return intransit;
  }

  /**
   * Returns the availableDate.
   *
   * @return the availableDate
   */
  public LocalDate getAvailableDate() {
    return availableDate;
  }

  /**
   * Returns the availableExpiresDate.
   *
   * @return the availableExpiresDate
   */
  public LocalDate getAvailableExpiresDate() {
    return availableExpiresDate;
  }

  /**
   * Returns the queuePosition.
   *
   * @return the queuePosition
   */
  public Integer getQueuePosition() {
    return queuePosition;
  }

  /**
   * Returns the queueLength.
   *
   * @return the queueLength
   */
  public Integer getQueueLength() {
    return queueLength;
  }

  /**
   * Returns the suspendStartDate.
   *
   * @return the suspendStartDate
   */
  public LocalDate getSuspendStartDate() {
    return suspendStartDate;
  }

  /**
   * Returns the suspendEndDate.
   *
   * @return the suspendEndDate
   */
  public LocalDate getSuspendEndDate() {
    return suspendEndDate;
  }

  /**
   * Returns the holdPlacedWithOverride.
   *
   * @return the holdPlacedWithOverride
   */
  public Boolean getHoldPlacedWithOverride() {
    return holdPlacedWithOverride;
  }

  /**
   * Returns the orderFiscalCycle.
   *
   * @return the orderFiscalCycle
   */
  public Integer getOrderFiscalCycle() {
    return orderFiscalCycle;
  }

  /**
   * Returns the orderId.
   *
   * @return the orderId
   */
  public String getOrderId() {
    return orderId;
  }

  /**
   * Returns the orderLibraryId.
   *
   * @return the orderLibraryId
   */
  public String getOrderLibraryId() {
    return orderLibraryId;
  }

  /**
   * Returns the orderLibraryDescription.
   *
   * @return the orderLibraryDescription
   */
  public String getOrderLibraryDescription() {
    return orderLibraryDescription;
  }

  /**
   * Returns the orderLine.
   *
   * @return the orderLine
   */
  public String getOrderLine() {
    return orderLine;
  }
}
