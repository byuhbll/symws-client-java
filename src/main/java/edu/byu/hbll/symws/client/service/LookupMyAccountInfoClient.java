package edu.byu.hbll.symws.client.service;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.SymwsClientException;
import edu.byu.hbll.symws.client.SymwsUtil;
import edu.byu.hbll.symws.client.model.LookupMyAccountInfoResponse;
import edu.byu.hbll.symws.client.model.LookupPatronInfoResponse;
import edu.byu.hbll.symws.client.service.LookupPatronInfoClient.OptionalParams;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Response;

/**
 * Client for looking up account information about the currently signed in SYMWS user.
 *
 * <p>This class is thread-safe, conditional upon the safety of the underlying service itself.
 */
public class LookupMyAccountInfoClient {

  /**
   * The path of this service, relative to the root URI provided in {@link
   * SymwsClientConfig#getRootUri()}.
   */
  public static final String SERVICE_PATH = "/rest/patron/lookupMyAccountInfo";

  private final SymwsClientConfig symwsClientConfig;
  private final Map<String, List<Object>> optionalParams;

  /**
   * Constructs a new instance.
   *
   * @param symwsClientConfig the client configuration to use for all requests made using this
   *     client
   * @param optionalParams the optional parameters to send with all requests made using this client
   */
  public LookupMyAccountInfoClient(
      SymwsClientConfig symwsClientConfig, OptionalParams optionalParams) {
    this.symwsClientConfig = symwsClientConfig;
    this.optionalParams = (optionalParams == null ? new OptionalParams() : optionalParams).asMap();
  }

  /**
   * Lookup the account information for the currently signed in SYMWS user.
   *
   * @return the account information
   * @throws SymwsClientException if the request is not successful
   */
  public LookupPatronInfoResponse lookup() throws SymwsClientException {
    Response response = SymwsUtil.request(symwsClientConfig, SERVICE_PATH, optionalParams);
    return SymwsUtil.parseResponse(response, LookupMyAccountInfoResponse.class);
  }
}
