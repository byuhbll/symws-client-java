package edu.byu.hbll.symws.client.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/** JAXB bean representing callnumber-level bibliographic data. */
public class CallInfo {

  @XmlElement(name = "libraryID")
  private String libraryId;

  @XmlElement(name = "classificationID")
  private String classificationId;

  @XmlElement private String callNumber;
  @XmlElement private Integer numberOfCopies;
  @XmlElement private String boundParentAuthor; // Optional
  @XmlElement private String boundParentTitle; // Optional

  @XmlElement(name = "ItemInfo")
  private List<ItemInfo> itemInfo = new ArrayList<>();

  /**
   * Returns the libraryId.
   *
   * @return the libraryId
   */
  public String getLibraryId() {
    return libraryId;
  }

  /**
   * Returns the classificationId.
   *
   * @return the classificationId
   */
  public String getClassificationId() {
    return classificationId;
  }

  /**
   * Returns the callNumber.
   *
   * @return the callNumber
   */
  public String getCallNumber() {
    return callNumber;
  }

  /**
   * Returns the numberOfCopies.
   *
   * @return the numberOfCopies
   */
  public Integer getNumberOfCopies() {
    return numberOfCopies;
  }

  /**
   * Returns the boundParentAuthor.
   *
   * @return the boundParentAuthor
   */
  public String getBoundParentAuthor() {
    return boundParentAuthor;
  }

  /**
   * Returns the boundParentTitle.
   *
   * @return the boundParentTitle
   */
  public String getBoundParentTitle() {
    return boundParentTitle;
  }

  /**
   * Returns the itemInfo.
   *
   * @return the itemInfo
   */
  public List<ItemInfo> getItemInfo() {
    return itemInfo;
  }
}
