package edu.byu.hbll.symws.client.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/** JAXB bean representing a Symphony user or patron. */
@XmlRootElement(name = "LookupPatronInfoResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class LookupPatronInfoResponse {

  @XmlElement private PatronInfo patronInfo; // Optional
  @XmlElement private PatronCirculationInfo patronCirculationInfo; // Optional
  @XmlElement private PatronAddressInfo patronAddressInfo; // Optional
  @XmlElement private List<PatronCheckoutInfo> patronCheckoutInfo = new ArrayList<>();
  @XmlElement private List<PatronCheckoutHistoryInfo> patronCheckoutHistoryInfo = new ArrayList<>();
  @XmlElement private List<PatronHoldInfo> patronHoldInfo = new ArrayList<>();
  @XmlElement private List<FeeInfo> feeInfo = new ArrayList<>();
  @XmlElement private PatronStatusInfo patronStatusInfo; // Optional
  @XmlElement private GroupInfo groupInfo; // Optional
  @XmlElement private UserSuspensionInfo userSuspensionInfo; // Optional
  @XmlElement private List<SMSInfo> smsInfo = new ArrayList<>();
  @XmlElement private String photoType; // Optional
  @XmlElement private String photo; // Optional

  /**
   * Returns the patronInfo.
   *
   * @return the patronInfo
   */
  public PatronInfo getPatronInfo() {
    return patronInfo;
  }

  /**
   * Returns the patronCirculationInfo.
   *
   * @return the patronCirculationInfo
   */
  public PatronCirculationInfo getPatronCirculationInfo() {
    return patronCirculationInfo;
  }

  /**
   * Returns the patronAddressInfo.
   *
   * @return the patronAddressInfo
   */
  public PatronAddressInfo getPatronAddressInfo() {
    return patronAddressInfo;
  }

  /**
   * Returns the patronCheckoutInfo.
   *
   * @return the patronCheckoutInfo
   */
  public List<PatronCheckoutInfo> getPatronCheckoutInfo() {
    return patronCheckoutInfo;
  }

  /**
   * Returns the patronCheckoutHistoryInfo.
   *
   * @return the patronCheckoutHistoryInfo
   */
  public List<PatronCheckoutHistoryInfo> getPatronCheckoutHistoryInfo() {
    return patronCheckoutHistoryInfo;
  }

  /**
   * Returns the patronHoldInfo.
   *
   * @return the patronHoldInfo
   */
  public List<PatronHoldInfo> getPatronHoldInfo() {
    return patronHoldInfo;
  }

  /**
   * Returns the feeInfo.
   *
   * @return the feeInfo
   */
  public List<FeeInfo> getFeeInfo() {
    return feeInfo;
  }

  /**
   * Returns the patronStatusInfo.
   *
   * @return the patronStatusInfo
   */
  public PatronStatusInfo getPatronStatusInfo() {
    return patronStatusInfo;
  }

  /**
   * Returns the groupInfo.
   *
   * @return the groupInfo
   */
  public GroupInfo getGroupInfo() {
    return groupInfo;
  }

  /**
   * Returns the userSuspensionInfo.
   *
   * @return the userSuspensionInfo
   */
  public UserSuspensionInfo getUserSuspensionInfo() {
    return userSuspensionInfo;
  }

  /**
   * Returns the smsInfo.
   *
   * @return the smsInfo
   */
  public List<SMSInfo> getSmsInfo() {
    return smsInfo;
  }

  /**
   * Returns the photoType.
   *
   * @return the photoType
   */
  public String getPhotoType() {
    return photoType;
  }

  /**
   * Returns the photo.
   *
   * @return the photo
   */
  public String getPhoto() {
    return photo;
  }
}
