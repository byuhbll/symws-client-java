package edu.byu.hbll.symws.client.model;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * Enumerated list of possible reasons a hold might be inactive.
 *
 * <p>From the SYMWS documentation: "The HoldInactiveType type contains enumeration values that you
 * can use in the lookupMyAccountInfo operation request to limit your results when you retrieve
 * inactive hold information."
 */
public enum HoldInactiveType {
  HOLD_INACTIVE_REASON_NONE,
  HOLD_INACTIVE_REASON_FILLED,
  HOLD_INACTIVE_REASON_CANCELLED,
  HOLD_INACTIVE_REASON_EXPIRED,
  HOLD_INACTIVE_REASON_CANCELLED_ON_RESERVE,
  HOLD_INACTIVE_REASON_BLANKET_NONEED,
  HOLD_INACTIVE_REASON_CANCELLED_ON_ORDER,
  HOLD_INACTIVE_REASON_MODIFIED_ORDER,
  @XmlEnumValue("HOLD_INACTIVE_REASON_EXP_ONSHLF")
  HOLD_INACTIVE_REASON_EXP_ONSHELF;
}
