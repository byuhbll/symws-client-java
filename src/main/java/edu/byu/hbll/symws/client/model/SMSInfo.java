package edu.byu.hbll.symws.client.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * JAXB bean representing SMS preferences and configuration.
 *
 * <p>From the SYMWS documentation: "The SMSInfo type displays information specific to the patron’s
 * SMS preferences and phone numbers."
 */
public class SMSInfo {

  @XmlElement private Long smsKey;
  @XmlElement private String label;
  @XmlElement private String phoneNumber;
  @XmlElement private String countryName;
  @XmlElement private String countryNameCode;
  @XmlElement private String countryCode;
  @XmlElement private Boolean optInCheckoutNotices;
  @XmlElement private Boolean optInHoldPickupNotices;
  @XmlElement private Boolean optInBillNotices;
  @XmlElement private Boolean optInManualMessages;
  @XmlElement private Boolean optInUserAnnouncementMessages;

  /**
   * Returns the smsKey.
   *
   * @return the smsKey
   */
  public Long getSmsKey() {
    return smsKey;
  }

  /**
   * Returns the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * Returns the phoneNumber.
   *
   * @return the phoneNumber
   */
  public String getPhoneNumber() {
    return phoneNumber;
  }

  /**
   * Returns the countryName.
   *
   * @return the countryName
   */
  public String getCountryName() {
    return countryName;
  }

  /**
   * Returns the countryNameCode.
   *
   * @return the countryNameCode
   */
  public String getCountryNameCode() {
    return countryNameCode;
  }

  /**
   * Returns the countryCode.
   *
   * @return the countryCode
   */
  public String getCountryCode() {
    return countryCode;
  }

  /**
   * Returns the optInCheckoutNotices.
   *
   * @return the optInCheckoutNotices
   */
  public Boolean getOptInCheckoutNotices() {
    return optInCheckoutNotices;
  }

  /**
   * Returns the optInHoldPickupNotices.
   *
   * @return the optInHoldPickupNotices
   */
  public Boolean getOptInHoldPickupNotices() {
    return optInHoldPickupNotices;
  }

  /**
   * Returns the optInBillNotices.
   *
   * @return the optInBillNotices
   */
  public Boolean getOptInBillNotices() {
    return optInBillNotices;
  }

  /**
   * Returns the optInManualMessages.
   *
   * @return the optInManualMessages
   */
  public Boolean getOptInManualMessages() {
    return optInManualMessages;
  }

  /**
   * Returns the optInUserAnnouncementMessages.
   *
   * @return the optInUserAnnouncementMessages
   */
  public Boolean getOptInUserAnnouncementMessages() {
    return optInUserAnnouncementMessages;
  }
}
