package edu.byu.hbll.symws.client.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/** JAXB bean representing a catalog record. */
public class TitleInfo {

  @XmlElement(name = "titleID")
  private Long titleId;

  @XmlElement private String titleControlNumber; // Optional

  @XmlElement(name = "catalogFormatID")
  private String catalogFormatId; // Optional

  @XmlElement private String catalogFormatType; // Optional
  @XmlElement private String materialType; // Optional
  @XmlElement private String baseCallNumber; // Optional
  @XmlElement private List<String> author = new ArrayList<>();
  @XmlElement private String title; // Optional

  @XmlElement(name = "ISBN")
  private List<String> isbn = new ArrayList<>();

  @XmlElement(name = "SICI")
  private List<String> sici = new ArrayList<>();

  @XmlElement(name = "UPC")
  private List<String> upc = new ArrayList<>();

  @XmlElement(name = "OCLCControlNumber")
  private String oclcControlNumber; // Optional

  @XmlElement(name = "sisacID")
  private String sisacId; // Optional

  @XmlElement private String publisherName; // Optional

  @XmlElement
  private String
      datePublished; // Optional, left as a string because it is free-form, not an ISO date.

  @XmlElement private Integer yearOfPublication; // Optional
  @XmlElement private String extent; // Optional

  @XmlElement(name = "netLibraryID")
  private String netLibraryId; // Optional

  @XmlElement private Integer numberOfCallNumbers; // Optional
  @XmlElement private Integer numberOfTitleHolds; // Optional
  @XmlElement private Integer copiesOnOrder; // Optional
  @XmlElement private Integer outstandingCopiesOnOrder; // Optional
  @XmlElement private Integer numberOfBoundwithLinks; // Optional
  @XmlElement private List<CallSummary> callSummary = new ArrayList<>();

  @XmlElement(name = "TitleAvailabilityInfo")
  private TitleAvailabilityInfo titleAvailabilityInfo; // Optional

  @XmlElement(name = "TitleOrderInfo")
  private List<TitleOrderInfo> titleOrderInfo = new ArrayList<>();

  @XmlElement(name = "CallInfo")
  private List<CallInfo> callInfo = new ArrayList<>();

  @XmlElement(name = "BibliographicInfo")
  private BibliographicInfo bibliographicInfo; // Optional

  @XmlElement(name = "MarcHoldingInfo")
  private List<MarcHoldingsInfo> marcHoldingInfo = new ArrayList<>();

  @XmlElement(name = "BoundwithLinkInfo")
  private List<BoundwithLinkInfo> boundwithLinkInfo = new ArrayList<>();

  /**
   * Returns the titleId.
   *
   * @return the titleId
   */
  public Long getTitleId() {
    return titleId;
  }

  /**
   * Returns the titleControlNumber.
   *
   * @return the titleControlNumber
   */
  public String getTitleControlNumber() {
    return titleControlNumber;
  }

  /**
   * Returns the catalogFormatId.
   *
   * @return the catalogFormatId
   */
  public String getCatalogFormatId() {
    return catalogFormatId;
  }

  /**
   * Returns the catalogFormatType.
   *
   * @return the catalogFormatType
   */
  public String getCatalogFormatType() {
    return catalogFormatType;
  }

  /**
   * Returns the materialType.
   *
   * @return the materialType
   */
  public String getMaterialType() {
    return materialType;
  }

  /**
   * Returns the baseCallNumber.
   *
   * @return the baseCallNumber
   */
  public String getBaseCallNumber() {
    return baseCallNumber;
  }

  /**
   * Returns the author.
   *
   * @return the author
   */
  public List<String> getAuthor() {
    return author;
  }

  /**
   * Returns the title.
   *
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * Returns the isbn.
   *
   * @return the isbn
   */
  public List<String> getIsbn() {
    return isbn;
  }

  /**
   * Returns the sici.
   *
   * @return the sici
   */
  public List<String> getSici() {
    return sici;
  }

  /**
   * Returns the upc.
   *
   * @return the upc
   */
  public List<String> getUpc() {
    return upc;
  }

  /**
   * Returns the oclcControlNumber.
   *
   * @return the oclcControlNumber
   */
  public String getOclcControlNumber() {
    return oclcControlNumber;
  }

  /**
   * Returns the sisacId.
   *
   * @return the sisacId
   */
  public String getSisacId() {
    return sisacId;
  }

  /**
   * Returns the publisherName.
   *
   * @return the publisherName
   */
  public String getPublisherName() {
    return publisherName;
  }

  /**
   * Returns the datePublished.
   *
   * @return the datePublished
   */
  public String getDatePublished() {
    return datePublished;
  }

  /**
   * Returns the yearOfPublication.
   *
   * @return the yearOfPublication
   */
  public Integer getYearOfPublication() {
    return yearOfPublication;
  }

  /**
   * Returns the extent.
   *
   * @return the extent
   */
  public String getExtent() {
    return extent;
  }

  /**
   * Returns the netLibraryId.
   *
   * @return the netLibraryId
   */
  public String getNetLibraryId() {
    return netLibraryId;
  }

  /**
   * Returns the numberOfCallNumbers.
   *
   * @return the numberOfCallNumbers
   */
  public Integer getNumberOfCallNumbers() {
    return numberOfCallNumbers;
  }

  /**
   * Returns the numberOfTitleHolds.
   *
   * @return the numberOfTitleHolds
   */
  public Integer getNumberOfTitleHolds() {
    return numberOfTitleHolds;
  }

  /**
   * Returns the copiesOnOrder.
   *
   * @return the copiesOnOrder
   */
  public Integer getCopiesOnOrder() {
    return copiesOnOrder;
  }

  /**
   * Returns the outstandingCopiesOnOrder.
   *
   * @return the outstandingCopiesOnOrder
   */
  public Integer getOutstandingCopiesOnOrder() {
    return outstandingCopiesOnOrder;
  }

  /**
   * Returns the numberOfBoundwithLinks.
   *
   * @return the numberOfBoundwithLinks
   */
  public Integer getNumberOfBoundwithLinks() {
    return numberOfBoundwithLinks;
  }

  /**
   * Returns the callSummary.
   *
   * @return the callSummary
   */
  public List<CallSummary> getCallSummary() {
    return callSummary;
  }

  /**
   * Returns the titleAvailabilityInfo.
   *
   * @return the titleAvailabilityInfo
   */
  public TitleAvailabilityInfo getTitleAvailabilityInfo() {
    return titleAvailabilityInfo;
  }

  /**
   * Returns the titleOrderInfo.
   *
   * @return the titleOrderInfo
   */
  public List<TitleOrderInfo> getTitleOrderInfo() {
    return titleOrderInfo;
  }

  /**
   * Returns the callInfo.
   *
   * @return the callInfo
   */
  public List<CallInfo> getCallInfo() {
    return callInfo;
  }

  /**
   * Returns the bibliographicInfo.
   *
   * @return the bibliographicInfo
   */
  public BibliographicInfo getBibliographicInfo() {
    return bibliographicInfo;
  }

  /**
   * Returns the marcHoldingInfo.
   *
   * @return the marcHoldingInfo
   */
  public List<MarcHoldingsInfo> getMarcHoldingInfo() {
    return marcHoldingInfo;
  }

  /**
   * Returns the boundwithLinkInfo.
   *
   * @return the boundwithLinkInfo
   */
  public List<BoundwithLinkInfo> getBoundwithLinkInfo() {
    return boundwithLinkInfo;
  }
}
