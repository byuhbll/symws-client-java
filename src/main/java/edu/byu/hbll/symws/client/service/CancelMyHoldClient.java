package edu.byu.hbll.symws.client.service;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.SymwsClientException;
import edu.byu.hbll.symws.client.SymwsUtil;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * Client for canceling holds belonging to the current user session in SYMWS.
 *
 * <p>This class is thread-safe, conditional upon the safety of the underlying service itself.
 */
public class CancelMyHoldClient {

  /**
   * The path of this service, relative to the root URI provided in {@link
   * SymwsClientConfig#getRootUri()}.
   */
  public static final String SERVICE_PATH = "/rest/patron/cancelMyHold";

  /** The name/key of the "holdKey" query parameter. */
  public static final String QUERYPARAM_HOLD_KEY = "holdKey";

  private final SymwsClientConfig symwsClientConfig;

  /**
   * Constructs a new instance.
   *
   * @param symwsClientConfig the client configuration to use for all requests made using this
   *     client; this client configuration MUST have an active session.
   */
  public CancelMyHoldClient(SymwsClientConfig symwsClientConfig) {
    this.symwsClientConfig = symwsClientConfig;
  }

  /**
   * Cancels the Symphony hold having the provided holdKey.
   *
   * @param holdKey the holdKey of the hold to cancel
   * @return true if the hold was successfully cancelled, false otherwise
   * @throws SymwsClientException if the web service returns an error
   * @throws IllegalArgumentException if the holdKey is a negative number
   */
  public boolean cancelHold(long holdKey) throws SymwsClientException, IllegalArgumentException {
    SymwsUtil.requireGreaterThanOrEqualTo(holdKey, 0L, "holdKey cannot be negative");

    MultivaluedMap<String, Object> queryParams = new MultivaluedHashMap<>(1);
    queryParams.add(QUERYPARAM_HOLD_KEY, holdKey);
    Response response = SymwsUtil.request(symwsClientConfig, SERVICE_PATH, queryParams);

    return response.readEntity(Boolean.class);
  }
}
