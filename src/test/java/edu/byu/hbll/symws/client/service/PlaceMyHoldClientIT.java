package edu.byu.hbll.symws.client.service;

import static org.junit.Assert.assertNotNull;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.TestConfig;
import edu.byu.hbll.symws.client.model.PlaceMyHoldResponse;
import edu.byu.hbll.symws.client.service.PlaceMyHoldClient.OptionalParams;
import org.junit.Test;

/** Integration tests for {@link PlaceMyHoldClient}. */
public class PlaceMyHoldClientIT {

  private TestConfig testConfig = TestConfig.getInstance();

  /**
   * Verifies that calling {@link PlaceMyHoldClient#placeTitleHold(long, OptionalParams)} with a
   * valid session will create a new hold on behalf of that user.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void placeTitleHoldShouldCreateNewHold() throws Exception {
    String userId = testConfig.getValidParams().getPatron().getUserId();
    String password = testConfig.getValidParams().getPatron().getPassword();
    SymwsClientConfig clientConfig =
        testConfig.getValidParams().getClientConfig().withAuthentication(userId, password);

    long titleId = Long.parseLong(testConfig.getValidParams().getTitle().getTitleId());
    PlaceMyHoldClient client = new PlaceMyHoldClient(clientConfig, null);
    PlaceMyHoldResponse response = client.placeTitleHold(titleId);
    Long holdKey = response.getHoldKey();
    try {
      assertNotNull(holdKey);
      assertNotNull(response.getDateHoldExpires());
      assertNotNull(response.getItemAvailable());
      assertNotNull(response.getTitle());
    } finally {
      if (holdKey != null) {
        cleanupHold(clientConfig, holdKey);
      }
    }
  }

  /**
   * Verifies that calling {@link PlaceMyHoldClient#placeItemHold(String, OptionalParams)} with a
   * valid session will create a new hold on behalf of that user.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void placeItemHoldShouldCreateNewHold() throws Exception {
    String userId = testConfig.getValidParams().getPatron().getUserId();
    String password = testConfig.getValidParams().getPatron().getPassword();
    SymwsClientConfig clientConfig =
        testConfig.getValidParams().getClientConfig().withAuthentication(userId, password);

    String itemId = testConfig.getValidParams().getItem().getItemId();
    PlaceMyHoldClient client = new PlaceMyHoldClient(clientConfig, null);
    PlaceMyHoldResponse response = client.placeItemHold(itemId);
    final Long holdKey = response.getHoldKey();
    try {
      assertNotNull(holdKey);
      assertNotNull(response.getDateHoldExpires());
      assertNotNull(response.getItemAvailable());
      assertNotNull(response.getTitle());
    } finally {
      if (holdKey != null) {
        cleanupHold(clientConfig, holdKey);
      }
    }
  }

  /**
   * Uses {@link CancelMyHoldClient} to remove the hold after the test is complete.
   *
   * @param clientConfig the {@link SymwsClientConfig} used to create the hold
   * @param holdKey the hold key provided by SYMWS when the hold was created
   * @throws Exception if an exception is thrown while cleaning up the hold
   */
  private void cleanupHold(SymwsClientConfig clientConfig, long holdKey) throws Exception {
    CancelMyHoldClient client = new CancelMyHoldClient(clientConfig);
    client.cancelHold(holdKey);
  }
}
