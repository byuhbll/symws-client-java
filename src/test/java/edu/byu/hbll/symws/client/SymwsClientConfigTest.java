package edu.byu.hbll.symws.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.net.URI;
import java.util.Optional;
import org.junit.Test;

/** Unit tests for {@link SymwsClientConfig}. */
public class SymwsClientConfigTest {

  private static final URI ROOT_URI = URI.create("https://www.example.com:443/test/path");
  private static final String CLIENT_ID = "testclient";
  private static final String USER_ID = "testuser";
  private static final String PASSWORD = "testpassword";

  /**
   * Verifies that providing a {@code null} value for the {@code clientId} when constructing a new
   * {@link SymwsClientConfig.Builder} will throw a {@link NullPointerException}.
   */
  @Test(expected = NullPointerException.class)
  public void builderShouldRejectNullClientId() {
    new SymwsClientConfig.Builder(null);
  }

  /**
   * Verifies that providing an empty value for the {@code clientId} when constructing a new {@link
   * SymwsClientConfig.Builder} will throw an {@link IllegalArgumentException}.
   */
  @Test(expected = IllegalArgumentException.class)
  public void builderShouldRejectEmptyClientId() {
    new SymwsClientConfig.Builder("");
  }

  /**
   * Verifies that providing only whitespace for the {@code clientId} when constructing a new {@link
   * SymwsClientConfig.Builder} will throw an {@link IllegalArgumentException}.
   */
  @Test(expected = IllegalArgumentException.class)
  public void builderShouldRejectBlankClientId() {
    new SymwsClientConfig.Builder(" ");
  }

  /**
   * Verifies that providing a {@code null} value to {@link SymwsClientConfig.Builder#rootUri(URI)}
   * will throw a {@link NullPointerException}.
   */
  @Test(expected = NullPointerException.class)
  public void builderShouldRejectNullRootUri() {
    new SymwsClientConfig.Builder(CLIENT_ID).rootUri(null);
  }

  /**
   * Verifies that an {@link SymwsClientConfig} instance will correctly construct when only the
   * required builder params are provided.
   */
  @Test
  public void builderShouldBuildWithMinimalData() {
    SymwsClientConfig symwsClientConfig = new SymwsClientConfig.Builder(CLIENT_ID).build();

    assertEquals(SymwsClientConfig.Builder.DEFAULT_ROOT_URI, symwsClientConfig.getRootUri());
    assertEquals(CLIENT_ID, symwsClientConfig.getClientId());
    assertEquals(Optional.empty(), symwsClientConfig.getSessionToken());
  }

  /**
   * Verifies that an {@link SymwsClientConfig} instance will correctly construct when all optional
   * builder params are provided.
   */
  @Test
  public void builderShouldBuildWithMaximumData() {
    SymwsClientConfig symwsClientConfig =
        new SymwsClientConfig.Builder(CLIENT_ID)
            .rootUri(ROOT_URI)
            .authentication(USER_ID, PASSWORD)
            .build();

    assertEquals(ROOT_URI, symwsClientConfig.getRootUri());
    assertEquals(CLIENT_ID, symwsClientConfig.getClientId());
    assertEquals(Optional.empty(), symwsClientConfig.getSessionToken());
  }

  /**
   * Verifies that calling {@link SymwsClientConfig#newSession()} will return a copy of the original
   * {@link SymwsClientConfig} with a session token.
   *
   * <p>All values should be copied from the original instance, except for the {@code sessionToken},
   * which should be replaced with the value provided by the {@code loginUser} service.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void withSessionShouldCreateCopyHavingSession() throws Exception {
    SymwsClientConfig orig = new SymwsClientConfig.Builder(CLIENT_ID).rootUri(ROOT_URI).build();

    SymwsClientConfig copy = orig.withAuthentication(USER_ID, PASSWORD);

    assertFalse(orig == copy);
    assertEquals(orig.getRootUri(), copy.getRootUri());
    assertEquals(orig.getClientId(), copy.getClientId());
    assertEquals(USER_ID, copy.getUserId());
    assertEquals(PASSWORD, copy.getPassword());
  }

  // TODO: Test for JSON Deserialization

}
