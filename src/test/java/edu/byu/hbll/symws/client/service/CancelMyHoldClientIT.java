package edu.byu.hbll.symws.client.service;

import static org.junit.Assert.assertTrue;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.SymwsClientException;
import edu.byu.hbll.symws.client.TestConfig;
import org.junit.Test;

/** Integration tests for {@link CancelMyHoldClient}. */
public class CancelMyHoldClientIT {

  private TestConfig testConfig = TestConfig.getInstance();

  /**
   * Verifies that calling {@link CanceleMyHoldClient#cancelHold(long)} with a valid session and
   * existing hold key will cancel the given hold.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void cancelHoldShouldCancelHold() throws Exception {
    String userId = testConfig.getValidParams().getPatron().getUserId();
    String password = testConfig.getValidParams().getPatron().getPassword();
    SymwsClientConfig clientConfig =
        testConfig.getValidParams().getClientConfig().withAuthentication(userId, password);

    // Setup the hold.
    long titleId = Long.parseLong(testConfig.getValidParams().getTitle().getTitleId());
    long holdKey = setupHold(clientConfig, titleId);

    // Now cancel the hold.  If this step fails, the hold will need to be cleaned up manually!
    CancelMyHoldClient client = new CancelMyHoldClient(clientConfig);
    boolean response = client.cancelHold(holdKey);
    assertTrue(response);
  }

  /**
   * Uses {@link CreateMyHoldClient} to place a new hold using the provided (sessioned) clientConfig
   * and titleId.
   *
   * @param clientConfig the {@link SymwsClientConfig} which will be used to cancel the hold
   * @param titleId the titleId or catalog key of the record for which to place the hold
   * @return the hold key of the newly created hold
   * @throws SymwsClientException if an exception is thrown while setting up the hold
   */
  private long setupHold(SymwsClientConfig clientConfig, long titleId) throws SymwsClientException {
    CreateMyHoldClient client = new CreateMyHoldClient(clientConfig, null);
    return client.createTitleHold(titleId);
  }
}
