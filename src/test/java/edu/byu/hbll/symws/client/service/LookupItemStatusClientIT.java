package edu.byu.hbll.symws.client.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.SymwsClientException;
import edu.byu.hbll.symws.client.TestConfig;
import edu.byu.hbll.symws.client.model.ItemStatusInfo;
import edu.byu.hbll.symws.client.model.SymwsFault;
import org.junit.Test;

/** Integration tests for {@link LookupItemStatusClient}. */
public class LookupItemStatusClientIT {

  private TestConfig testConfig = TestConfig.getInstance();

  /**
   * Verifies that calling {@link LookupItemStatusClient#lookup(String)} will return the item
   * status.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupShouldReturnItemStatus() throws Exception {
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();
    String itemId = testConfig.getValidParams().getItem().getItemId();

    LookupItemStatusClient lookupItemStatusClient = new LookupItemStatusClient(clientConfig);
    ItemStatusInfo lookupItemStatusResponse = lookupItemStatusClient.lookup(itemId);
    assertEquals(itemId, lookupItemStatusResponse.getItemId());
  }

  /**
   * Verifies that calling {@link LookupItemStatusClient#lookup(String)} with an unprivileged user
   * will fail to return the item status.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupShouldFailIfUserIsUnprivileged() throws Exception {
    String userId = testConfig.getValidParams().getPatron().getUserId();
    String password = testConfig.getValidParams().getPatron().getPassword();
    SymwsClientConfig clientConfig =
        testConfig.getValidParams().getClientConfig().withAuthentication(userId, password);
    String itemId = testConfig.getValidParams().getItem().getItemId();

    LookupItemStatusClient lookupItemStatusClient = new LookupItemStatusClient(clientConfig);
    try {
      lookupItemStatusClient.lookup(itemId);
      fail("SymwsClientException should have been thrown.");
    } catch (SymwsClientException e) {
      assertTrue("This exception should have a fault", e.hasFault());
      assertEquals(
          "This exception should have the expected fault code",
          SymwsFault.ACCESS_DENIED,
          e.getFault().getCode());
    }
  }

  /**
   * Verifies that calling {@link LookupItemStatusClient#lookup(String)} with an invalid item ID or
   * barcode will fail to return the item status.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupShouldFailIfItemDoesNotExist() throws Exception {
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();
    String bogusItemId = testConfig.getBogusParams().getItem().getItemId();

    LookupItemStatusClient lookupItemStatusClient = new LookupItemStatusClient(clientConfig);
    try {
      lookupItemStatusClient.lookup(bogusItemId);
      fail("SymwsClientException should have been thrown.");
    } catch (SymwsClientException e) {
      assertTrue("This exception should have a fault", e.hasFault());
      assertEquals(
          "This exception should have the expected fault code",
          SymwsFault.ITEM_NOT_FOUND_IN_CATALOG,
          e.getFault().getCode());
    }
  }
}
