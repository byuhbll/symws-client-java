package edu.byu.hbll.symws.client.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.SymwsClientException;
import edu.byu.hbll.symws.client.TestConfig;
import edu.byu.hbll.symws.client.model.LoginUserResponse;
import org.junit.Test;

/** Integration tests for {@link LoginUserClient}. */
public class LoginUserClientIT {

  private TestConfig testConfig = TestConfig.getInstance();

  /**
   * Verifies that calling {@link LoginUserClient#login(String, String)} with valid credentials will
   * successfully create a native SYMWS session.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void loginShouldCreateSessionWithValidCredentials() throws Exception {
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();
    String userId = testConfig.getValidParams().getPatron().getUserId();
    String password = testConfig.getValidParams().getPatron().getPassword();

    LoginUserClient loginUserClient = new LoginUserClient(clientConfig);
    LoginUserResponse session = loginUserClient.login(userId, password);

    assertNotNull("A sessionToken should have been generated", session.getSessionToken());
  }

  /**
   * Verifies that calling {@link LoginUserClient#login(String, String)} with an invalid userId or
   * altId will cause a {@link SymwsClientException} to be thrown with a corresponding fault.
   */
  @Test
  public void loginShouldFailWithInvalidUser() {
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();
    String bogusUserId = testConfig.getBogusParams().getPatron().getUserId();
    String bogusPassword = testConfig.getBogusParams().getPatron().getPassword();

    LoginUserClient loginUserClient = new LoginUserClient(clientConfig);
    try {
      loginUserClient.login(bogusUserId, bogusPassword);
      fail("SymwsClientException should have been thrown");
    } catch (SymwsClientException e) {
      assertTrue(e.hasFault());
    }
  }

  /**
   * Verifies that calling {@link LoginUserClient#login(String, String)} with an invalid password
   * will cause a {@link SymwsClientException} to be thrown with a corresponding fault.
   */
  @Test
  public void loginShouldFailWithInvalidPassword() {
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();
    String userId = testConfig.getValidParams().getPatron().getUserId();
    String bogusPassword = testConfig.getBogusParams().getPatron().getPassword();

    LoginUserClient loginUserClient = new LoginUserClient(clientConfig);
    try {
      loginUserClient.login(userId, bogusPassword);
      fail("SymwsClientException should have been thrown");
    } catch (SymwsClientException e) {
      assertTrue(e.hasFault());
    }
  }
}
