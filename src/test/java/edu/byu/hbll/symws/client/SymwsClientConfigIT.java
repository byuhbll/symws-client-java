package edu.byu.hbll.symws.client;

import edu.byu.hbll.symws.client.service.LoginUserClient;
import org.junit.Test;

/** Integration tests for {@link SymwsClientConfig}. */
public class SymwsClientConfigIT {

  private TestConfig testConfig = TestConfig.getInstance();

  /**
   * Verifies that calling {@link LoginUserClient#login(String, String)} with a {@link
   * SymwsClientConfig} which does not point to a valid SYMWS installation will cause a {@link
   * SymwsClientException} to be thrown.
   *
   * <p>TODO: This should be moved to SymwsClientConfigIT.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test(expected = SymwsClientException.class)
  public void newSessionShouldFailWithInvalidRootUri() throws Exception {
    SymwsClientConfig bogusConfig = testConfig.getBogusParams().getClientConfig();
    String userId = testConfig.getValidParams().getPatron().getUserId();
    String password = testConfig.getValidParams().getPatron().getPassword();

    bogusConfig.withAuthentication(userId, password).newSession();
  }

  /**
   * Verifies that calling {@link LoginUserClient#login(String, String)} with a {@link
   * SymwsClientConfig} which does not point to a valid SYMWS installation will cause a {@link
   * SymwsClientException} to be thrown.
   *
   * <p>TODO: This should be moved to SymwsClientConfigIT.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test(expected = SymwsClientException.class)
  public void newSessionShouldFailWithInvalidClientId() throws Exception {
    SymwsClientConfig bogusConfig = testConfig.getBogusParams().getClientConfig();
    String userId = testConfig.getValidParams().getPatron().getUserId();
    String password = testConfig.getValidParams().getPatron().getPassword();

    bogusConfig.withAuthentication(userId, password).newSession();
  }
}
