package edu.byu.hbll.symws.client.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.SymwsClientException;
import edu.byu.hbll.symws.client.TestConfig;
import edu.byu.hbll.symws.client.model.CheckoutInfoFilter;
import edu.byu.hbll.symws.client.model.FeeInfoFilter;
import edu.byu.hbll.symws.client.model.HoldInfoFilter;
import edu.byu.hbll.symws.client.model.LookupPatronInfoResponse;
import edu.byu.hbll.symws.client.model.SymwsFault;
import edu.byu.hbll.symws.client.service.LookupPatronInfoClient.OptionalParams;
import java.util.Collections;
import org.junit.Test;

/** Integration tests for {@link LookupPatronInfoClient}. */
public class LookupPatronInfoClientIT {

  private TestConfig testConfig = TestConfig.getInstance();

  /**
   * Verifies that calling {@link LookupPatronInfoClient#lookupWithUserId(String, OptionalParams)}
   * returns a successful but effectively empty response when no optional parameters are included.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupWithUserIdShouldRetrieveEmptyResponseWithNoParams() throws Exception {
    String userId = testConfig.getValidParams().getPatron().getUserId();
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();

    LookupPatronInfoClient client = new LookupPatronInfoClient(clientConfig, null);
    LookupPatronInfoResponse response = client.lookupWithUserId(userId);

    assertEquals(Collections.emptyList(), response.getFeeInfo());
    assertNull(response.getGroupInfo());
    assertNull(response.getPatronAddressInfo());
    assertEquals(Collections.emptyList(), response.getPatronCheckoutHistoryInfo());
    assertEquals(Collections.emptyList(), response.getPatronCheckoutInfo());
    assertNull(response.getPatronCirculationInfo());
    assertEquals(Collections.emptyList(), response.getPatronHoldInfo());
    assertNull(response.getPatronStatusInfo());
    assertNull(response.getPhoto());
    assertNull(response.getPhotoType());
    assertEquals(Collections.emptyList(), response.getSmsInfo());
    assertNull(response.getUserSuspensionInfo());
  }

  /**
   * Verifies that calling {@link LookupPatronInfoClient#lookupWithUserId(String, OptionalParams)}
   * returns a populated response when all optional parameters are included.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupWithUserIdShouldRetrievePopulatedResponseWithAllParams() throws Exception {
    String userId = testConfig.getValidParams().getPatron().getUserId();
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();

    LookupPatronInfoClient client =
        new LookupPatronInfoClient(
            clientConfig,
            new OptionalParams()
                .includeFeeInfo(FeeInfoFilter.ALL_FEES_AND_PAYMENTS)
                .includePatronAddressInfo()
                .includePatronCheckoutHistoryInfo()
                .includePatronCheckoutInfo(CheckoutInfoFilter.ALL)
                .includePatronCirculationInfo()
                .includePatronHoldInfo(HoldInfoFilter.ALL)
                .includePatronInfo()
                .includePatronStatusInfo()
                .includePhoto()
                .includeSmsInfo()
                .includeUserGroupInfo()
                .includeUserSuspensionInfo());
    LookupPatronInfoResponse response = client.lookupWithUserId(userId);

    assertNotNull(response.getPatronAddressInfo());
    assertNotNull(response.getPatronCirculationInfo());
    assertNotNull(response.getPatronInfo());
    assertNotNull(response.getPatronStatusInfo());
    // The rest of these can be null depending on the patron.  I'm not sure how to test for them.
    // assertNotNull(response.getFeeInfo());
    // assertNotNull(response.getGroupInfo());
    // assertNotNull(response.getPatronCheckoutHistoryInfo());
    // assertNotNull(response.getPatronCheckoutInfo());
    // assertNotNull(response.getPatronHoldInfo());
    // assertNotNull(response.getPhoto());
    // assertNotNull(response.getPhotoType());
    // assertNotNull(response.getSmsInfo());
    // assertNotNull(response.getUserSuspensionInfo());
  }

  /**
   * Verifies that calling {@link LookupPatronInfoClient#lookupWithAlternateId(String,
   * OptionalParams)} returns a successful but effectively empty response when no optional
   * parameters are included.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupWithAlternateIdShouldRetrieveEmptyResponseWithNoParams() throws Exception {
    String alternateId = testConfig.getValidParams().getPatron().getAlternateId();
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();

    LookupPatronInfoClient client = new LookupPatronInfoClient(clientConfig, null);
    LookupPatronInfoResponse response = client.lookupWithAlternateId(alternateId);

    assertEquals(Collections.emptyList(), response.getFeeInfo());
    assertNull(response.getGroupInfo());
    assertNull(response.getPatronAddressInfo());
    assertEquals(Collections.emptyList(), response.getPatronCheckoutHistoryInfo());
    assertEquals(Collections.emptyList(), response.getPatronCheckoutInfo());
    assertNull(response.getPatronCirculationInfo());
    assertEquals(Collections.emptyList(), response.getPatronHoldInfo());
    assertNull(response.getPatronStatusInfo());
    assertNull(response.getPhoto());
    assertNull(response.getPhotoType());
    assertEquals(Collections.emptyList(), response.getSmsInfo());
    assertNull(response.getUserSuspensionInfo());
  }

  /**
   * Verifies that calling {@link LookupPatronInfoClient#lookupWithAlternateId(String,
   * OptionalParams)} returns a populated response when all optional parameters are included.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupWithAlternateIdShouldRetrievePopulatedResponseWithAllParams() throws Exception {
    String alternateId = testConfig.getValidParams().getPatron().getAlternateId();
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();

    LookupPatronInfoClient client =
        new LookupPatronInfoClient(
            clientConfig,
            new OptionalParams()
                .includeFeeInfo(FeeInfoFilter.ALL_FEES_AND_PAYMENTS)
                .includePatronAddressInfo()
                .includePatronCheckoutHistoryInfo()
                .includePatronCheckoutInfo(CheckoutInfoFilter.ALL)
                .includePatronCirculationInfo()
                .includePatronHoldInfo(HoldInfoFilter.ALL)
                .includePatronInfo()
                .includePatronStatusInfo()
                .includePhoto()
                .includeSmsInfo()
                .includeUserGroupInfo()
                .includeUserSuspensionInfo());
    LookupPatronInfoResponse response = client.lookupWithAlternateId(alternateId);

    assertNotNull(response.getPatronAddressInfo());
    assertNotNull(response.getPatronCirculationInfo());
    assertNotNull(response.getPatronInfo());
    assertNotNull(response.getPatronStatusInfo());
    // The rest of these can be null depending on the patron.  I'm not sure how to test for them.
    // assertNotNull(response.getFeeInfo());
    // assertNotNull(response.getGroupInfo());
    // assertNotNull(response.getPatronCheckoutHistoryInfo());
    // assertNotNull(response.getPatronCheckoutInfo());
    // assertNotNull(response.getPatronHoldInfo());
    // assertNotNull(response.getPhoto());
    // assertNotNull(response.getPhotoType());
    // assertNotNull(response.getSmsInfo());
    // assertNotNull(response.getUserSuspensionInfo());
  }

  /**
   * Verifies that calling {@link LookupPatronInfoClient#lookupWithWebAuthId(String,
   * OptionalParams)} returns a successful but effectively empty response when no optional
   * parameters are included.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupWithWebAuthIdShouldRetrieveEmptyResponseWithNoParams() throws Exception {
    String webAuthId = testConfig.getValidParams().getPatron().getWebAuthId();
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();

    LookupPatronInfoClient client = new LookupPatronInfoClient(clientConfig, null);
    LookupPatronInfoResponse response = client.lookupWithWebAuthId(webAuthId);

    assertEquals(Collections.emptyList(), response.getFeeInfo());
    assertNull(response.getGroupInfo());
    assertNull(response.getPatronAddressInfo());
    assertEquals(Collections.emptyList(), response.getPatronCheckoutHistoryInfo());
    assertEquals(Collections.emptyList(), response.getPatronCheckoutInfo());
    assertNull(response.getPatronCirculationInfo());
    assertEquals(Collections.emptyList(), response.getPatronHoldInfo());
    assertNull(response.getPatronStatusInfo());
    assertNull(response.getPhoto());
    assertNull(response.getPhotoType());
    assertEquals(Collections.emptyList(), response.getSmsInfo());
    assertNull(response.getUserSuspensionInfo());
  }

  /**
   * Verifies that calling {@link LookupPatronInfoClient#lookupWithWebAuthId(String,
   * OptionalParams)} returns a populated response when all optional parameters are included.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupWithWebAuthIdShouldRetrievePopulatedResponseWithAllParams() throws Exception {
    String webAuthId = testConfig.getValidParams().getPatron().getWebAuthId();
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();

    LookupPatronInfoClient client =
        new LookupPatronInfoClient(
            clientConfig,
            new OptionalParams()
                .includeFeeInfo(FeeInfoFilter.ALL_FEES_AND_PAYMENTS)
                .includePatronAddressInfo()
                .includePatronCheckoutHistoryInfo()
                .includePatronCheckoutInfo(CheckoutInfoFilter.ALL)
                .includePatronCirculationInfo()
                .includePatronHoldInfo(HoldInfoFilter.ALL)
                .includePatronInfo()
                .includePatronStatusInfo()
                .includePhoto()
                .includeSmsInfo()
                .includeUserGroupInfo()
                .includeUserSuspensionInfo());

    LookupPatronInfoResponse response = client.lookupWithWebAuthId(webAuthId);

    assertNotNull(response.getPatronAddressInfo());
    assertNotNull(response.getPatronCirculationInfo());
    assertNotNull(response.getPatronInfo());
    assertNotNull(response.getPatronStatusInfo());
    // The rest of these can be null depending on the patron.  I'm not sure how to test for them.
    // assertNotNull(response.getFeeInfo());
    // assertNotNull(response.getGroupInfo());
    // assertNotNull(response.getPatronCheckoutHistoryInfo());
    // assertNotNull(response.getPatronCheckoutInfo());
    // assertNotNull(response.getPatronHoldInfo());
    // assertNotNull(response.getPhoto());
    // assertNotNull(response.getPhotoType());
    // assertNotNull(response.getSmsInfo());
    // assertNotNull(response.getUserSuspensionInfo());
  }

  /**
   * Verifies that attempting to retrieve a patron record using an invalid userId will fail.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupByUserIdShouldFailWithBogusUserId() throws Exception {
    String bogusUserId = testConfig.getBogusParams().getPatron().getUserId();
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();

    LookupPatronInfoClient client = new LookupPatronInfoClient(clientConfig, null);
    try {
      client.lookupWithUserId(bogusUserId);
      fail("SymwsClientException should have been thrown.");
    } catch (SymwsClientException e) {
      assertTrue("This exception should have a fault", e.hasFault());
      assertEquals(
          "This exception should have the expected fault code",
          SymwsFault.USER_NOT_FOUND,
          e.getFault().getCode());
    }
  }

  /**
   * Verifies that attempting to retrieve a patron record using an invalid alternateId will fail.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupByAlternateIdShouldFailWithBogusAlternateId() throws Exception {
    String bogusAlternateId = testConfig.getBogusParams().getPatron().getAlternateId();
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();

    LookupPatronInfoClient client = new LookupPatronInfoClient(clientConfig, null);
    try {
      client.lookupWithAlternateId(bogusAlternateId);
      fail("SymwsClientException should have been thrown.");
    } catch (SymwsClientException e) {
      assertTrue("This exception should have a fault", e.hasFault());
      assertEquals(
          "This exception should have the expected fault code",
          SymwsFault.USER_NOT_FOUND,
          e.getFault().getCode());
    }
  }

  /**
   * Verifies that attempting to retrieve a patron record using an invalid webAuthId will fail.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupByWebAuthIdShouldFailWithBogusWebAuthId() throws Exception {
    String bogusWebAuthId = testConfig.getBogusParams().getPatron().getWebAuthId();
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();

    LookupPatronInfoClient client = new LookupPatronInfoClient(clientConfig, null);
    try {
      client.lookupWithWebAuthId(bogusWebAuthId);
      fail("SymwsClientException should have been thrown.");
    } catch (SymwsClientException e) {
      assertTrue("This exception should have a fault", e.hasFault());
      assertEquals(
          "This exception should have the expected fault code",
          SymwsFault.USER_NOT_FOUND,
          e.getFault().getCode());
    }
  }

  /**
   * Verifies that calling {@link LookupPatronInfoClient#lookupWithUserId(String)} with an
   * unprivileged user will fail to return the user record.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupShouldFailIfUserIsUnprivileged() throws Exception {
    String userId = testConfig.getValidParams().getPatron().getUserId();
    String password = testConfig.getValidParams().getPatron().getPassword();
    SymwsClientConfig clientConfig =
        testConfig.getValidParams().getClientConfig().withAuthentication(userId, password);

    LookupPatronInfoClient client = new LookupPatronInfoClient(clientConfig, null);
    try {
      client.lookupWithUserId(userId);
      fail("SymwsClientException should have been thrown.");
    } catch (SymwsClientException e) {
      assertTrue("This exception should have a fault", e.hasFault());
      assertEquals(
          "This exception should have the expected fault code",
          SymwsFault.ACCESS_DENIED,
          e.getFault().getCode());
    }
  }
}
