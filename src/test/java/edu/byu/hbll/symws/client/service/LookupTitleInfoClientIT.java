package edu.byu.hbll.symws.client.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.TestConfig;
import edu.byu.hbll.symws.client.model.MarcEntryFilter;
import edu.byu.hbll.symws.client.model.ShowShadowedOption;
import edu.byu.hbll.symws.client.model.TitleInfo;
import edu.byu.hbll.symws.client.service.LookupTitleInfoClient.OptionalParams;
import java.util.Collections;
import org.junit.Test;

/** Integration tests for {@link LookupTitleInfoClient}. */
public class LookupTitleInfoClientIT {

  private TestConfig testConfig = TestConfig.getInstance();

  /**
   * Verifies that calling {@link LookupTitleInfoClient#lookupWithItemId(String, OptionalParams)}
   * will return a successful but effectively empty response when no optional parameters are
   * included.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupWithItemIdShouldRetrieveEmptyResponseWithNoParams() throws Exception {
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();
    String itemId = testConfig.getValidParams().getItem().getItemId();

    LookupTitleInfoClient lookupTitleInfoClient = new LookupTitleInfoClient(clientConfig, null);
    TitleInfo titleInfo = lookupTitleInfoClient.lookupWithItemId(itemId);

    assertNotNull(titleInfo.getTitleId());

    assertEquals(Collections.emptyList(), titleInfo.getAuthor());
    assertNull(titleInfo.getBaseCallNumber());
    assertNull(titleInfo.getBibliographicInfo());
    assertEquals(Collections.emptyList(), titleInfo.getBoundwithLinkInfo());
    assertEquals(Collections.emptyList(), titleInfo.getCallInfo());
    assertEquals(Collections.emptyList(), titleInfo.getCallSummary());
    assertNull(titleInfo.getCatalogFormatId());
    assertNull(titleInfo.getCatalogFormatType());
    assertNull(titleInfo.getCopiesOnOrder());
    assertNull(titleInfo.getDatePublished());
    assertNull(titleInfo.getExtent());
    assertEquals(Collections.emptyList(), titleInfo.getIsbn());
    assertEquals(Collections.emptyList(), titleInfo.getMarcHoldingInfo());
    assertNull(titleInfo.getMaterialType());
    assertNull(titleInfo.getNetLibraryId());
    assertNull(titleInfo.getNumberOfBoundwithLinks());
    assertNull(titleInfo.getNumberOfCallNumbers());
    assertNull(titleInfo.getNumberOfTitleHolds());
    assertNull(titleInfo.getOclcControlNumber());
    assertNull(titleInfo.getOutstandingCopiesOnOrder());
    assertNull(titleInfo.getPublisherName());
    assertEquals(Collections.emptyList(), titleInfo.getSici());
    assertNull(titleInfo.getSisacId());
    assertNull(titleInfo.getTitle());
    assertNull(titleInfo.getTitleAvailabilityInfo());
    assertEquals(Collections.emptyList(), titleInfo.getTitleOrderInfo());
    assertEquals(Collections.emptyList(), titleInfo.getUpc());
    assertNull(titleInfo.getYearOfPublication());
  }

  /**
   * Verifies that calling {@link LookupTitleInfoClient#lookupWithTitleId(long, OptionalParams)}
   * will return a successful but effectively empty response when no optional parameters are
   * included.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupWithTitleIdShouldRetrieveEmptyResponseWithNoParams() throws Exception {
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();
    Long titleId = Long.valueOf(testConfig.getValidParams().getTitle().getTitleId());

    LookupTitleInfoClient lookupTitleInfoClient = new LookupTitleInfoClient(clientConfig, null);
    TitleInfo titleInfo = lookupTitleInfoClient.lookupWithTitleId(titleId);

    assertEquals(titleId, titleInfo.getTitleId());

    assertEquals(Collections.emptyList(), titleInfo.getAuthor());
    assertNull(titleInfo.getBaseCallNumber());
    assertNull(titleInfo.getBibliographicInfo());
    assertEquals(Collections.emptyList(), titleInfo.getBoundwithLinkInfo());
    assertEquals(Collections.emptyList(), titleInfo.getCallInfo());
    assertEquals(Collections.emptyList(), titleInfo.getCallSummary());
    assertNull(titleInfo.getCatalogFormatId());
    assertNull(titleInfo.getCatalogFormatType());
    assertNull(titleInfo.getCopiesOnOrder());
    assertNull(titleInfo.getDatePublished());
    assertNull(titleInfo.getExtent());
    assertEquals(Collections.emptyList(), titleInfo.getIsbn());
    assertEquals(Collections.emptyList(), titleInfo.getMarcHoldingInfo());
    assertNull(titleInfo.getMaterialType());
    assertNull(titleInfo.getNetLibraryId());
    assertNull(titleInfo.getNumberOfBoundwithLinks());
    assertNull(titleInfo.getNumberOfCallNumbers());
    assertNull(titleInfo.getNumberOfTitleHolds());
    assertNull(titleInfo.getOclcControlNumber());
    assertNull(titleInfo.getOutstandingCopiesOnOrder());
    assertNull(titleInfo.getPublisherName());
    assertEquals(Collections.emptyList(), titleInfo.getSici());
    assertNull(titleInfo.getSisacId());
    assertNull(titleInfo.getTitle());
    assertNull(titleInfo.getTitleAvailabilityInfo());
    assertEquals(Collections.emptyList(), titleInfo.getTitleOrderInfo());
    assertEquals(Collections.emptyList(), titleInfo.getUpc());
    assertNull(titleInfo.getYearOfPublication());
  }

  /**
   * Verifies that calling {@link LookupTitleInfoClient#lookupWithItemId(String, OptionalParams)}
   * will return a full {@link TitleInfo} response when all optional parameters are included.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupWithItemIdShouldRetrievePopulatedResponseWithAllParams() throws Exception {
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();
    String itemId = testConfig.getValidParams().getItem().getItemId();

    LookupTitleInfoClient lookupTitleInfoClient =
        new LookupTitleInfoClient(
            clientConfig,
            new OptionalParams()
                .includeAvailabilityInfo()
                .includeBoundTogether()
                .includeCallNumberSummary()
                .includeCatalogingInfo()
                .includeItemCategory()
                .includeItemInfo()
                .includeMarcHoldings()
                .includeOPACInfo()
                .includeOrderInfo()
                .includeRelatedSearchInfo()
                .includeShadowed(ShowShadowedOption.BOTH)
                // .marcEntryId()  I'm not sure how this filter works
                .marcEntryFilter(MarcEntryFilter.ALL));
    TitleInfo titleInfo = lookupTitleInfoClient.lookupWithItemId(itemId);
    assertNotNull(titleInfo.getTitleId());

    assertNotNull(titleInfo.getAuthor());
    assertNotNull(titleInfo.getBaseCallNumber());
    assertNotNull(titleInfo.getBibliographicInfo());
    assertNotNull(titleInfo.getCallInfo());
    assertNotNull(titleInfo.getCallSummary());
    assertNotNull(titleInfo.getCatalogFormatId());
    assertNotNull(titleInfo.getCatalogFormatType());
    assertNotNull(titleInfo.getDatePublished());
    assertNotNull(titleInfo.getExtent());
    assertNotNull(titleInfo.getMaterialType());
    assertNotNull(titleInfo.getNetLibraryId());
    assertNotNull(titleInfo.getNumberOfBoundwithLinks());
    assertNotNull(titleInfo.getNumberOfCallNumbers());
    assertNotNull(titleInfo.getNumberOfTitleHolds());
    assertNotNull(titleInfo.getTitle());
    assertNotNull(titleInfo.getTitleAvailabilityInfo());
  }

  /**
   * Verifies that calling {@link LookupTitleInfoClient#lookupWithTitleId(long, OptionalParams)}
   * will return a full {@link TitleInfo} response when all optional parameters are included.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupWithTitleIdShouldRetrievePopulatedResponseWithAllParams() throws Exception {
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();
    Long titleId = Long.valueOf(testConfig.getValidParams().getTitle().getTitleId());

    LookupTitleInfoClient lookupTitleInfoClient =
        new LookupTitleInfoClient(
            clientConfig,
            new OptionalParams()
                .includeAvailabilityInfo()
                .includeBoundTogether()
                .includeCallNumberSummary()
                .includeCatalogingInfo()
                .includeItemCategory()
                .includeItemInfo()
                .includeMarcHoldings()
                .includeOPACInfo()
                .includeOrderInfo()
                .includeRelatedSearchInfo()
                .includeShadowed(ShowShadowedOption.BOTH)
                // .marcEntryId()  I'm not sure how this filter works
                .marcEntryFilter(MarcEntryFilter.ALL));
    TitleInfo titleInfo = lookupTitleInfoClient.lookupWithTitleId(titleId);
    assertEquals(titleId, titleInfo.getTitleId());

    assertNotNull(titleInfo.getAuthor());
    assertNotNull(titleInfo.getBaseCallNumber());
    assertNotNull(titleInfo.getBibliographicInfo());
    assertNotNull(titleInfo.getCallInfo());
    assertNotNull(titleInfo.getCallSummary());
    assertNotNull(titleInfo.getCatalogFormatId());
    assertNotNull(titleInfo.getCatalogFormatType());
    assertNotNull(titleInfo.getDatePublished());
    assertNotNull(titleInfo.getExtent());
    assertNotNull(titleInfo.getMaterialType());
    assertNotNull(titleInfo.getNetLibraryId());
    assertNotNull(titleInfo.getNumberOfBoundwithLinks());
    assertNotNull(titleInfo.getNumberOfCallNumbers());
    assertNotNull(titleInfo.getNumberOfTitleHolds());
    assertNotNull(titleInfo.getTitle());
    assertNotNull(titleInfo.getTitleAvailabilityInfo());
  }

  /**
   * Verifies that calling {@link LookupTitleInfoClient#lookupWithItemId(String, OptionalParams)}
   * will return an empty response if an invalid itemId is provided.
   *
   * <p>Unlike other lookup services, which will return a failure if an attempt is made to retrieve
   * non-existent data, the lookupTitleInfo service will return a 200 OK response along with an
   * empty XML stub wrapping a non-existent catalog record if the title being queried does not
   * exist.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupWithItemIdShouldReturnAnEmptyResponseWithBogusItemId() throws Exception {
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();
    String bogusItemId = testConfig.getBogusParams().getItem().getItemId();

    LookupTitleInfoClient lookupTitleInfoClient =
        new LookupTitleInfoClient(
            clientConfig,
            new OptionalParams()
                .includeAvailabilityInfo()
                .includeBoundTogether()
                .includeCallNumberSummary()
                .includeCatalogingInfo()
                .includeItemCategory()
                .includeItemInfo()
                .includeMarcHoldings()
                .includeOPACInfo()
                .includeOrderInfo()
                .includeRelatedSearchInfo()
                .includeShadowed(ShowShadowedOption.BOTH)
                .marcEntryFilter(MarcEntryFilter.ALL));
    TitleInfo titleInfo = lookupTitleInfoClient.lookupWithItemId(bogusItemId);
    assertNull(titleInfo.getTitleId());
    assertEquals(
        Integer.valueOf(0), titleInfo.getTitleAvailabilityInfo().getTotalCopiesAvailable());

    assertEquals(Collections.emptyList(), titleInfo.getAuthor());
    assertNull(titleInfo.getBaseCallNumber());
    assertNull(titleInfo.getBibliographicInfo());
    assertEquals(Collections.emptyList(), titleInfo.getBoundwithLinkInfo());
    assertEquals(Collections.emptyList(), titleInfo.getCallInfo());
    assertEquals(Collections.emptyList(), titleInfo.getCallSummary());
    assertNull(titleInfo.getCatalogFormatId());
    assertNull(titleInfo.getCatalogFormatType());
    assertNull(titleInfo.getCopiesOnOrder());
    assertNull(titleInfo.getDatePublished());
    assertNull(titleInfo.getExtent());
    assertEquals(Collections.emptyList(), titleInfo.getIsbn());
    assertEquals(Collections.emptyList(), titleInfo.getMarcHoldingInfo());
    assertNull(titleInfo.getMaterialType());
    assertNull(titleInfo.getNetLibraryId());
    assertNull(titleInfo.getNumberOfBoundwithLinks());
    assertNull(titleInfo.getNumberOfCallNumbers());
    assertNull(titleInfo.getNumberOfTitleHolds());
    assertNull(titleInfo.getOclcControlNumber());
    assertNull(titleInfo.getOutstandingCopiesOnOrder());
    assertNull(titleInfo.getPublisherName());
    assertEquals(Collections.emptyList(), titleInfo.getSici());
    assertNull(titleInfo.getSisacId());
    assertNull(titleInfo.getTitle());
    assertNotNull(
        titleInfo.getTitleAvailabilityInfo()); // This will be populated with an empty object.
    assertEquals(Collections.emptyList(), titleInfo.getTitleOrderInfo());
    assertEquals(Collections.emptyList(), titleInfo.getUpc());
    assertNull(titleInfo.getYearOfPublication());
  }

  /**
   * Verifies that calling {@link LookupTitleInfoClient#lookupWithTitleId(long, OptionalParams)}
   * will return an empty response if an invalid titleId is provided.
   *
   * <p>Unlike other lookup services, which will return a failure if an attempt is made to retrieve
   * non-existent data, the lookupTitleInfo service will return a 200 OK response along with an
   * empty XML stub wrapping a non-existent catalog record if the title being queried does not
   * exist.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupWithTitleIdShouldReturnAnEmptyResponseWithBogusTitleId() throws Exception {
    SymwsClientConfig clientConfig = testConfig.getValidParams().getClientConfig();
    Long bogusTitleId = Long.valueOf(testConfig.getBogusParams().getTitle().getTitleId());

    LookupTitleInfoClient lookupTitleInfoClient =
        new LookupTitleInfoClient(
            clientConfig,
            new OptionalParams()
                .includeAvailabilityInfo()
                .includeBoundTogether()
                .includeCallNumberSummary()
                .includeCatalogingInfo()
                .includeItemCategory()
                .includeItemInfo()
                .includeMarcHoldings()
                .includeOPACInfo()
                .includeOrderInfo()
                .includeRelatedSearchInfo()
                .includeShadowed(ShowShadowedOption.BOTH)
                .marcEntryFilter(MarcEntryFilter.ALL));
    TitleInfo titleInfo = lookupTitleInfoClient.lookupWithTitleId(bogusTitleId);
    // This field should be populated with the bogus title ID; but no other fields should be
    // populated.
    assertEquals(bogusTitleId, titleInfo.getTitleId());
    assertEquals(
        Integer.valueOf(0), titleInfo.getTitleAvailabilityInfo().getTotalCopiesAvailable());

    assertEquals(Collections.emptyList(), titleInfo.getAuthor());
    assertNull(titleInfo.getBaseCallNumber());
    assertNull(titleInfo.getBibliographicInfo());
    assertEquals(Collections.emptyList(), titleInfo.getBoundwithLinkInfo());
    assertEquals(Collections.emptyList(), titleInfo.getCallInfo());
    assertEquals(Collections.emptyList(), titleInfo.getCallSummary());
    assertNull(titleInfo.getCatalogFormatId());
    assertNull(titleInfo.getCatalogFormatType());
    assertNull(titleInfo.getCopiesOnOrder());
    assertNull(titleInfo.getDatePublished());
    assertNull(titleInfo.getExtent());
    assertEquals(Collections.emptyList(), titleInfo.getIsbn());
    assertEquals(Collections.emptyList(), titleInfo.getMarcHoldingInfo());
    assertNull(titleInfo.getMaterialType());
    assertNull(titleInfo.getNetLibraryId());
    assertNull(titleInfo.getNumberOfBoundwithLinks());
    assertNull(titleInfo.getNumberOfCallNumbers());
    assertNull(titleInfo.getNumberOfTitleHolds());
    assertNull(titleInfo.getOclcControlNumber());
    assertNull(titleInfo.getOutstandingCopiesOnOrder());
    assertNull(titleInfo.getPublisherName());
    assertEquals(Collections.emptyList(), titleInfo.getSici());
    assertNull(titleInfo.getSisacId());
    assertNull(titleInfo.getTitle());
    assertNotNull(
        titleInfo.getTitleAvailabilityInfo()); // This will be populated with an empty object.
    assertEquals(Collections.emptyList(), titleInfo.getTitleOrderInfo());
    assertEquals(Collections.emptyList(), titleInfo.getUpc());
    assertNull(titleInfo.getYearOfPublication());
  }
}
