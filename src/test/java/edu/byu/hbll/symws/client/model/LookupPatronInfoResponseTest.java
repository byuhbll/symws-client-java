package edu.byu.hbll.symws.client.model;

import static org.junit.Assert.assertEquals;

import edu.byu.hbll.xml.XmlUtils;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.xml.transform.dom.DOMSource;
import org.junit.Test;
import org.w3c.dom.Document;

/** Unit tests for {@link LookupPatronInfoResponse}. */
public class LookupPatronInfoResponseTest {

  private static final Path LOOKUP_PATRON_INFO_FILE =
      Paths.get("src/test/resources/lookupPatronInfo.xml");

  /**
   * Verifies that deserializing a {@link LookupPatronInfoResponse} containing a hold which has been
   * abandoned/expired on the shelf will properly deserialize the hold inactive reason metadata.
   *
   * @throws Exception if an exception occurs while parsing the source XML
   */
  @Test
  public void shouldDeserializeExpiredOnShelfHold() throws Exception {
    Document xml =
        XmlUtils.removeNamespaces(XmlUtils.parse(Files.newInputStream(LOOKUP_PATRON_INFO_FILE)));
    LookupPatronInfoResponse model =
        XmlUtils.unmarshal(new DOMSource(xml), LookupPatronInfoResponse.class);

    PatronHoldInfo hold = model.getPatronHoldInfo().get(0);
    assertEquals("EXP_ONSHLF", hold.getHoldInactiveReasonId());
    assertEquals(HoldInactiveType.HOLD_INACTIVE_REASON_EXP_ONSHELF, hold.getHoldInactiveType());
  }
}
