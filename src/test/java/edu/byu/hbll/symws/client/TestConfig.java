package edu.byu.hbll.symws.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.byu.hbll.json.YamlLoader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * A set of variables used in integration tests which may be unique to a particular installation of
 * Symphony and SYMWS, and loaded from a local configuration YAML file.
 */
public class TestConfig {

  private static TestConfig singleton;

  @JsonProperty("valid")
  private Parameters validParams;

  @JsonProperty("bogus")
  private Parameters bogusParams;

  /**
   * Returns a singleton instance of this class.
   *
   * @return the singleton
   */
  public static TestConfig getInstance() {
    if (singleton == null) {
      Path source = Paths.get("src/test/resources/test.yml");
      try {
        JsonNode json = new YamlLoader().load(source);
        singleton = new ObjectMapper().treeToValue(json, TestConfig.class);
      } catch (IOException e) {
        throw new UncheckedIOException(e);
      }
    }
    return singleton;
  }

  /**
   * Returns the validParams.
   *
   * @return the validParams
   */
  public Parameters getValidParams() {
    return validParams;
  }

  /**
   * Returns the bogusParams.
   *
   * @return the bogusParams
   */
  public Parameters getBogusParams() {
    return bogusParams;
  }

  /** Custom Jackson deserializer for the {@link TestConfig} class. */
  public static class Deserializer extends JsonDeserializer<TestConfig> {

    @Override
    public TestConfig deserialize(JsonParser parser, DeserializationContext context)
        throws IOException {
      ObjectMapper mapper = new ObjectMapper();
      JsonNode json = parser.readValueAsTree();

      TestConfig testConfig = new TestConfig();
      testConfig.validParams =
          mapper.treeToValue(json.path("parameters").path("valid"), Parameters.class);
      testConfig.bogusParams =
          mapper.treeToValue(json.path("parameters").path("bogus"), Parameters.class);
      return testConfig;
    }
  }

  /** Jackson bean representing a set of test parameters. */
  public static class Parameters {

    private SymwsClientConfig clientConfig;
    private String servicePath;
    private User patron;
    private Item item;
    private Title title;

    /**
     * Returns the clientConfig.
     *
     * @return the clientConfig
     */
    public SymwsClientConfig getClientConfig() {
      return clientConfig;
    }

    /**
     * Returns the servicePath.
     *
     * @return the servicePath
     */
    public String getServicePath() {
      return servicePath;
    }

    /**
     * Returns the patron.
     *
     * @return the patron
     */
    public User getPatron() {
      return patron;
    }

    /**
     * Returns the item.
     *
     * @return the item
     */
    public Item getItem() {
      return item;
    }

    /**
     * Returns the title.
     *
     * @return the title
     */
    public Title getTitle() {
      return title;
    }
  }

  /** Jackson bean representing a set of test parameters related to a patron user. */
  public static class User {

    private String userId;
    private String alternateId;
    private String webAuthId;
    private String password;

    /**
     * Returns the userId.
     *
     * @return the userId
     */
    public String getUserId() {
      return userId;
    }

    /**
     * Returns the alternateId.
     *
     * @return the alternateId
     */
    public String getAlternateId() {
      return alternateId;
    }

    /**
     * Returns the webAuthId.
     *
     * @return the webAuthId
     */
    public String getWebAuthId() {
      return webAuthId;
    }

    /**
     * Returns the password.
     *
     * @return the password
     */
    public String getPassword() {
      return password;
    }
  }

  /** Jackson bean representing a set of test parameters related to an item. */
  public static class Item {

    private String itemId;

    /**
     * Returns the itemId.
     *
     * @return the itemId
     */
    public String getItemId() {
      return itemId;
    }
  }

  /** Jackson bean representing a set of test parameters related to an title or catalog record. */
  public static class Title {

    private String titleId;

    /**
     * Returns the titleId.
     *
     * @return the titleId
     */
    public String getTitleId() {
      return titleId;
    }
  }
}
