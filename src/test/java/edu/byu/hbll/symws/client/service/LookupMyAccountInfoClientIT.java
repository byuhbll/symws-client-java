package edu.byu.hbll.symws.client.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.TestConfig;
import edu.byu.hbll.symws.client.model.CheckoutInfoFilter;
import edu.byu.hbll.symws.client.model.FeeInfoFilter;
import edu.byu.hbll.symws.client.model.HoldInfoFilter;
import edu.byu.hbll.symws.client.model.LookupPatronInfoResponse;
import edu.byu.hbll.symws.client.service.LookupPatronInfoClient.OptionalParams;
import java.util.Collections;
import org.junit.Test;

/** Integration tests for {@link LookupMyAccountInfoClient}. */
public class LookupMyAccountInfoClientIT {

  private TestConfig testConfig = TestConfig.getInstance();

  /**
   * Verifies that the lookup succeeds but returns an effectively empty response when no optional
   * parameters are included.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupShouldRetrieveEmptyResponseWithNoParams() throws Exception {
    String userId = testConfig.getValidParams().getPatron().getUserId();
    String password = testConfig.getValidParams().getPatron().getPassword();
    SymwsClientConfig clientConfig =
        testConfig.getValidParams().getClientConfig().withAuthentication(userId, password);

    LookupMyAccountInfoClient client = new LookupMyAccountInfoClient(clientConfig, null);
    LookupPatronInfoResponse response = client.lookup();

    assertEquals(Collections.emptyList(), response.getFeeInfo());
    assertNull(response.getGroupInfo());
    assertNull(response.getPatronAddressInfo());
    assertEquals(Collections.emptyList(), response.getPatronCheckoutHistoryInfo());
    assertEquals(Collections.emptyList(), response.getPatronCheckoutInfo());
    assertNull(response.getPatronCirculationInfo());
    assertEquals(Collections.emptyList(), response.getPatronHoldInfo());
    assertNull(response.getPatronStatusInfo());
    assertNull(response.getPhoto());
    assertNull(response.getPhotoType());
    assertEquals(Collections.emptyList(), response.getSmsInfo());
    assertNull(response.getUserSuspensionInfo());
  }

  /**
   * Verifies that the lookup succeeds and returns the requested account data.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void lookupShouldRetrievePopulatedResponseWithAllParams() throws Exception {
    String userId = testConfig.getValidParams().getPatron().getUserId();
    String password = testConfig.getValidParams().getPatron().getPassword();
    SymwsClientConfig clientConfig =
        testConfig.getValidParams().getClientConfig().withAuthentication(userId, password);

    LookupMyAccountInfoClient client =
        new LookupMyAccountInfoClient(
            clientConfig,
            new OptionalParams()
                .includeFeeInfo(FeeInfoFilter.ALL_FEES_AND_PAYMENTS)
                .includePatronAddressInfo()
                .includePatronCheckoutHistoryInfo()
                .includePatronCheckoutInfo(CheckoutInfoFilter.ALL)
                .includePatronCirculationInfo()
                .includePatronHoldInfo(HoldInfoFilter.ALL)
                .includePatronInfo()
                .includePatronStatusInfo()
                .includePhoto()
                .includeSmsInfo()
                .includeUserGroupInfo()
                .includeUserSuspensionInfo());
    LookupPatronInfoResponse response = client.lookup();

    assertNotNull(response.getPatronAddressInfo());
    assertNotNull(response.getPatronCirculationInfo());
    assertNotNull(response.getPatronInfo());
    assertNotNull(response.getPatronStatusInfo());
    // The rest of these can be null depending on the patron.  I'm not sure how to test for them.
    // assertNotNull(response.getFeeInfo());
    // assertNotNull(response.getGroupInfo());
    // assertNotNull(response.getPatronCheckoutHistoryInfo());
    // assertNotNull(response.getPatronCheckoutInfo());
    // assertNotNull(response.getPatronHoldInfo());
    // assertNotNull(response.getPhoto());
    // assertNotNull(response.getPhotoType());
    // assertNotNull(response.getSmsInfo());
    // assertNotNull(response.getUserSuspensionInfo());
  }
}
