package edu.byu.hbll.symws.client.service;

import edu.byu.hbll.symws.client.SymwsClientConfig;
import edu.byu.hbll.symws.client.SymwsClientException;
import edu.byu.hbll.symws.client.TestConfig;
import edu.byu.hbll.symws.client.service.PlaceMyHoldClient.OptionalParams;
import org.junit.Test;

/** Integration tests for {@link CreateMyHoldClient}. */
public class CreateMyHoldClientIT {

  private TestConfig testConfig = TestConfig.getInstance();

  /**
   * Verifies that calling {@link CreateMyHoldClient#placeTitleHold(long, OptionalParams)} with a
   * valid session will create a new hold on behalf of that user.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void createTitleHoldShouldCreateNewHold() throws Exception {
    String userId = testConfig.getValidParams().getPatron().getUserId();
    String password = testConfig.getValidParams().getPatron().getPassword();
    SymwsClientConfig clientConfig =
        testConfig.getValidParams().getClientConfig().withAuthentication(userId, password);

    long titleId = Long.parseLong(testConfig.getValidParams().getTitle().getTitleId());
    CreateMyHoldClient client = new CreateMyHoldClient(clientConfig, null);
    long holdKey = client.createTitleHold(titleId);
    // If we reach this point without error, the test has passed.  No assertions are required;
    // cleanup the hold.
    cleanupHold(clientConfig, holdKey);
  }

  /**
   * Verifies that calling {@link CreateMyHoldClient#placeItemHold(String, OptionalParams)} with a
   * valid session will create a new hold on behalf of that user.
   *
   * @throws Exception if an exception is thrown while running the test
   */
  @Test
  public void createItemHoldShouldCreateNewHold() throws Exception {
    String userId = testConfig.getValidParams().getPatron().getUserId();
    String password = testConfig.getValidParams().getPatron().getPassword();
    SymwsClientConfig clientConfig =
        testConfig.getValidParams().getClientConfig().withAuthentication(userId, password);

    String itemId = testConfig.getValidParams().getItem().getItemId();
    try {
      CreateMyHoldClient client = new CreateMyHoldClient(clientConfig, null);
      long holdKey = client.createItemHold(itemId);
      // If we reach this point without error, the test has passed.  No assertions are required;
      // cleanup the hold.
      cleanupHold(clientConfig, holdKey);
    } catch (SymwsClientException e) {
      System.out.println(e.getResponseBody());
    }
  }

  /**
   * Uses {@link CancelMyHoldClient} to remove the hold after the test is complete.
   *
   * @param clientConfig the {@link SymwsClientConfig} used to create the hold
   * @param holdKey the hold key provided by SYMWS when the hold was created
   * @throws Exception if an exception is thrown while cleaning up the hold
   */
  private void cleanupHold(SymwsClientConfig clientConfig, long holdKey) throws Exception {
    CancelMyHoldClient client = new CancelMyHoldClient(clientConfig);
    client.cancelHold(holdKey);
  }
}
